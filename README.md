# Lviv Transport Tracker #
* * *
[ ![Codeship Status for denys_meloshyn/lvivtransporttracker](https://app.codeship.com/projects/52203050-b888-0134-1f2e-12459f82346d/status?branch=master)](https://app.codeship.com/projects/194706)

Use LvivTransportTracker to see real time locations of public transport. Check arrival time of transport on the selected stop, find information about all routes, stops and points to change the transport. Use map to find the nearby stops and routes` details.

Mark transports and stops as favourite to get quick access to them.

Application uses data from Lviv city administration portal: http://city-adm.lviv.ua/