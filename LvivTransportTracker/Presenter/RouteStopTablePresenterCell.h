//
//  RouteStopTablePresenterCell.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 25/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RouteStopTableViewCellModel.h"
#import "RouteStopTableInteractionCell.h"

@protocol RouteStopTablePresenterCellDelegate <NSObject>

@optional
- (void)highlightCell:(RouteStopTableViewCellModel *)model;

@end

@interface RouteStopTablePresenterCell : NSObject

@property (nonatomic, strong) RouteStopTableInteractionCell *interaction;
@property (nonatomic, weak) NSObject <RouteStopTablePresenterCellDelegate> *delegate;

- (RouteStopTableViewCellModel *)viewModel;
- (void)changeHighlightedState:(BOOL)highlighted;

@end
