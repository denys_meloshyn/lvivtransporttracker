//
//  StopListPresenter.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 24/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

protocol StopListPresenterProtocol {
    var interactor: StopListInteractorProtocol { get }

    func locationPressed()
    func tabBarItem() -> UITabBarItem
    func searchChanged(_ text: String)
    func sectionPressed(_ section: Int)
    func title(for section: Int) -> String?
    func selectStop(at indexPath: IndexPath)
    func model(for index: IndexPath) -> StopListViewModel
}

class StopListPresenter: StopListPresenterProtocol {
    let router: StopListRouterProtocol
    weak var view: StopListViewProtocol?
    let interactor: StopListInteractorProtocol

    init(view: StopListViewProtocol, interactor: StopListInteractorProtocol, router: StopListRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        view.showPage(title: LocalisationManager.stops())
        showHideEmptyView()

        NotificationCenter.default.addObserver(forName: .appStartUpdate, object: nil, queue: nil) { [weak self] notification in
            self?.router.popToRoot()
        }

        NotificationCenter.default.addObserver(forName: .appFinishUpdate, object: nil, queue: nil) { [weak self] notification in
            self?.contentChanged()
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func locationPressed() {
        interactor.track(.stopListOpenMap)
        router.openStopMapScreen()
    }

    func showHideEmptyView() {
        if interactor.numberOfSections() == 0 {
            view?.emptyMessage(isHidden: false)
        } else {
            view?.emptyMessage(isHidden: true)
        }
    }

    func tabBarItem() -> UITabBarItem {
        return UITabBarItem(title: LocalisationManager.stops(),
                            image: R.image.tabbar_stop_icon(),
                            selectedImage: R.image.tabbar_stop_icon())
    }

    func title(for section: Int) -> String? {
        let stop = interactor.stop(at: section)

        let name = "🚏 \(stop.name ?? "")"
        if let stopDescription = stop.stopDescription {
            return "\(name)\n🗺 \(stopDescription)"
        }

        return name
    }

    func model(for indexPath: IndexPath) -> StopListViewModel {
        guard let transport = interactor.transport(at: indexPath) else {
            interactor.fetchTransports(for: indexPath)
            let colors: [UIColor] = [Constants.ladColor(), Constants.letColor(), Constants.tramColor()]
            let color = colors.randomElement()!

            return StopListViewModel(isLoading: true, name: nil, backgroundColour: color)
        }

        let model = StopListViewModel(isLoading: false,
                                      name: transport.shortName,
                                      backgroundColour: Utilities.color(for: transport.routeType ?? ""))

        return model
    }

    func sectionPressed(_ section: Int) {
        let stop = interactor.stop(at: section)
        interactor.track(.stopListOpenStop, parameters: ["name": stop.name ?? ""])

        router.openStopScreen(stop.objectID)
    }

    func searchChanged(_ text: String) {
        interactor.track(.stopListSearch, parameters: ["value": text])
        interactor.filterStops(with: text)
    }

    func selectStop(at indexPath: IndexPath) {
        let stop = interactor.stop(at: indexPath.section)
        guard let transport = interactor.transport(at: indexPath) else {
            return
        }

        interactor.track(.stopListOpenTransport, parameters: ["transport": transport.shortName ?? "",
                                                              "stop": stop.name ?? ""])
        router.openTransportDetailScreen(stopID: stop.objectID, transportID: transport.objectID)
    }
}

extension StopListPresenter: StopListInteractorDelegate {
    func contentChanged() {
        showHideEmptyView()
        view?.refreshItems()
    }

    func itemChanged(at indexPath: IndexPath) {
        let newIndexPath = IndexPath(row: 0, section: indexPath.row)
        view?.refreshItems(at: newIndexPath)
    }
}
