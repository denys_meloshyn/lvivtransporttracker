//
// Created by Denys Meloshyn on 2019-01-27.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol StopDetailMapPresenterProtocol {
}

class StopDetailMapPresenter: StopDetailMapPresenterProtocol {
    private weak var view: StopDetailMapViewProtocol?
    private var interactor: StopDetailMapInteractorProtocol

    private var allAnnotations = [TransportAnnotation]()

    init(view: StopDetailMapViewProtocol, interactor: StopDetailMapInteractorProtocol) {
        self.view = view
        self.interactor = interactor

        self.interactor.delegate = self

        let stop = interactor.stop
        let coordinate = CLLocationCoordinate2DMake(stop.latitude?.doubleValue ?? 0,
                                                    stop.longitude?.doubleValue ?? 0)
        let annotation = StopAnnotation(coordinate: coordinate, title: stop.name ?? "", stopID: stop.objectID)
        view.add(annotation: annotation)
        view.move(to: annotation)
    }

    func configureTransportAnnotations(for data: TransitRealtime_FeedMessage,
                                       with previousAnnotations: [TransportAnnotation]) -> (new: [TransportAnnotation], remove: [TransportAnnotation], all: [TransportAnnotation]) {
        var newAnnotations = [TransportAnnotation]()
        var allAnnotations = [TransportAnnotation]()
        let annotationsToRemove = NSMutableArray(array: previousAnnotations)

        let transports = interactor.transports()

        let feedItems = data.entity.filter { (entity: TransitRealtime_FeedEntity) -> Bool in
            let filter = transports.filter { (route: GtfsRoute) -> Bool in
                return route.routeID == entity.vehicle.trip.routeID
            }

            return !filter.isEmpty
        }

        for item in feedItems {
            let annotations = previousAnnotations.filter { (annotation: MKAnnotation) -> Bool in
                if let transportAnnotation = annotation as? TransportAnnotation {
                    return transportAnnotation.currentModel.routeID == item.vehicle.vehicle.id
                }

                return false
            }

            let transportAnnotation: TransportAnnotation
            if let annotation = annotations.first {
                annotationsToRemove.remove(annotation)
                allAnnotations.append(annotation)
                transportAnnotation = annotation
            } else {
                let filter = transports.filter { (route: GtfsRoute) -> Bool in
                    return route.routeID == item.vehicle.trip.routeID
                }

                transportAnnotation = TransportAnnotation()
                let position = TransportPosition()
                position.licensePlate = item.vehicle.vehicle.licensePlate
                position.transportName = filter.first?.shortName
                position.routeObjectID = filter.first?.objectID
                position.routeColor = Utilities.color(for: filter.first?.routeType ?? "")
                position.routeID = item.vehicle.vehicle.id

                transportAnnotation.currentModel = position
                newAnnotations.append(transportAnnotation)
                allAnnotations.append(transportAnnotation)
            }

            transportAnnotation.currentModel.angle = CGFloat(item.vehicle.position.bearing)
            transportAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(item.vehicle.position.latitude),
                                                                    longitude: Double(item.vehicle.position.longitude))
        }

        return (newAnnotations, annotationsToRemove as! [TransportAnnotation], allAnnotations)
    }
}

extension StopDetailMapPresenter: StopDetailMapInteractorDelegate {
    func contentChanged(data: TransitRealtime_FeedMessage) {
        let (add, remove, all) = configureTransportAnnotations(for: data, with: allAnnotations)
        allAnnotations = all

        view?.add(annotations: add)
        view?.remove(annotations: remove)
        view?.updateAllAnnotations()
    }
}
