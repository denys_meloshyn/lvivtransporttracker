//
// Created by Denys Meloshyn on 2019-01-26.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import RxSwift

protocol StopMapPresenterProtocol {
    func stopPressed(annotation: MKAnnotation)
}

class StopMapPresenter: StopMapPresenterProtocol {
    private let disposeBag = DisposeBag()
    private var router: StopMapRouterProtocol
    private weak var view: StopMapViewProtocol?
    private var interactor: StopMapInteractorProtocol
    private var allAnnotations = [TransportAnnotation]()

    init(view: StopMapViewProtocol, interactor: StopMapInteractorProtocol, router: StopMapRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        for i in 0..<interactor.numberOfStops() {
            let stop = interactor.stop(for: i)
            let coordinate = CLLocationCoordinate2DMake(stop.latitude?.doubleValue ?? 0,
                                                        stop.longitude?.doubleValue ?? 0)
            let annotation = StopAnnotation(coordinate: coordinate, title: stop.name ?? "", stopID: stop.objectID)
            view.addStop(annotation: annotation)
        }

        self.interactor.transportLocation.subscribe { [weak self] event in
            switch event {
            case .next(let model):
                guard let data = model, let strongSelf = self else {
                    return
                }
                let (add, remove, all) = strongSelf.configureTransportAnnotations(for: data, with: strongSelf.allAnnotations)
                strongSelf.allAnnotations = all
                strongSelf.view?.add(annotations: add)
                strongSelf.view?.remove(annotations: remove)
                strongSelf.view?.updateAllAnnotations()
            default:
                break
            }
        }.disposed(by: disposeBag)
    }

    func stopPressed(annotation: MKAnnotation) {
        guard let stopAnnotation = annotation as? StopAnnotation else {
            return
        }

        let stop = interactor.stop(for: stopAnnotation)
        interactor.track(.stopMapStopSelected, parameters: ["name": stop.name ?? ""])
        if stop.transports?.count == 0 {
            interactor.fetchTransports(for: stop)
        }
        router.openStopDetailScreen(stopID: stopAnnotation.stopID)
    }

    func configureTransportAnnotations(for data: TransitRealtime_FeedMessage,
                                       with previousAnnotations: [TransportAnnotation]) -> (new: [TransportAnnotation], remove: [TransportAnnotation], all: [TransportAnnotation]) {
        var newAnnotations = [TransportAnnotation]()
        var allAnnotations = [TransportAnnotation]()
        let annotationsToRemove = NSMutableArray(array: previousAnnotations)

        for item in data.entity {
            let annotations = previousAnnotations.filter { annotation -> Bool in
                return annotation.currentModel.routeID == item.vehicle.vehicle.id
            }

            let transportAnnotation: TransportAnnotation
            if let annotation = annotations.first {
                annotationsToRemove.remove(annotation)
                allAnnotations.append(annotation)
                transportAnnotation = annotation
            } else {
                let filter = interactor.transports.filter { (route: GtfsRoute) -> Bool in
                    return route.routeID == item.vehicle.trip.routeID
                }

                transportAnnotation = TransportAnnotation()
                let position = TransportPosition()
                position.licensePlate = item.vehicle.vehicle.licensePlate
                position.transportName = filter.first?.shortName
                position.routeObjectID = filter.first?.objectID
                position.routeColor = Utilities.color(for: filter.first?.routeType ?? "")
                position.routeID = item.vehicle.vehicle.id

                transportAnnotation.currentModel = position
                newAnnotations.append(transportAnnotation)
                allAnnotations.append(transportAnnotation)
            }

            transportAnnotation.currentModel.angle = CGFloat(item.vehicle.position.bearing)
            transportAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(item.vehicle.position.latitude),
                                                                    longitude: Double(item.vehicle.position.longitude))
        }

        return (newAnnotations, annotationsToRemove as! [TransportAnnotation], allAnnotations)
    }
}
