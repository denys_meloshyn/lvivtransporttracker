//
//  RouteStopTablePresenterCell.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 25/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "RouteStopTablePresenterCell.h"

#import "Constants.h"

@implementation RouteStopTablePresenterCell

- (instancetype)init {
    self = [super init];
    
    if (self != nil) {
        self.interaction = [[RouteStopTableInteractionCell alloc] init];
    }
    
    return self;
}

- (RouteStopTableViewCellModel *)viewModel
{
//    RouteStopTableViewCellModel *model = [[RouteStopTableViewCellModel alloc] init];
//
//    BOOL isFirstStop = (self.interaction.indexPath.row == 0);
//    BOOL isLastStop = (self.interaction.indexPath.row == [self.interaction.route.stopsTimesA count] - 1);
//
//    if (isLastStop || isFirstStop) {
//        model.stopViewWidth = 40.0;
//        model.titleFont = [UIFont boldSystemFontOfSize:20.0];
//    }
//    else {
//        model.stopViewWidth = 20.0;
//    }
//
//    model.stopViewCornerRadius = model.stopViewWidth / (CGFloat)2.0;
//    model.innerCircleViewCornerRadius = (model.stopViewWidth - 6) / (CGFloat)2.0;
//
//    if (isFirstStop) {
//        model.isConnectionLineTopViewHidden = YES;
//    }
//
//    if (isLastStop) {
//        model.isConnectionLineBottomViewHidden = YES;
//    }
//
//    model.title = self.interaction.stopEntity.name;
//
//    UIColor *typeColor = [Constants colorForRouteType:self.interaction.route.routeType];
//
//    model.stopViewBorderColor = typeColor;
//    model.connectionLineTopViewBackgroundColor = typeColor;
//    model.connectionLineBottomViewBackgroundColor = typeColor;
//
//    if ([self.interaction.route.stopsTimesA count] <= 1) {
//        model.innerCircleViewBackgroundColor = typeColor;
//    } else {
//        model.innerCircleViewBackgroundColor = [UIColor whiteColor];
//    }
//
//    return model;
    return nil;
}

- (void)changeHighlightedState:(BOOL)highlighted {
//    RouteStopTableViewCellModel *model = [[RouteStopTableViewCellModel alloc] init];
//    model.highlighted = highlighted;
//    
//    if (highlighted) {
//        model.contentViewBackgroundColor = [Constants colorForRouteType:self.interaction.route.routeType];
//
//        model.connectionLineTopViewBackgroundColor = [UIColor whiteColor];
//        model.connectionLineBottomViewBackgroundColor = [UIColor whiteColor];
//
//        if ([self.interaction.stopEntity.stopRouteTimes.routeTimes count] <= 1) {
//            model.stopViewBorderColor = [UIColor whiteColor];
//            model.stopViewBackgroundColor = [UIColor whiteColor];
//        }
//        else {
//            model.stopViewBorderColor = [UIColor whiteColor];
//            model.stopViewBackgroundColor = [Constants colorForRouteType:self.interaction.route.routeType];
//        }
//    }
//    else {
//        model.contentViewBackgroundColor = [UIColor whiteColor];
//
//        model.connectionLineTopViewBackgroundColor = [Constants colorForRouteType:self.interaction.route.routeType];
//        model.connectionLineBottomViewBackgroundColor = [Constants colorForRouteType:self.interaction.route.routeType];
//
//        model.stopViewBackgroundColor = [UIColor whiteColor];
//        model.stopViewBorderColor = [Constants colorForRouteType:self.interaction.route.routeType];
//    }
//    
//    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(highlightCell:)]) {
//        [self.delegate highlightCell:model];
//    }
}

@end
