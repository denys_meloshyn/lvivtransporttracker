//
// Created by Denys Meloshyn on 2019-01-20.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol FavoritePresenterProtocol {
    var interactor: FavoriteInteractorProtocol { get }

    func viewWillAppear(_ animated: Bool)
    func title(for section: Int) -> String
    func didSelectItem(at indexPath: IndexPath)
    func viewModel(for indexPath: IndexPath) -> FavoriteCellViewModel
}

enum FavoriteCellViewModel {
    case stop(viewModel: String)
    case transport(name: String, color: UIColor)
}

class FavoritePresenter: FavoritePresenterProtocol {
    private weak var view: FavoriteViewProtocol?

    let router: FavoriteRouterProtocol
    var interactor: FavoriteInteractorProtocol

    init(view: FavoriteViewProtocol, interactor: FavoriteInteractorProtocol, router: FavoriteRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        view.showPage(title: R.string.localizable.favourite())

        if interactor.numberOfSections() == 0 {
            view.showEmptyMessage()
        } else {
            view.showFavoriteList()
        }

        self.interactor.delegate = self
        NotificationCenter.default.addObserver(forName: .appStartUpdate, object: nil, queue: nil) { [weak self] notification in
            self?.router.popToRoot()
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func viewWillAppear(_ animated: Bool) {
        interactor.reFetch()
        view?.refresh()
    }

    func title(for section: Int) -> String {
        let result: String
        if interactor.numberOfSections() > 1 {
            if section == 0 {
                result = R.string.localizable.stops()
            } else {
                result = R.string.localizable.transport()
            }
        } else {
            if interactor.stops.count > 0 {
                result = R.string.localizable.stops()
            } else {
                result = R.string.localizable.transport()
            }
        }

        return result
    }

    func viewModel(for indexPath: IndexPath) -> FavoriteCellViewModel {
        let result: FavoriteCellViewModel
        if interactor.numberOfSections() > 1 {
            if indexPath.section == 0 {
                result = stopCellViewModel(for: interactor.stop(at: indexPath.row))
            } else {
                result = transportCellViewModel(for: interactor.transport(at: indexPath.row))
            }
        } else {
            if interactor.stops.count > 0 {
                result = stopCellViewModel(for: interactor.stop(at: indexPath.row))
            } else {
                result = transportCellViewModel(for: interactor.transport(at: indexPath.row))
            }
        }

        return result
    }

    func stopCellViewModel(for stop: GtfsStop) -> FavoriteCellViewModel {
        return .stop(viewModel: "🚏 \(stop.name ?? "")")
    }

    func transportCellViewModel(for transport: GtfsRoute) -> FavoriteCellViewModel {
        return .transport(name: "\(transport.shortName ?? "") | \(transport.longName ?? "")", 
                          color: Utilities.color(for: transport.routeType ?? ""))
    }

    func didSelectItem(at indexPath: IndexPath) {
        if interactor.numberOfSections() > 1 {
            if indexPath.section == 0 {
                let model = interactor.stop(at: indexPath.row)
                interactor.track(.favouriteOpenStop, parameters: ["name": model.name ?? ""])
                router.openStopScreen(stopID: model.objectID)
            } else {
                let model = interactor.transport(at: indexPath.row)
                interactor.track(.favouriteOpenTransport, parameters: ["name": model.shortName ?? ""])
                router.openTransportScreen(transportID: model.objectID)
            }
        } else {
            if interactor.stops.count > 0 {
                let model = interactor.stop(at: indexPath.row)
                interactor.track(.favouriteOpenStop, parameters: ["name": model.name ?? ""])
                router.openStopScreen(stopID: model.objectID)
            } else {
                let model = interactor.transport(at: indexPath.row)
                interactor.track(.favouriteOpenTransport, parameters: ["name": model.shortName ?? ""])
                router.openTransportScreen(transportID: model.objectID)
            }
        }
    }
}

extension FavoritePresenter: FavoriteInteractorDelegate {
    func contentChanged() {
        if interactor.numberOfSections() == 0 {
            view?.showEmptyMessage()
        } else {
            view?.showFavoriteList()
            view?.refresh()
        }
    }
}
