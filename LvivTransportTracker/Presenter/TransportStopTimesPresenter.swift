//
// Created by Denys Meloshyn on 2019-01-13.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportStopTimesPresenterProtocol {
    var interactor: TransportStopTimesInteractorProtocol { get }

    func favouritePressed()
    func stopDetailPressed(at indexPath: IndexPath)
    func modelForCell(at indexPath: IndexPath) -> TransportStopTimesCellModel
}

struct TransportStopTimesCellModel {
    let title: String
    let color: UIColor
    let timetable: [[TimeTable]]
}

class TransportStopTimesPresenter: TransportStopTimesPresenterProtocol {
    private weak var view: TransportStopTimesView!

    let router: TransportStopTimesRouterProtocol
    let interactor: TransportStopTimesInteractorProtocol

    init(view: TransportStopTimesView,
         interactor: TransportStopTimesInteractorProtocol,
         router: TransportStopTimesRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        view.showPage(title: interactor.transportDetail.transport?.shortName)
        refreshFavourite()
    }

    func modelForCell(at indexPath: IndexPath) -> TransportStopTimesCellModel {
        let timetable = interactor.timetable(at: indexPath)
        let model = TransportStopTimesCellModel(title: "🚏 \(interactor.stopName(at: indexPath))",
                                                color: Utilities.color(for: interactor.transportDetail.transport?.routeType ?? ""),
                                                timetable: timetable)
        return model
    }

    func stopDetailPressed(at indexPath: IndexPath) {
        let stop = interactor.stop(at: indexPath)

        if stop.transports?.count == 0 {
            interactor.fetchTransports(for: stop)
        }

        interactor.track(.transportStopTimeOpenStop, parameters: ["name": stop.name ?? ""])
        router.openStopScreen(stop.objectID)
    }

    func favouritePressed() {
        interactor.changeFavouriteState()
        refreshFavourite()
    }

    private func refreshFavourite() {
        let isFavourite = FavouriteRepository.shared.isFavourite(type: .transport, itemID: interactor.transportDetail.transport?.routeID ?? "")
        interactor.track(.transportStopTimeFavourite, parameters: ["value": isFavourite])

        view.updateFavourite(image: isFavourite ? R.image.favorite_selected()! : R.image.favorite_not_selected()!)
    }
}
