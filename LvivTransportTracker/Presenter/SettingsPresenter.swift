//
// Created by Denys Meloshyn on 2019-01-19.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import DeviceKit

protocol SettingsPresenterProtocol {
    func updatePressed()
    func sendEmailPressed()
    func followOnFBPressed()
    func viewModel() -> SettingsViewModel
    func timeStepperChanged(with value: Double)
}

class SettingsPresenter: SettingsPresenterProtocol {
    private weak var view: SettingsViewProtocol?
    private var interactor: SettingsInteractionProtocol

    init(view: SettingsViewProtocol, interactor: SettingsInteractionProtocol) {
        self.view = view
        self.interactor = interactor

        view.showPage(title: LocalisationManager.settings())
    }

    func viewModel() -> SettingsViewModel {
        return createViewModel()
    }

    func updatePressed() {
        interactor.track(.settingsUpdateShowWarning)
        view?.showUpdateMessage(R.string.localizable.warning(),
                                message: R.string.localizable.doYouWantToUpdateAllTransportAndRouteInfo(),
                                okMessage: R.string.localizable.oK(),
                                cancelMessage: R.string.localizable.cancel()) { action in
            NotificationCenter.default.post(name: .appStartUpdate, object: nil)
            UIApplication.shared.isIdleTimerDisabled = true

            self.interactor.track(.settingsUpdate)
            self.interactor.loadUpdate()
            self.view?.refreshView(with: self.createViewModel(with: 0, loaded: 0))
        }
    }

    func createViewModel(with total: Int? = nil, loaded: Int? = nil) -> SettingsViewModel {
        let update = TimeConverter.minuteSecondsConvert(CGFloat(interactor.updateIntervalValue)) ?? ""

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.doesRelativeDateFormatting = true
        let lastUpdate = dateFormatter.string(from: interactor.lastUpdate())

        var progress: Double?
        var loadingProgress: String?

        if let total = total, let loaded = loaded  {
            if total == 0 {
                progress = 0
                loadingProgress = "0%"
            } else {
                progress = max(0.0, min(1.0, Double(loaded) / Double(total)))
                loadingProgress = "\(Int(progress! * 100))%"
            }
        }

        let model = SettingsViewModel(email: kSupportEmail,
                                      update: update,
                                      updateFrequency: Double(interactor.updateIntervalValue),
                                      lastUpdate: lastUpdate,
                                      loadingProgress: progress,
                                      loadingProgressText: loadingProgress)
        return model
    }

    func timeStepperChanged(with value: Double) {
        interactor.updateIntervalValue = Int(value)
        interactor.track(.settingsStepperChanged, parameters: ["value": interactor.updateIntervalValue])

        view?.refreshView(with: createViewModel())
    }

    func sendEmailPressed() {
        interactor.track(.settingsSendEmail)
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
        let deviceInformation = "(\(UIDevice.current.systemVersion), \(Device()), \(appVersion))"
        view?.sendEmail(with: kSupportEmail, subject: R.string.localizable.feedbackOrQuestionsAboutIOSApplication() + " " + deviceInformation)
    }

    func followOnFBPressed() {
        interactor.track(.settingsFollowFB)
        let url = URL(string: "https://www.facebook.com/LvivTransportTracker/")!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

extension SettingsPresenter: SettingsInteractionDelegate {
    func loadingFinished() {
        UIApplication.shared.isIdleTimerDisabled = false
        interactor.track(.settingsUpdateFinish)
        NotificationCenter.default.post(name: .appFinishUpdate, object: nil)
        view?.refreshView(with: createViewModel())
    }

    func errorDuringLoading(error: Error) {
        UIApplication.shared.isIdleTimerDisabled = false
        interactor.track(.settingsUpdateError, parameters: ["error": "\(error)"])
        view?.refreshView(with: createViewModel())

        view?.showErrorMessageDuringLoading(title: R.string.localizable.error(),
                                            message: R.string.localizable.errorDuringLoadingTransportInformation(),
                                            tryAgainMessage: R.string.localizable.tryAgain(),
                                            cancelMessage: R.string.localizable.cancel()) { action in
            self.interactor.loadUpdate()
        }
    }

    func loadingProgressChanged(total: Int, loaded: Int) {
        view?.refreshView(with: createViewModel(with: total, loaded: loaded))
    }
}
