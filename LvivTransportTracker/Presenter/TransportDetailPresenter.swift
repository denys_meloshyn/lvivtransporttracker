//
//  TransportDetailPresenter.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 08/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

protocol TransportDetailPresenterProtocol {
}

class TransportDetailPresenter: TransportDetailPresenterProtocol {
    let interactor: TransportDetailInteractor
    let router: TransportDetailRouterProtocol

    var selectedTimeTableIndexPath: TimeTableIndexPath
    
    weak var view: TransportDetailViewProtocol?

    init(view: TransportDetailViewProtocol, interactor: TransportDetailInteractor, router: TransportDetailRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        let timeTable = interactor.currentTimeTable!
        selectedTimeTableIndexPath = interactor.index(for: timeTable)

        self.interactor.delegate = self

        updateTimeTableActionTitle()
        view.showPage(title: interactor.stopTransportTime.transport?.shortName ?? "")
    }

    private func formattedTimeTable(_ timeTable: TimeTable) -> String {
        let formatter = ConstantsSwift.timeTableDateFormatter
        let dateComponents = dateComponent(from: timeTable)

        return formatter.string(from: dateComponents.date!)
    }

    private func dateComponent(from model: TimeTable) -> DateComponents {
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current
        dateComponents.hour = model.hour
        dateComponents.minute = model.minute

        return dateComponents
    }

    func cellViewModel(for indexPath: IndexPath) -> RouteStopTableViewCellModel {
        let model = RouteStopTableViewCellModel()
        let stop = interactor.stop(at: indexPath)
        let transports = stop.transports as? Set<GtfsRoute> ?? Set()

        let isFirstStop = indexPath.row == 0
        let isLastStop = indexPath.row == (interactor.numberOfStops() - 1)

        if isLastStop || isFirstStop {
            model.stopViewWidth = 40
            model.titleFont = UIFont.boldSystemFont(ofSize: 20)
        }
        else {
            model.stopViewWidth = 20
        }

        model.stopViewCornerRadius = model.stopViewWidth / 2
        model.innerCircleViewCornerRadius = (model.stopViewWidth - 6) / 2

        if isFirstStop {
            model.isConnectionLineTopViewHidden = true
        }

        if isLastStop {
            model.isConnectionLineBottomViewHidden = true
        }

        let typeColor = Utilities.color(for: interactor.stopTransportTime.transport?.routeType ?? "")

        model.stopViewBorderColor = typeColor
        model.connectionLineTopViewBackgroundColor = typeColor
        model.connectionLineBottomViewBackgroundColor = typeColor

        if transports.count <= 1 {
            model.innerCircleViewBackgroundColor = typeColor
        } else {
            if #available(iOS 13.0, *) {
                model.innerCircleViewBackgroundColor = .systemBackground
            } else {
                model.innerCircleViewBackgroundColor = .white
            }
        }

        if transports.count == 0 {
            model.isFetching = true
            interactor.fetchTransport(for: stop)
            return model
        }

        model.title = stop.name
        model.otherTransports = configureOtherTransports(stop)

        let stopTime = interactor.tripTimeTable[indexPath.row]
        if let (hour, minute, second) = Utilities.splitTime(stopTime.arrivalTime ?? "") {
            let timeTable = TimeTable(hour: hour, minute: minute, second: second, tripID: "")
            model.title = "\(stop.name ?? "") | 🕐 \(formattedTimeTable(timeTable))"
        }

        return model
    }

    func configureOtherTransports(_ stop: GtfsStop) -> NSAttributedString {
        if stop.transports?.count ?? 0 == 1 {
            return NSAttributedString(string: "")
        }

        let result = NSMutableAttributedString(string: "")
        let transports = interactor.allTransport(for: stop,
                                                 except: interactor.stopTransportTime.transport?.shortName ?? "")

        for (enumerator, route) in transports.enumerated() {
            let attributes = [NSAttributedString.Key.foregroundColor: Utilities.color(for: route.routeType ?? "")]
            let transportAttributedString = NSAttributedString(string: route.shortName ?? "", attributes: attributes)
            result.append(transportAttributedString)

            if enumerator < transports.count - 1 {
                let separatorAttributedString = NSAttributedString(string: LocalisationManager.listSeparator())
                result.append(separatorAttributedString)
            }
        }

        return result
    }

    func timeTableTitle(for row: Int, component: Int) -> String? {
        if component == 0 {
            let section = interactor.timeTables[row]
            return String("\(section.first?.hour ?? 0)")
        }

        let section = interactor.timeTables[selectedTimeTableIndexPath.hourIndex]
        return String("\(section[row].minute)")
    }

    func newTimeTableSelected(for row: Int, component: Int) {
        if component == 0 {
            selectedTimeTableIndexPath = TimeTableIndexPath(hourIndex: row, minuteIndex: 0)
            view?.refreshTimetableMinutesAndShowFirst()
        } else {
            selectedTimeTableIndexPath = TimeTableIndexPath(hourIndex: selectedTimeTableIndexPath.hourIndex,
                                                            minuteIndex: row)
        }
    }

    func numberOfHours() -> Int {
        return interactor.timeTables.count
    }

    func numberOfMinutes() -> Int {
        let section = interactor.timeTables[selectedTimeTableIndexPath.hourIndex]
        return section.count
    }

    func timeTablePressed() {
        interactor.track(.transportDetailShowTimetable)
        view?.changePickerVisibility(isHidden: false)
    }

    func updateTimeTableActionTitle() {
        let timeTable = interactor.currentTimeTable!
        let title = "🕒 \(formattedTimeTable(timeTable))"
        view?.showTimeTableActionButton(title: title)
    }

    func selectTimetableAction() {
        let timeTables = interactor.timeTables
        let timeTable = timeTables[selectedTimeTableIndexPath.hourIndex][selectedTimeTableIndexPath.minuteIndex]
        interactor.updateCurrentTimetable(with: timeTable)

        view?.changePickerVisibility(isHidden: true)
        updateTimeTableActionTitle()

        interactor.fetchStops(for: timeTable.tripID)
    }

    func cancelSelectingTimetableAction() {
        view?.changePickerVisibility(isHidden: true)
    }

    func stopSelected(at indexPath: IndexPath) {
        let stop = interactor.stop(at: indexPath)
        interactor.track(.transportDetailOpenStop, parameters: ["name": stop.name ?? ""])
        router.openStopDetail(with: stop.objectID)
    }
    
    func mapPressed() {
        guard let objectID = interactor.stopTransportTime.stop?.objectID,
            let items = interactor.shape(for: interactor.currentTimeTable.tripID) else {
            return
        }
        
        router.openMap(with: objectID,
                       shapes: items.compactMap{ $0.locationCoordinate() },
                       routeID: interactor.stopTransportTime.transport?.routeID ?? "",
                       tripID: interactor.currentTimeTable.tripID,
                       stops: interactor.allStops())
    }
}

extension TransportDetailPresenter: TransportDetailInteractorDelegate {
    func contentChanged() {
        view?.refreshStops()
    }
}
