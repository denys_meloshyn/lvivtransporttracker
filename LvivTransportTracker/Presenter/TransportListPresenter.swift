//
// Created by Denys Meloshyn on 2019-01-06.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportListPresenterProtocol {
    var interactor: TransportListInteractorProtocol { get }

    func titleForSection(index: Int) -> String
    func transportSelected(at indexPath: IndexPath)
    func modelForCell(at indexPath: IndexPath) -> TransportListCellModel
}

struct TransportListCellModel {
    let title: String
    let titleColor: UIColor
    let borderColor: UIColor
    let backgroundColor: UIColor
    let highlightedTitleColor: UIColor
    let highlightedBackgroundColor: UIColor
}

class TransportListPresenter: TransportListPresenterProtocol {
    private weak var view: TransportListViewProtocol!

    let router: TransportListRouterProtocol
    var interactor: TransportListInteractorProtocol

    init(view: TransportListViewProtocol,
         interactor: TransportListInteractorProtocol,
         router: TransportListRouterProtocol) {
        self.view = view
        self.router = router
        self.interactor = interactor

        view.showPage(title: LocalisationManager.transport())
        
        showHideEmptyView()
        self.interactor.delegate = self

        NotificationCenter.default.addObserver(forName: .appStartUpdate, object: nil, queue: nil) { [weak self] notification in
            self?.router.popToRoot()
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    func titleForSection(index: Int) -> String {
        let title = interactor.sectionName(for: index)

        switch title {
        case kRouteTypeBus:
            return LocalisationManager.bus().capitalized
        case kRouteTypeTrolleybus:
            return LocalisationManager.trolleybus().capitalized
        default:
            return LocalisationManager.tram().capitalized
        }
    }

    func modelForCell(at indexPath: IndexPath) -> TransportListCellModel {
        let sectionTitle = interactor.sectionName(for: indexPath.section)
        let color = Utilities.color(for: sectionTitle)
        let transport = interactor.transport(at: indexPath)

        let model = TransportListCellModel(title: transport.shortName ?? "",
                                           titleColor: .black,
                                           borderColor: color,
                                           backgroundColor: .white,
                                           highlightedTitleColor: .white,
                                           highlightedBackgroundColor: color)
        return model
    }

    func transportSelected(at indexPath: IndexPath) {
        let transport = interactor.transport(at: indexPath)

        interactor.track(.transportListOpenTransport, parameters: ["name": transport.shortName ?? ""])
        router.openTransportDetailScreen(transportID: transport.objectID)
    }

    func showHideEmptyView() {
        if interactor.numberOfTransportTypes() == 0 {
            view?.emptyMessage(isHidden: false)
        } else {
            view?.emptyMessage(isHidden: true)
        }
    }
}

extension TransportListPresenter: TransportListInteractorDelegate {
    func contentChanged() {
        showHideEmptyView()
        view?.refreshItems()
    }
}
