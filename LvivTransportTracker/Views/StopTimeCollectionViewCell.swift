//
//  StopTimeCollectionViewCell.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 11.08.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

class StopTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        layer.borderWidth = 2.0
        layer.cornerRadius = 5.0
    }
}
