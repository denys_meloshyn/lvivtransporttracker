//
//  BarButtonItem.swift
//  Econ
//
//  Created by Denys Meloshyn on 13/03/2018.
//  Copyright © 2018 e-conomic. All rights reserved.
//

import UIKit

typealias BarButtonItemActionBlock = (BarButtonItem) -> Void

class BarButtonItem: UIBarButtonItem {
    private var actionBlock: BarButtonItemActionBlock?
    
    init(with image: UIImage, style: UIBarButtonItem.Style, action: @escaping BarButtonItemActionBlock) {
        super.init()
        
        actionBlock = action

        let button = UIButton(type: .custom)
        button.tintColor = Constants.defaultApperanceColor()
        button.addTarget(self, action: #selector(BarButtonItem.targetAction), for: .touchUpInside)
        button.setImage(image, for: .normal)
        button.setImage(image, for: .highlighted)
        self.customView = button
    }

    init(with title: String, style: UIBarButtonItem.Style, action: @escaping BarButtonItemActionBlock) {
        super.init()

        actionBlock = action

        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(BarButtonItem.targetAction), for: .touchUpInside)
        button.setTitle(title, for: .normal)
        button.setTitle(title, for: .highlighted)
        button.setTitleColor(Constants.defaultApperanceColor(), for: .normal)
        button.setTitleColor(Constants.defaultApperanceColor(), for: .highlighted)
        self.customView = button
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func targetAction() {
        actionBlock?(self)
    }
}
