//
//  EmptyContainerView.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 26/01/2019.
//  Copyright © 2019 Denys Meloshyn. All rights reserved.
//

import UIKit

class EmptyContainerView: UIView {
    @IBOutlet var titleLabel: UILabel!
}
