//
//  LoadingAnimationView.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 27.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "LoadingAnimationView.h"

#import "Constants.h"
#import "LoadingAnimationPath.h"

static NSString *const kColor = @"color";
static NSString *const kPathInfo = @"pathInfo";
static NSString *const kAnimations = @"animations";

@interface LoadingAnimationView () <CAAnimationDelegate>

@property (nonatomic, strong) IBOutlet UIView *containerView;

@property (nonatomic) NSInteger animationIndex;
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic) LoadingAnimationState state;
@property (nonatomic, strong) NSMutableDictionary *paths;
@property (nonatomic, strong) CAShapeLayer *animationLayer;
@property (nonatomic, strong) CABasicAnimation *currentAnimation;

@end

@implementation LoadingAnimationView

+ (LoadingAnimationView *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static LoadingAnimationView *shareInstance = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        shareInstance = [[LoadingAnimationView alloc] init];
    });
    
    return shareInstance;
}

+ (NSArray *)pathAnimations
{
    static dispatch_once_t onceToken = 0;
    static NSArray *items = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        NSMutableArray *result = [NSMutableArray array];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"LoadingAnimationPath" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        NSArray *animations = dict[ kAnimations ];
        NSDictionary *dictInfo = dict[ kPathInfo ];
        
        NSMutableArray *subResult;
        NSDictionary *pathInfoDict;
        LoadingAnimationPath *animationPath;
        for (NSArray *subAnimations in animations) {
            subResult = [NSMutableArray array];
            
            for (NSDictionary *animationDict in subAnimations) {
                animationPath = [[LoadingAnimationPath alloc] init];
                [animationPath configureWithDictionary:animationDict];
                
                pathInfoDict = dictInfo[ animationPath.pathID ];
                animationPath.color = [UIColor colorWithCSS:pathInfoDict[ kColor ]];
                
                [subResult addObject:animationPath];
            }
            
            [result addObject:subResult];
        }
        
        items = [NSArray arrayWithArray:result];
    });
    
    return [items copy];
}

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        self.paths = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void)startAnimation
{
    self.animationIndex = 0;
    self.state = LoadingAnimationStateRun;
    self.containerView.layer.sublayers = nil;
    self.paths = [NSMutableDictionary dictionary];
    
    [self runAmiationAtIndex:self.animationIndex];
}

- (void)runAmiationAtIndex:(NSInteger)index
{
    BOOL isDelegateSetted = NO;
    NSArray *animations = [LoadingAnimationView pathAnimations];
    NSArray *animationAtIndex = animations[ index ];
    
    UIBezierPath *bezierPath;
    UIBezierPath *newBezierPath;
    for (LoadingAnimationPath *path in animationAtIndex) {
        bezierPath = self.paths[ path.pathID ];
        
        if (bezierPath == nil) {
            bezierPath = [UIBezierPath bezierPath];
            [bezierPath moveToPoint:path.position];
            [bezierPath addLineToPoint:path.position];
            
            self.paths[ path.pathID ] = bezierPath;
        }
        else {
            newBezierPath = [bezierPath copy];
            [newBezierPath addLineToPoint:path.position];
            
            CAShapeLayer *animationLayer = [CAShapeLayer layer];
            animationLayer.strokeColor = [path.color CGColor];
            animationLayer.lineWidth = 5.0;
            animationLayer.fillColor = [[UIColor clearColor] CGColor];
            [self.containerView.layer addSublayer:animationLayer];
            
            self.currentAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
            
            if (!isDelegateSetted) {
                self.currentAnimation.delegate = self;
                isDelegateSetted = YES;
            }
            
            self.currentAnimation.duration = 1.0;
            self.currentAnimation.fromValue = (id)bezierPath.CGPath;
            self.currentAnimation.toValue = (id)newBezierPath.CGPath;
            
            [animationLayer addAnimation:self.currentAnimation forKey:nil];
            
            animationLayer.path = newBezierPath.CGPath;
            
            self.paths[ path.pathID ] = newBezierPath;
        }
    }
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    if (self.state == LoadingAnimationStateRun) {
        self.animationIndex++;
        
        if (self.animationIndex <  [[LoadingAnimationView pathAnimations] count]) {
            [self runAmiationAtIndex:self.animationIndex];
        }
        else {
            [self startAnimation];
        }
    }
}

- (BOOL)isAnimationAddedToTheView:(UIView *)rootView
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [LoadingAnimationView class]];
    NSArray *filtereItems = [rootView.subviews filteredArrayUsingPredicate:predicate];
    
    if ([filtereItems count] > 0) {
        return YES;
    }
    
    return NO;
}

- (void)addConstraintsForView:(UIView *)view toSuperView:(UIView *)superView
{
    NSArray *constraints;
    NSDictionary *viewsDictionary;
    
    viewsDictionary = @{ @"view" : view };
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [superView addConstraints:constraints];
    
    // Set left & right constraints to the super view
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [superView addConstraints:constraints];
}

#pragma mark - Public methods

- (void)removeAnimationFromView:(UIView *)rootView
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [LoadingAnimationView class]];
    NSArray *filtereItems = [rootView.subviews filteredArrayUsingPredicate:predicate];
    
    if ([filtereItems count] > 0) {
        LoadingAnimationView *loadingView  = [filtereItems firstObject];
        loadingView.currentAnimation.delegate = nil;
        [loadingView removeFromSuperview];
    }
}

- (void)showInView:(UIView *)rootView
{
    // Add spinner only when we couldn't find it in the view
    if (![self isAnimationAddedToTheView:rootView]) {
        // Get Xib file
        NSArray *nibItems = [[NSBundle mainBundle] loadNibNamed:@"LoadingAnimationView" owner:nil options:nil];
        LoadingAnimationView *animationView = [nibItems firstObject];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [rootView addSubview:animationView];
        
        [self addConstraintsForView:animationView toSuperView:rootView];
        [animationView performSelector:@selector(startAnimation) withObject:nil afterDelay:0.0];
    }
}

+ (LoadingAnimationView *)findAnimationInView:(UIView *)rootView
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [LoadingAnimationView class]];
    NSArray *filtereItems = [rootView.subviews filteredArrayUsingPredicate:predicate];
    LoadingAnimationView *result = [filtereItems firstObject];
    
    return result;
}

+ (BOOL)isAnimationAddedToTheView:(UIView *)rootView
{
    if ([LoadingAnimationView findAnimationInView:rootView] != nil) {
        return YES;
    }
    
    return NO;
}

+ (void)pauseAnimationInView:(UIView *)rootView
{
    LoadingAnimationView *loadingView  = [LoadingAnimationView findAnimationInView:rootView];
    loadingView.state = LoadingAnimationStatePause;
    loadingView.containerView.layer.sublayers = nil;
    loadingView.currentAnimation.delegate = nil;
}

+ (void)resumeAnimationInView:(UIView *)rootView
{
    LoadingAnimationView *loadingView  = [LoadingAnimationView findAnimationInView:rootView];
    
    if (loadingView.state == LoadingAnimationStatePause) {
        loadingView.state = LoadingAnimationStateRun;
        [loadingView runAmiationAtIndex:loadingView.animationIndex];
    }
}

+ (void)removeAnimationFromView:(UIView *)rootView
{
    LoadingAnimationView *loadingView  = [LoadingAnimationView findAnimationInView:rootView];
    loadingView.currentAnimation.delegate = nil;
    [loadingView removeFromSuperview];
}

+ (void)showInView:(UIView *)rootView
{
    // Add spinner only when we couldn't find it in the view
    if (![LoadingAnimationView isAnimationAddedToTheView:rootView]) {
        // Get Xib file
        NSArray *nibItems = [[NSBundle mainBundle] loadNibNamed:@"LoadingAnimationView" owner:nil options:nil];
        LoadingAnimationView *animationView = [nibItems firstObject];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [rootView addSubview:animationView];
        
        [animationView addConstraintsForView:animationView toSuperView:rootView];
        [animationView performSelector:@selector(startAnimation) withObject:nil afterDelay:0.0];
    }
}

@end
