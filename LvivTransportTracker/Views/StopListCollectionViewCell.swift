//
//  StopListCollectionViewCell.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 24/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit
import SkeletonView

struct StopListViewModel {
    let isLoading: Bool
    let name: String?
    let backgroundColour: UIColor
}

class StopListCollectionViewCell: UICollectionViewCell {
    @IBOutlet var title: UILabel!

    func configure(with model: StopListViewModel) {
        title.text = model.name
        
        if model.isLoading {
//            let animation = { (layer: CALayer) in
//                return layer.pulse
//            }
            backgroundColor = .clear
            //title.showAnimatedSkeleton(usingColor: model.backgroundColour, animation: animation)
            title.showAnimatedSkeleton()
        } else {
            title.hideSkeleton()
            backgroundColor = model.backgroundColour
        }
    }
}
