//
//  StopTableHeaderView.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 20.05.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "StopTableHeaderView.h"

#import "Constants.h"

@interface StopTableHeaderView ()

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end

@implementation StopTableHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.titleLabel.textColor = [Constants defaultApperanceColor];
    
    self.imageView.tintColor = [Constants defaultApperanceColor];
    self.imageView.image = [self.imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

#pragma mark - IBAction methods

- (IBAction)openStopDetailAction:(id)sender
{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(openStopDetails:section:)]) {
        [self.delegate openStopDetails:self.tableView section:self.section];
    }
}

@end
