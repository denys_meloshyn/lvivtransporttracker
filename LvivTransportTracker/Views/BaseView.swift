//
//  BaseView.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 24/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol LifeCycleStateProtocol {
    func viewDidLoad()
    func viewWillAppear(_ animated: Bool)
    func viewDidAppear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)
}

class BaseView<T: UIViewController>: BaseViewProtocol, LifeCycleStateProtocol {
    weak var viewController: T?

    init(with viewController: T) {
        self.viewController = viewController
    }

    // MARK: - BaseViewProtocol

    func showPage(title: String?) {
        viewController?.navigationItem.title = title
    }

    // MARK: - LifeCycleStateProtocol

    func viewDidLoad() {
    }

    func viewWillAppear(_ animated: Bool) {
    }

    func viewDidAppear(_ animated: Bool) {
    }

    func viewWillDisappear(_ animated: Bool) {
    }

    func viewDidDisappear(_ animated: Bool) {
    }
}
