//
//  StopListSectionView.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 24/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol StopListSectionViewDelegate: class {
    func actionButtonPressed(sender: StopListSectionView)
}

class StopListSectionView: UICollectionReusableView {
    @IBOutlet var title: UILabel!

    weak var delegate: StopListSectionViewDelegate?

    @IBAction func detailButtonPressed() {
        delegate?.actionButtonPressed(sender: self)
    }
}
