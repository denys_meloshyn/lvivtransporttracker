//
//  StopTableHeaderView.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 20.05.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StopTableHeaderViewDelegate <NSObject>

@optional
- (void)openStopDetails:(UITableView *)tableView section:(NSInteger)section;

@end

@interface StopTableHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@property (nonatomic) NSInteger section;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) id <StopTableHeaderViewDelegate> delegate;

@end
