//
//  TransportAnnotationView.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 07.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TransportAnnotationView.h"

@interface TransportAnnotationView ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation TransportAnnotationView

- (instancetype)initWithAnnotation:(TransportAnnotation *)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self != nil) {
        self.frame = CGRectMake(0, 0, 35, 35);
        
        self.tintColor = annotation.currentModel.routeColor;
        
        UIButton *disclosureButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        self.rightCalloutAccessoryView = disclosureButton;
        
        self.canShowCallout = YES;
        
        [self updateAnnotation];
    }
    
    return self;
}

- (void)updateAnnotation {
    TransportAnnotation *annotation = self.annotation;
    
    [self.imageView removeFromSuperview];
    
    UIImage *arrowImage = [UIImage imageNamed:@"arrow"];
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    self.imageView.tintColor = annotation.currentModel.routeColor;
    self.imageView.image = [arrowImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    CGFloat radians = (CGFloat)(annotation.currentModel.angle * M_PI / 180.0);
    
    CGAffineTransform t = CGAffineTransformMakeRotation(radians);
    self.imageView.transform = t;
    
    [self addSubview:self.imageView];
}

@end
