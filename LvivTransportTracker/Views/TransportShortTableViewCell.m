//
//  TransportShortTableViewCell.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 09/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TransportShortTableViewCell.h"

@interface TransportShortTableViewCell ()

@end

@implementation TransportShortTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.exclusiveTouch = YES;
}

@end
