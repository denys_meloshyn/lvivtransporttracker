//
//  TransportAnnotationView.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 07.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "TransportAnnotation.h"

@interface TransportAnnotationView : MKAnnotationView

- (void)updateAnnotation;

@end
