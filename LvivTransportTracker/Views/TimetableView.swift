//
//  TimetableView.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 12.08.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

class TimetableView: UIView {
    @IBOutlet var hoursCollectionView: UICollectionView!
    @IBOutlet var minutesCollectionView: UICollectionView!

    var color = UIColor.black
    var timetable = [[TimeTable]]() {
        didSet {
            hoursCollectionView.reloadData()
            minutesCollectionView.reloadData()
        }
    }

    fileprivate var selectedIndexPath = IndexPath()

    override func awakeFromNib() {
        super.awakeFromNib()

        selectedIndexPath = IndexPath(row: 0, section: 0)

        hoursCollectionView.register(R.nib.stopTimeCollectionViewCell)
        minutesCollectionView.register(R.nib.stopTimeCollectionViewCell)
    }
}

extension TimetableView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if timetable.isEmpty {
            return 0
        }

        if collectionView == hoursCollectionView {
            return timetable.count
        }

        return timetable[selectedIndexPath.row].count
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.stopTimeCollectionViewCell, for: indexPath)!

        cell.backgroundColor = .clear
        cell.layer.borderColor = color.cgColor

        if collectionView == hoursCollectionView {
            cell.titleLabel.text = "\(timetable[indexPath.row][0].hour)"

            if selectedIndexPath == indexPath {
                cell.layer.borderWidth = 3.0
                cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
            } else {
                cell.layer.borderWidth = 2.0
                cell.titleLabel.font = UIFont.systemFont(ofSize: 17)
            }

            return cell
        }

        cell.titleLabel.text = "\(timetable[selectedIndexPath.row][indexPath.row].minute)"

        return cell
    }
}

extension TimetableView: UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == hoursCollectionView {
            minutesCollectionView.reloadData()

            selectedIndexPath = indexPath
            hoursCollectionView.reloadItems(at: hoursCollectionView.indexPathsForVisibleItems)
        }
    }
}
