//
//  LoadingAnimationView.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 27.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    LoadingAnimationStatePause = 0,
    LoadingAnimationStateRun
} LoadingAnimationState;

@interface LoadingAnimationView : UIView

+ (LoadingAnimationView *)sharedInstance;

- (void)startAnimation;
- (void)showInView:(UIView *)rootView;
- (void)removeAnimationFromView:(UIView *)rootView;

+ (void)showInView:(UIView *)rootView;
+ (void)pauseAnimationInView:(UIView *)rootView;
+ (void)resumeAnimationInView:(UIView *)rootView;
+ (void)removeAnimationFromView:(UIView *)rootView;

@end
