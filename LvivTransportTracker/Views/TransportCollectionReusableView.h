//
//  TransportCollectionReusableView.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 02/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransportCollectionReusableView : UICollectionReusableView

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
