//
//  TransportStopTimesCollectionViewCell.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 13/01/2019.
//  Copyright © 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import SnapKit

protocol TransportStopTimesCollectionViewCellDelegate: class {
    func detailPressed(sender: TransportStopTimesCollectionViewCell)
}

class TransportStopTimesCollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var containerView: UIView!

    weak var delegate: TransportStopTimesCollectionViewCellDelegate?
    var timeTableView = R.nib.timetableView(owner: nil)!

    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.addSubview(timeTableView)
        timeTableView.snp.makeConstraints { maker in
            maker.edges.equalToSuperview()
        }
    }

    func configure(with model: TransportStopTimesCellModel) {
        titleLabel.text = model.title
        timeTableView.color = model.color
        timeTableView.timetable = model.timetable
    }

    @IBAction func openDetailAction() {
        delegate?.detailPressed(sender: self)
    }
}
