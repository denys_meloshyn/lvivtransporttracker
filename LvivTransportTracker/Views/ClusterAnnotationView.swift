//
// Created by Denys Meloshyn on 2019-02-03.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

class ClusterAnnotationView: MKAnnotationView {
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        tintColor = Constants.defaultApperanceColor()

        isOpaque = false
        canShowCallout = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        guard let clusterAnnotation = annotation as? MKClusterAnnotation, clusterAnnotation.memberAnnotations.count > 0 else {
            return
        }

        let circleFrame = rect.insetBy(dx: 2, dy: 2)

        context.setFillColor(Constants.defaultApperanceColor().cgColor)
        context.setStrokeColor(UIColor(white: 1, alpha: 0.3).cgColor)
        context.setLineWidth(3)

        context.fillEllipse(in: circleFrame)
        context.strokeEllipse(in: circleFrame)

        let textFontAttributes: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: 12),
                                                                 .foregroundColor: UIColor.white]
        let numberStr: String
        if clusterAnnotation.memberAnnotations.count >= 100 {
            numberStr = "∞"
        } else {
            numberStr = "\(clusterAnnotation.memberAnnotations.count)"
        }

        let numberStrObjc =  NSString(string: numberStr)
        var textSize = numberStrObjc.size(withAttributes: textFontAttributes)
        textSize.width = min(rect.width, textSize.width)

        var textRect = CGRect.zero
        textRect.origin.x = (rect.width - textSize.width) / 2
        textRect.origin.y = (rect.height - textSize.height) / 2
        textRect.size = textSize

        numberStrObjc.draw(in: textRect, withAttributes: textFontAttributes)
    }
}
