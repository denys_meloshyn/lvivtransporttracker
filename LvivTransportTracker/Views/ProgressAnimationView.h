//
//  ProgressAnimationView.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/10/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressAnimationView : UIView

+ (void)removeFromView:(UIView *)superView;
+ (ProgressAnimationView *)showInView:(UIView *)superView;

- (void)updateProgress:(CGFloat)progress withText:(NSString *)progressText;

@end
