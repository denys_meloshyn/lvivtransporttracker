//
//  StopTableViewCell.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopTableViewCell : UITableViewCell

//@property (nonatomic, strong) Stop *currentStop;

- (void)configure;

@end
