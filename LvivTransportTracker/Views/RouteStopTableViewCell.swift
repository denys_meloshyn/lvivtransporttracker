//
//  RouteStopTableViewCell.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 10/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit
import SkeletonView

class RouteStopTableViewCell: UITableViewCell {
    @IBOutlet var stopView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var innerCircleView: UIView!
    @IBOutlet var otherTransportsLabel: UILabel!
    @IBOutlet var connectionLineTopView: UIView!
    @IBOutlet var connectionLineBottomView: UIView!
    @IBOutlet var constraintStopViewWidth: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()

        isExclusiveTouch = true
        stopView.layer.borderWidth = 3
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        titleLabel.text = ""
        otherTransportsLabel.text = ""
        connectionLineTopView.isHidden = false
        innerCircleView.layer.borderWidth = 0.0
        connectionLineBottomView.isHidden = false
        connectionLineTopView.layer.borderWidth = 0
        connectionLineBottomView.layer.borderWidth = 0
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        innerCircleView.layer.borderColor = UIColor.clear.cgColor
        connectionLineTopView.layer.borderColor = UIColor.clear.cgColor
        connectionLineBottomView.layer.borderColor = UIColor.clear.cgColor
    }

    func configure(with model: RouteStopTableViewCellModel) {
        hideSkeletons()

        constraintStopViewWidth.constant = model.stopViewWidth

        connectionLineTopView.isHidden = model.isConnectionLineTopViewHidden
        connectionLineTopView.backgroundColor = model.connectionLineTopViewBackgroundColor

        connectionLineBottomView.isHidden = model.isConnectionLineBottomViewHidden
        connectionLineBottomView.backgroundColor = model.connectionLineBottomViewBackgroundColor

        innerCircleView.backgroundColor = model.innerCircleViewBackgroundColor
        innerCircleView.layer.cornerRadius = model.innerCircleViewCornerRadius

        stopView.layer.cornerRadius = model.stopViewCornerRadius
        stopView.layer.borderColor = model.stopViewBorderColor.cgColor

        if model.isFetching {
            stopView.showAnimatedSkeleton()
            titleLabel.showAnimatedSkeleton()
            innerCircleView.showAnimatedSkeleton()
            otherTransportsLabel.showAnimatedSkeleton()
            connectionLineTopView.showAnimatedSkeleton()
            connectionLineBottomView.showAnimatedSkeleton()

            layoutIfNeeded()
            return
        }

        titleLabel.text = model.title
        titleLabel.font = model.titleFont
        otherTransportsLabel.attributedText = model.otherTransports

        layoutIfNeeded()
    }
    
    private func hideSkeletons() {
        stopView.hideSkeleton()
        titleLabel.hideSkeleton()
        innerCircleView.hideSkeleton()
        otherTransportsLabel.hideSkeleton()
        connectionLineTopView.hideSkeleton()
        connectionLineBottomView.hideSkeleton()
    }
}
