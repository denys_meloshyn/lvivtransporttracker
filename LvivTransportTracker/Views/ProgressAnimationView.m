//
//  ProgressAnimationView.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/10/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "ProgressAnimationView.h"

#import "Constants.h"

@interface ProgressAnimationView ()

@property (nonatomic, strong) IBOutlet UILabel *progressLabel;
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;

@end

@implementation ProgressAnimationView

+ (BOOL)isAnimationAddedToTheView:(UIView *)rootView
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [self class]];
    NSArray *filtereItems = [rootView.subviews filteredArrayUsingPredicate:predicate];
    
    if ([filtereItems count] > 0) {
        return YES;
    }
    
    return NO;
}

+ (ProgressAnimationView *)showInView:(UIView *)superView
{
    ProgressAnimationView *animationView;
    
    // Add view only when we couldn't find it in the super view
    if (![ProgressAnimationView isAnimationAddedToTheView:superView]) {
        // Get Xib file
        NSArray *nibItems = [[NSBundle mainBundle] loadNibNamed:@"ProgressAnimationView" owner:nil options:nil];
        animationView = [nibItems firstObject];
        animationView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [superView addSubview:animationView];
        
        [animationView addTopBottomConstraints:superView];
    }
    
    return animationView;
}

+ (void)removeFromView:(UIView *)superView
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self isMemberOfClass: %@", [self class]];
    NSArray *filtereItems = [superView.subviews filteredArrayUsingPredicate:predicate];
    
    if ([filtereItems count] > 0) {
        ProgressAnimationView *animationView  = [filtereItems firstObject];
        [animationView removeFromSuperview];
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.progressView.tintColor = [Constants defaultApperanceColor];
    self.progressLabel.textColor = [Constants defaultApperanceColor];
}

- (void)updateProgress:(CGFloat)progress withText:(NSString *)progressText
{
    self.progressView.progress = progress;
    self.progressLabel.text = progressText;
}

#pragma mark - Private methods

- (void)addTopBottomConstraints:(UIView *)superView
{
    NSArray *constraints;
    NSDictionary *viewsDictionary;
    
    viewsDictionary = @{ @"view" : self };
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [superView addConstraints:constraints];
    
    // Set left & right constraints to the super view
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:viewsDictionary];
    [superView addConstraints:constraints];
}

@end
