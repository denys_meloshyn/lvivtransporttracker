//
// Created by Denys Meloshyn on 2019-02-03.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

class StopAnnotationView: MKAnnotationView {
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        tintColor = Constants.defaultApperanceColor()

        let disclosureButton = UIButton(type: .detailDisclosure)
        rightCalloutAccessoryView = disclosureButton

        isOpaque = false
        canShowCallout = true

        clusteringIdentifier = "ClusterStop"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        let circleBorderFrame = rect.insetBy(dx: 6, dy: 6)
        let circleMainFrame = rect.insetBy(dx: 9, dy: 9)

        context.setFillColor(Constants.defaultApperanceColor().cgColor)
        context.setStrokeColor(Constants.defaultApperanceColor().cgColor)
        context.setLineWidth(2)

        context.fillEllipse(in: circleMainFrame)
        context.strokeEllipse(in: circleBorderFrame)
    }
}
