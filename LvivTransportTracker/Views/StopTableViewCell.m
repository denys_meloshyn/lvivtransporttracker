//
//  StopTableViewCell.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "StopTableViewCell.h"

#import "Constants.h"
#import "TransportManager.h"
#import <UICollectionViewLeftAlignedLayout/UICollectionViewLeftAlignedLayout.h>

@interface StopTableViewCell ()

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSArray *widths;

@property (nonatomic, strong) IBOutlet UILabel *transportTitleLabel;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *constraintCollectionViewHeight;

@end

@implementation StopTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.exclusiveTouch = YES;
    
    UICollectionViewLeftAlignedLayout *leftCollectionFlowLayout = [[UICollectionViewLeftAlignedLayout alloc] init];
    UIEdgeInsets inset = leftCollectionFlowLayout.sectionInset;
    inset.left = 15.0;
    inset.right = 15.0;
    inset.bottom = 30.0;
    leftCollectionFlowLayout.sectionInset = inset;
    
    self.collectionView.collectionViewLayout = leftCollectionFlowLayout;
    
    self.collectionView.scrollsToTop = NO;
    
    UINib *nib = [UINib nibWithNibName:@"TransportShortCollectionViewCell" bundle:[NSBundle mainBundle]];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"TransportShortCollectionViewCell"];
}

- (void)configure
{
    [self resetCell];
    
    [self layoutIfNeeded];
    
//    self.transportTitleLabel.text = self.currentStop.name;
    
    NSMutableArray *result = [NSMutableArray array];
//    NSArray *letTransport = [self.currentStop transportsForType:kElectroType];
//    NSArray *ladTransport = [self.currentStop transportsForType:kMicroBusesType];
//    
//    if ([letTransport count] > 0) {
//        [result addObject:letTransport];
//    }
//
//    if ([ladTransport count] > 0) {
//        [result addObject:ladTransport];
//    }
    self.items = [NSArray arrayWithArray:result];
    
    [self calculateCellWidth];
    
    [self.collectionView reloadData];
    self.constraintCollectionViewHeight.constant = self.collectionView.collectionViewLayout.collectionViewContentSize.height;
}

- (void)resetCell
{
    self.transportTitleLabel.text = @"";
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.items count];
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSArray *items = self.items[ section ];
    
    // Return number of items
    return [items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;
    
    NSArray *widths = self.widths[ indexPath.section ];
    
    NSNumber *width = widths[ indexPath.row ];
    size.width = [width doubleValue];
    size.height = 40;
    
    // Set collection view cell size
    return size;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)calculateCellWidth
{
    CGFloat width = 0.0;
    NSMutableArray *resultCells = [NSMutableArray array];
    NSMutableArray *resultSections = [NSMutableArray array];
    
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"TransportShortCollectionViewCell" owner:nil options:nil];
    UICollectionViewCell *cell = [nibs firstObject];
    
    CGRect frame = CGRectZero;
    
    for (NSInteger i = 0; i < [self numberOfSectionsInCollectionView:self.collectionView]; i++) {
        [resultCells removeAllObjects];
        
        for (NSInteger j = 0; j < [self collectionView:self.collectionView numberOfItemsInSection:i]; j++) {
            frame = cell.frame;
            frame.size.height = 44.0;
            frame.size.width = CGRectGetWidth(self.collectionView.frame);
            cell.frame = frame;
            
//            cell.currentTransport = items[ j ];
            
//            width = MIN(ceil([cell width]), CGRectGetWidth(self.collectionView.frame));
            
            [resultCells addObject:@(width)];
        }
        
        [resultSections addObject:resultCells];
    }
    
    self.widths = [NSArray arrayWithArray:resultSections];
}

@end
