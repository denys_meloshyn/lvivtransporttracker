//
//  TransportTableViewCell.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 19.08.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

class TransportTableViewCell: UITableViewCell {
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var routeLabel: UILabel!
    @IBOutlet var nextTimeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        isExclusiveTouch = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        routeLabel.text = ""
    }

    func configure(model: GtfsRoute, arrivingTime: TimeTable?, nextTimeTable: TimeTable?) {
        routeLabel.text = "\(model.shortName ?? "") | \(model.longName ?? "")"
        contentView.backgroundColor = Utilities.color(for: model.routeType ?? "")

        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .short
        formatter.allowedUnits = NSCalendar.Unit(rawValue: NSCalendar.Unit.minute.rawValue | NSCalendar.Unit.second.rawValue)
        formatter.maximumUnitCount = 1

        if let arrivingTime = arrivingTime {
            timeLabel.text = formatter.string(from: arrivingTime.diff(Date()))
        } else {
            timeLabel.text = "-//-"
        }

        if let nextTimeTable = nextTimeTable {
            nextTimeLabel.text = formatter.string(from: nextTimeTable.diff(Date()))
        } else {
            nextTimeLabel.text = "-//-"
        }
    }
}
