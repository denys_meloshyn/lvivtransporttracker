//
//  TransportShortTableViewCell.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 09/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransportShortTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *routeNumberLabel;

@end
