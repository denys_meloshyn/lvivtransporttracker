//
// Created by Denys Meloshyn on 2019-01-07.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit

class TransportShortCollectionViewCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var containerView: UIView!

    var model: TransportListCellModel!

    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                titleLabel.textColor = model.highlightedTitleColor
                backgroundColor = model.highlightedBackgroundColor
            } else {
                if #available(iOS 13.0, *) {
                    titleLabel.textColor = UIColor.label
                    backgroundColor = UIColor.systemBackground
                } else {
                    titleLabel.textColor = model.titleColor
                    backgroundColor = model.backgroundColor
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        isExclusiveTouch = true
        contentView.layer.borderWidth = 3
    }

    func configure(with model: TransportListCellModel) {
        self.model = model

        titleLabel.text = model.title
        contentView.layer.borderColor = model.borderColor.cgColor
    }

    func width() -> CGFloat {
        containerView.layoutIfNeeded()
        return containerView.frame.width
    }
}
