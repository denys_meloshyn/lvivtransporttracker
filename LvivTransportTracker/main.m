//
//  main.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 22.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
