//
//  AsynchronousURLConnection.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 22.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AsynchronousURLConnection : NSObject

typedef void (^ErrorCompletionBlock)(id data, NSError *error);
typedef void (^AsynchronousCompletionBlock)(id data, NSURLResponse *response, NSError *error);

+ (AsynchronousURLConnection *)sharedInstance;
- (id)parseData:(NSData *)data error:(NSError **)error;
- (NSURLSessionDataTask *)sendAsynchronousRequest:(NSURLRequest *)request withCompletionBlock:(AsynchronousCompletionBlock)completionBlock;
- (NSURLSessionDataTask *)loadData:(NSURLRequest *)request withCompletionBlock:(AsynchronousCompletionBlock)completionBlock;

@end
