//
//  TransportAnnotation.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 07/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TransportAnnotation.h"

#import "TimeConverter.h"

@implementation TransportAnnotation

- (void)configureWithModel:(TransportPosition *)model;
{
    self.currentModel = model;

    self.title = model.transportName;
    self.subtitle = [TimeConverter convert:self.currentModel.timeToPoint];
}

- (NSString *)title {
    return [NSString stringWithFormat:@"%@ | %@", self.currentModel.transportName, self.currentModel.licensePlate];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"TransportAnnotation: %@", self.currentModel];
}

@end
