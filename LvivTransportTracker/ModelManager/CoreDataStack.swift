//
//  CoreDataStack.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 29.07.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import CoreData

class CoreDataStack: NSObject {
    struct PersistentContainerModel {
        static let favourite = PersistentContainerModel(modelName: "LvivTransportTrackerFavourite", dataBaseName: "LvivTransportTrackerFavourite")
        static let lvivTransportTracker = PersistentContainerModel(modelName: "LvivTransportTracker", dataBaseName: "LvivTransportTracker")
        static let lvivTransportTrackerUpdate = PersistentContainerModel(modelName: "LvivTransportTracker", dataBaseName: "LvivTransportTrackerUpdate")

        let modelName: String
        let dataBaseName: String
    }

    enum DataBaseName: String {
        case lvivTransportTracker
        case lvivTransportTrackerUpdate
        case favourite
    }

    static let shared = CoreDataStack()

    let model = CoreDataStack.loadModel()
    private var persistentContainers = [DataBaseName: NSPersistentContainer]()

    private func createPersistentContainer(for dataBase: PersistentContainerModel, model: NSManagedObjectModel? = nil, completion: ((NSPersistentContainer) -> Void)? = nil) -> NSPersistentContainer {
        let container = self.container(for: dataBase, model: model)

        container.loadPersistentStores { description, error in
            if error != nil {
                do {
                    try container.persistentStoreCoordinator.destroyPersistentStore(at: description.url!, ofType: NSSQLiteStoreType, options: nil)
                    try container.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                                                configurationName: nil,
                                                                                at: description.url!,
                                                                                options: nil)
                } catch {
                    print("\(error)")
                }
               
                container.loadPersistentStores { description, error in
                    if error != nil {
                        fatalError("Unresolved error \(description) \(String(describing: error))")
                    }
                }
            } else {
                completion?(container)
            }
        }

        return container
    }

    private func container(for dataBase: PersistentContainerModel, model: NSManagedObjectModel? = nil) -> NSPersistentContainer {
        let container: NSPersistentContainer
        if let model = model {
            container = NSPersistentContainer(name: dataBase.modelName, managedObjectModel: model)
        } else {
            container = NSPersistentContainer(name: dataBase.modelName)
        }

        let alternateURL = NSPersistentContainer.defaultDirectoryURL()
        let storeURL = alternateURL.appendingPathComponent("\(dataBase.dataBaseName).sqlite")
        let description = NSPersistentStoreDescription(url: storeURL)
        container.persistentStoreDescriptions = [description]

        return container
    }

    private static func loadModel() -> NSManagedObjectModel {
        guard let modelURL = Bundle.main.url(forResource: "LvivTransportTracker", withExtension: "momd") else {
            fatalError("error")
        }

        guard let model = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("error")
        }

        return model
    }

    lazy var persistentContainer: NSPersistentContainer = {
        let key = DataBaseName.lvivTransportTracker
        guard let persistentContainer = persistentContainers[key] else {
            let newContainer = createPersistentContainer(for: PersistentContainerModel.lvivTransportTracker, model: model)
            persistentContainers[key] = newContainer
            return newContainer
        }

        return persistentContainer
    }()

    func updatePersistentContainer() -> NSPersistentContainer {
        let key = DataBaseName.lvivTransportTrackerUpdate
        guard let persistentContainer = persistentContainers[key] else {
            let container = createPersistentContainer(for: PersistentContainerModel.lvivTransportTrackerUpdate, model: model)
            persistentContainers[key] = container
            return container
        }

        return persistentContainer
    }

    lazy var favouritePersistentContainer: NSPersistentContainer = {
        return createPersistentContainer(for: PersistentContainerModel.favourite)
    }()

    func recreateUpdatePersistentContainer(completion: ((NSPersistentStoreDescription, Error?) -> Void)? = nil) {
        let container = updatePersistentContainer()
        do {
            try container.persistentStoreCoordinator.persistentStores.forEach { store in
                guard let url = store.url else {
                    return
                }
                try container.persistentStoreCoordinator.destroyPersistentStore(at: url, ofType: NSSQLiteStoreType, options: nil)
                try container.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                                            configurationName: nil,
                                                                            at: url,
                                                                            options: nil)
            }
        } catch {
            DependencyContainer.instance.logger.log(.error, "\(error)")
        }
    }

    func saveContext(for dataBase: DataBaseName) {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func findOrCreateEntity(_ entity: NSManagedObject.Type,
                            entityID: String,
                            entityKeyID: String,
                            in managedObjectContext: NSManagedObjectContext) -> NSManagedObject? {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "%K == %@", entityKeyID, entityID)
        fetchRequest.fetchLimit = 1

        do {
            let items = try managedObjectContext.fetch(fetchRequest)
            return items.first as? NSManagedObject ?? entity.init(context: managedObjectContext)
        } catch {
            return nil
        }
    }

    func findOrCreateEntity(_ entity: NSManagedObject.Type,
                            entityKeyIdValue: [(String, String)],
                            in managedObjectContext: NSManagedObjectContext) -> NSManagedObject? {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
        let predicates = entityKeyIdValue.map { entityKeyID, entityValue -> NSPredicate in
            NSPredicate(format: "%K == %@", entityKeyID, entityValue)
        }
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        fetchRequest.fetchLimit = 1

        do {
            let items = try managedObjectContext.fetch(fetchRequest)
            return items.first as? NSManagedObject ?? entity.init(context: managedObjectContext)
        } catch {
            return nil
        }
    }

    func findMultipleEntity(_ entity: NSManagedObject.Type,
                            entities: [String],
                            in managedObjectContext: NSManagedObjectContext) -> [NSManagedObject] {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = entity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "computedPrimaryKey IN %@", entities)

        do {
            let items = try managedObjectContext.fetch(fetchRequest)
            return items as? [NSManagedObject] ?? []
        } catch {
            return []
        }
    }

    @objc static func objcShared() -> CoreDataStack {
        return shared
    }

    @objc func managedObjectContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }
}
