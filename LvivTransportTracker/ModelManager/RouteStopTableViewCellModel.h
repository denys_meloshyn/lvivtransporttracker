//
//  RouteStopTableViewCellModel.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 26/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RouteStopTableViewCellModel : NSObject

@property (nonatomic) BOOL isFetching;
@property (nonatomic) BOOL highlighted;
@property (nonatomic) CGFloat stopViewWidth;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic) CGFloat stopViewCornerRadius;
@property (nonatomic) BOOL isConnectionLineTopViewHidden;
@property (nonatomic) CGFloat innerCircleViewBorderWidth;
@property (nonatomic) CGFloat innerCircleViewCornerRadius;
@property (nonatomic, strong) UIColor *stopViewBorderColor;
@property (nonatomic) BOOL isConnectionLineBottomViewHidden;
@property (nonatomic, strong) UIColor *selectedBackgroundColor;
@property (nonatomic, strong) UIColor *stopViewBackgroundColor;
@property (nonatomic, strong) NSAttributedString *otherTransports;
@property (nonatomic, strong) UIColor *innerCircleViewBorderColor;
@property (nonatomic, strong) UIColor *contentViewBackgroundColor;
@property (nonatomic, strong) UIColor *innerCircleViewBackgroundColor;
@property (nonatomic, strong) UIColor *connectionLineTopViewBorderColor;
@property (nonatomic, strong) UIColor *connectionLineBottomViewBorderColor;
@property (nonatomic, strong) UIColor *connectionLineTopViewBackgroundColor;
@property (nonatomic, strong) UIColor *connectionLineBottomViewBackgroundColor;

@end
