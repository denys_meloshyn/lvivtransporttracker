//
//  TransportManager.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 23/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TransportManager.h"
#import "LvivTransportTracker-Swift.h"

@interface TransportManager ()

@property(nonatomic, strong) NSMutableDictionary *fetchOperations;

@end

@implementation TransportManager

+ (TransportManager *)sharedInstance {
    static dispatch_once_t onceToken = 0;
    static TransportManager *shareInstance = nil;

    // Init singleton
    dispatch_once(&onceToken, ^{
        shareInstance = [[TransportManager alloc] init];
    });

    return shareInstance;
}

- (instancetype)init {
    self = [super init];

    if (self != nil) {
        self.fetchQueue = [[NSOperationQueue alloc] init];
        self.fetchQueue.maxConcurrentOperationCount = 1;

        self.fetchOperations = [NSMutableDictionary dictionary];
    }

    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    return [[CoreDataStack objcShared] managedObjectContext];
}

#pragma mark - Public methods

- (void)dropDataBase:(NSManagedObjectContext *)managedObjectContext completionBlock:(ErrorCompletionBlock)completionBlock {
    NSMutableArray *entities = [NSMutableArray array];
//    [entities addObject:@"GtfsAgency"];
//    [entities addObject:@"GtfsCalendar"];
//    [entities addObject:@"GtfsCalendarDate"];
//    [entities addObject:@"GtfsFareAttributes"];
//    [entities addObject:@"GtfsFareRules"];
//    [entities addObject:@"GtfsRoute"];
//    [entities addObject:@"GtfsShape"];
//    [entities addObject:@"GtfsShapePoint"];
//    [entities addObject:@"GtfsStop"];
//    [entities addObject:@"GtfsStopTime"];
//    [entities addObject:@"GtfsTrip"];

    [self dropEntityName:@"StopTransportTime" managedObjectContext:managedObjectContext deleteID:@"stop"];
    [self dropEntityName:@"TransportDetail" managedObjectContext:managedObjectContext deleteID:@"transport"];
    [self dropEntityName:@"TransportDetailStopsTimes" managedObjectContext:managedObjectContext deleteID:@"stopID"];

    if (completionBlock != nil) {
        completionBlock(nil, nil);
    }
}

- (void)dropEntityName:entityName managedObjectContext:(NSManagedObjectContext *)managedObjectContext deleteID:(NSString *)deleteID {
    NSLog(@"Drop entity: %@", entityName);
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:deleteID ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    [fetchedResultsController performFetch:NULL];

    NSObject <NSFetchedResultsSectionInfo> *section = [[fetchedResultsController sections] firstObject];
    for (NSUInteger i = 0; i < section.numberOfObjects; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        NSManagedObject *object = [fetchedResultsController objectAtIndexPath:indexPath];
        [managedObjectContext deleteObject:object];
    }
}

- (NSError *)saveContext:(NSManagedObjectContext *)managedObjectContext {
    NSError *error = nil;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }

    return error;
}

- (void)saveTemporaryManagedObjectContext:(NSManagedObjectContext *)temporaryManagedObjectContext completionBlock:(ErrorCompletionBlock)completionBlock {
    [temporaryManagedObjectContext performBlock:^{
        [self saveContext:temporaryManagedObjectContext];

        [temporaryManagedObjectContext.parentContext performBlock:^{
            [self saveContext:temporaryManagedObjectContext.parentContext];

            if (completionBlock != nil) {
                completionBlock(nil, nil);
            }
        }];
    }];
}

#pragma mark - Stop

- (NSFetchedResultsController <GtfsStop *> *)stopFetchController:(NSManagedObjectContext *)managedObjectContext {
    NSFetchRequest *fetchRequest = [GtfsStop fetchRequest];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor];

    NSFetchedResultsController *fetchedResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                              managedObjectContext:managedObjectContext
                                                                                                sectionNameKeyPath:nil
                                                                                                         cacheName:nil];

    return fetchedResultController;
}

@end
