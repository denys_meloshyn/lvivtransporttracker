//
//  TransportAnnotation.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 07/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

#import "TransportPosition.h"

@interface TransportAnnotation : NSObject <MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic) CLLocationCoordinate2D coordinate;

@property (nonatomic, strong) TransportPosition *currentModel;

- (void)configureWithModel:(TransportPosition *)model;

@end
