//
//  GTFS.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 29.07.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import Foundation

import CSV
import RxSwift
import SSZipArchive
import HTTPNetworking
import CoreData

enum GTFSFile: String {
    case agency = "agency.txt"
    case calendarDates = "calendar_dates.txt"
    case calendar = "calendar.txt"
    case fareAttributes = "fare_attributes.txt"
    case fareRules = "fare_rules.txt"
    case feedInfo = "feed_info.txt"
    case routes = "routes.txt"
    case shapes = "shapes.txt"
    case stopTimes = "stop_times.txt"
    case stops = "stops.txt"
    case trips = "trips.txt"
}

protocol FileManagerProtocol {
    var storeDirectory: URL { get }

    func removeItem(at URL: URL) throws
}

protocol ZipArchiveProtocol {
    func unzip(file: URL, destination: URL)
}

protocol GTFSDelegate: class {
    func finishReadingFile(name: String, index: Int, from: Int)
    func file(_ file: GTFSFile, item: [String: String], managedObjectContext: NSManagedObjectContext)
}

class GTFS: NSObject {
    enum Constants {
        static let bufferSize = 1_000
    }

    private let zipURL: URL
    private let staticURL: URL
    private let folderURL: URL
    private let realTimeURL: URL
    private let httpNetwork: HTTPProtocol
    private let zipArchive: ZipArchiveProtocol
    private let fileManager: FileManagerProtocol
    private let managedObjectContext: NSManagedObjectContext
    private let logger = DependencyContainer.instance.logger

    private let disposeBag = DisposeBag()
    weak var delegate: GTFSDelegate?

    init(staticURL: URL,
         realTimeURL: URL,
         managedObjectContext: NSManagedObjectContext,
         httpNetwork: HTTPProtocol = HTTPNetwork.instance,
         fileManager: FileManagerProtocol = FileManager(),
         zipArchive: ZipArchiveProtocol = SSZipArchive(path: "")) {
        self.httpNetwork = httpNetwork
        self.staticURL = staticURL
        self.realTimeURL = realTimeURL
        self.fileManager = fileManager
        self.zipArchive = zipArchive
        self.managedObjectContext = managedObjectContext

        let fileName = "feed"
        _ = CoreDataStack.shared.updatePersistentContainer()
        zipURL = fileManager.storeDirectory.appendingPathComponent("\(fileName).zip")
        folderURL = fileManager.storeDirectory.appendingPathComponent(fileName)
    }

    func updateOrCreateEntitiesGrouped(file: GTFSFile,
                                       values: [[String: String]],
                                       computedPrimaryKey: ComputedPrimaryKeyProtocol.Type) -> ([String: [String: String]], [String]) {
        var primaryKeyValues = [String]()
        var groupItems = [String: [String: String]]()
        values.forEach { dictionary in
            guard let primaryKey = computedPrimaryKey.primaryKey(dict: dictionary) else {
                return
            }

            groupItems[primaryKey] = dictionary
            primaryKeyValues.append(primaryKey)
        }

        return (groupItems, primaryKeyValues)
    }

    func updateOrCreateEntities(file: GTFSFile, values: [[String: String]], managedObjectContext: NSManagedObjectContext) {
        values.forEach { dictionary in
            delegate?.file(file, item: dictionary, managedObjectContext: managedObjectContext)
        }
    }

    func loadStatic(completion: @escaping HTTPCompletionBlock) {
        let request = URLRequest(url: staticURL)
        httpNetwork.load(request, executeCompletionBlockInMainThread: false) { [weak self] httpData, response, error in
            if let error = error {
                completion(nil, nil, error)
                return
            }

            guard let folderURL = self?.folderURL,
                  let fileManager = self?.fileManager,
                  let zipURL = self?.zipURL,
                  let disposeBag = self?.disposeBag else {
                completion(nil, nil, nil)
                return
            }

            do {
                if FileManager.default.fileExists(atPath: folderURL.path) {
                    try fileManager.removeItem(at: folderURL)
                }
                try httpData?.write(to: zipURL)

                guard SSZipArchive.unzipFile(atPath: zipURL.path, toDestination: folderURL.path) else {
                    completion(nil, nil, NSError(domain: "", code: -1))
                    return
                }

                let dropDataBaseCompletable = Completable.create { event in
                    self?.logger.log(.debug, "Drop DB")
                    CoreDataStack.shared.recreateUpdatePersistentContainer()
                    self?.logger.log(.debug, "Save drop DB")
                    event(.completed)

                    return Disposables.create()
                }

                var fileFinished = 0
                let files = try FileManager.default.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil)
                let completableActions = files.enumerated().map { offset, file -> Completable in
                    Completable.create { [weak self] event in
                        CoreDataStack.shared.updatePersistentContainer().performBackgroundTask { backgroundContext in
                            self?.logger.log(.debug, "Start \(file.lastPathComponent)")

                            guard let gtfsFile = GTFSFile(rawValue: file.lastPathComponent),
                                  let stream = InputStream(url: file) else {
                                event(.error(NSError(domain: "", code: -1)))
                                return
                            }

                            do {
                                let csv = try CSVReader(stream: stream, hasHeaderRow: true)

                                var counter = 0
                                var buffer = [[String: String]]()
                                if let header = csv.headerRow {
                                    while let row = csv.next() {
                                        var dict = [String: String]()
                                        for (i, headerItem) in header.enumerated() {
                                            dict[headerItem] = row[i]
                                        }

                                        if buffer.count >= GTFS.Constants.bufferSize {
                                            self?.logger.log(.debug, "Save \(counter) \(gtfsFile)")
                                            self?.updateOrCreateEntities(file: gtfsFile, values: buffer, managedObjectContext: backgroundContext)
                                            try backgroundContext.save()
                                            buffer = []
                                        }
                                        buffer.append(dict)
                                        counter += 1
                                    }
                                }

                                if !buffer.isEmpty {
                                    self?.logger.log(.debug, "Save \(counter) \(gtfsFile)")
                                    self?.updateOrCreateEntities(file: gtfsFile, values: buffer, managedObjectContext: backgroundContext)
                                    try backgroundContext.save()
                                }

                                self?.delegate?.finishReadingFile(name: file.lastPathComponent, index: fileFinished, from: files.count)
                                self?.logger.log(.debug, "Finish \(file.lastPathComponent)")
                                fileFinished += 1
                            } catch {
                                event(.error(error))
                                return
                            }
                            
                            event(.completed)
                        }
                        
                        return Disposables.create()
                    }
                }

                dropDataBaseCompletable.andThen(Completable.zip(completableActions)).subscribe { event in
                    do {
                        self?.logger.log(.debug, "Save managedObjectContext")
                        guard let updateURL = CoreDataStack.shared.updatePersistentContainer().persistentStoreCoordinator.persistentStores.first?.url,
                              let origin = CoreDataStack.shared.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url else {
                            self?.logger.log(.error, "URL is nil \(CoreDataStack.shared.updatePersistentContainer().persistentStoreCoordinator.persistentStores.first?.url), \(CoreDataStack.shared.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url)")
                            return
                        }

                        try CoreDataStack.shared.persistentContainer.persistentStoreCoordinator.replacePersistentStore(at: origin,
                                                                                                                       destinationOptions: nil,
                                                                                                                       withPersistentStoreFrom: updateURL,
                                                                                                                       sourceOptions: nil, ofType: NSSQLiteStoreType)
                        try CoreDataStack.shared.persistentContainer.persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                                                                                   configurationName: nil,
                                                                                                                   at: origin,
                                                                                                                   options: nil)
                        self?.logger.log(.debug, "Finish saving")
                    } catch {
                        self?.logger.log(.error, "\(error)")
                    }

                    DispatchQueue.main.async {
                        completion(nil, nil, nil)
                    }
                }.disposed(by: disposeBag)
            } catch {
                DispatchQueue.main.async {
                    completion(nil, nil, nil)
                }
            }
        }
    }
}

extension FileManager: FileManagerProtocol {
    var storeDirectory: URL {
        if #available(iOS 10, *) {
            return temporaryDirectory
        } else {
            return URL(string: NSTemporaryDirectory())!
        }
    }
}

extension SSZipArchive: ZipArchiveProtocol {
    func unzip(file: URL, destination: URL) {

    }
}
