//
//  TimeTable.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 03/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

struct TimeTable {
    let hour: Int
    let minute: Int
    let second: Int
    let tripID: String

    func diff(_ date: Date) -> DateComponents {
        let set = Set([Calendar.Component.hour, Calendar.Component.minute, Calendar.Component.second])
        var dateComponents = Calendar.current.dateComponents(set, from: date)

        if let dateComponentsHour = dateComponents.hour {
            dateComponents.hour = hour - dateComponentsHour
        }

        if let dateComponentsMinute = dateComponents.minute {
            dateComponents.minute = minute - dateComponentsMinute
        }

        if let dateComponentsSecond = dateComponents.second {
            dateComponents.second = second - dateComponentsSecond
        }

        return dateComponents
    }
}

extension TimeTable: Equatable {}
