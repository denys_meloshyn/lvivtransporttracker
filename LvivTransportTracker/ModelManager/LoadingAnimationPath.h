//
//  LoadingAnimationPath.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 29/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingAnimationPath : NSObject

@property (nonatomic) CGPoint position;
@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) NSString *action;
@property (nonatomic, strong) NSString *pathID;

- (void)configureWithDictionary:(NSDictionary *)dict;

@end
