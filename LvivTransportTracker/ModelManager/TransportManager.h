//
//  TransportManager.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 23/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "AsynchronousURLConnection.h"

@interface TransportManager : NSObject

@property (nonatomic, strong) NSOperationQueue *fetchQueue;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

+ (TransportManager *)sharedInstance;

- (NSError *)saveContext:(NSManagedObjectContext *)managedObjectContext;
- (void)dropDataBase:(NSManagedObjectContext *)managedObjectContext completionBlock:(ErrorCompletionBlock)completionBlock;
- (void)saveTemporaryManagedObjectContext:(NSManagedObjectContext *)temporaryManagedObjectContext completionBlock:(ErrorCompletionBlock)completionBlock;

@end
