//
//  TransportPosition.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TransportPosition.h"

@implementation TransportPosition

- (NSString *)description {
    return [NSString stringWithFormat:@"TransportPosition: angle: %lf routeID: %@", self.angle, self.routeID];
}

@end
