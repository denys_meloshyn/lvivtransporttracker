//
//  LoadingAnimationPath.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 29/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "LoadingAnimationPath.h"

static NSString *const kX = @"x";
static NSString *const kY = @"y";
static NSString *const kPathID = @"pathID";
static NSString *const kAction = @"action";

@implementation LoadingAnimationPath

- (void)configureWithDictionary:(NSDictionary *)dict
{
    CGFloat x = [dict[ kX ] floatValue];
    CGFloat y = [dict[ kY ] floatValue];
    
    self.position = CGPointMake(x, y);
    
    self.pathID = dict[ kPathID ];
    self.action = dict[ kAction ];
}

@end
