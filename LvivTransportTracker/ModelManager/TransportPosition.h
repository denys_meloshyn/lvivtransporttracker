//
//  TransportPosition.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

@interface TransportPosition : NSObject

@property (nonatomic) CGFloat angle;
@property (nonatomic) NSInteger timeToPoint;
@property (nonatomic, strong) NSString *routeID;
@property (nonatomic, strong) UIColor *routeColor;
@property (nonatomic, strong) NSString *licensePlate;
@property (nonatomic, strong) NSString *transportName;
@property (nonatomic, strong) NSManagedObjectID *routeObjectID;

@end
