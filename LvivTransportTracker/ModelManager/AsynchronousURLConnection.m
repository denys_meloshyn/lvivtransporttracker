//
//  AsynchronousURLConnection.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 22.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "AsynchronousURLConnection.h"

#import "NetworkActivityIndicatorManager.h"

@implementation AsynchronousURLConnection

+ (AsynchronousURLConnection *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static AsynchronousURLConnection *shareInstance = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        shareInstance = [[AsynchronousURLConnection alloc] init];
    });
    
    return shareInstance;
}

- (NSURLSessionDataTask *)loadData:(NSURLRequest *)request withCompletionBlock:(AsynchronousCompletionBlock)completionBlock
{
    [[NetworkActivityIndicatorManager sharedInstance] startLoading];

    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];

    __block NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [[NetworkActivityIndicatorManager sharedInstance] stopLoading];

        if (task.state == NSURLSessionTaskStateCanceling || error.code == NSURLErrorCancelled) {
            return;
        }

        if (error != nil) {
            if (completionBlock != nil) {
                completionBlock(nil, response, error);
            }

            return;
        }

        NSInteger statusCode = ((NSHTTPURLResponse *)response).statusCode;
        if (statusCode != 200) {
            if (completionBlock != nil) {
                error = [NSError errorWithDomain:@"" code:statusCode userInfo:nil];
                completionBlock(nil, response, error);
            }

            return;
        }

        if (completionBlock != nil) {
            completionBlock(data, response, nil);
        }
    }];

    [task resume];

    return task;
}

- (NSURLSessionDataTask *)sendAsynchronousRequest:(NSURLRequest *)request withCompletionBlock:(AsynchronousCompletionBlock)completionBlock
{
    NSURLSessionDataTask *task = [self loadData:request withCompletionBlock:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (task.state == NSURLSessionTaskStateCanceling || error.code == NSURLErrorCancelled) {
            return;
        }
        
        if (error != nil) {
            if (completionBlock != nil) {
                completionBlock(nil, response, error);
            }
            
            return;
        }
        
        NSError *errorJSON;
        NSObject *result = [self parseData:data error:&errorJSON];
        
        if (errorJSON != nil) {
            if (completionBlock != nil) {
                completionBlock(nil, response, errorJSON);
            }
            
            return;
        }
        
        if (completionBlock != nil) {
            completionBlock(result, response, nil);
        }
    }];
    
    [task resume];
    
    return task;
}

- (id)parseData:(NSData *)data error:(NSError **)error
{
    
    NSObject *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:error];
    
    return result;
}

@end
