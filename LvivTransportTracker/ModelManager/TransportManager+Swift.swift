//
//  TransportManager+Swift.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 17/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

extension TransportManager {
    func fetchAllTransports(for stopID: NSManagedObjectID, managedObjectContext: NSManagedObjectContext) {
        let stop = managedObjectContext.object(with: stopID) as! GtfsStop
        DependencyContainer.instance.logger.log(.debug, "fetchAllTransports: \(String(describing: stop.name ?? ""))")

        let stopTimeRequest: NSFetchRequest<GtfsStopTime> = GtfsStopTime.fetchRequest()
        stopTimeRequest.predicate = NSPredicate(format: "stopID == %@", stop.stopID!)

        do {
            let asynchronousStopTimeRequest = try managedObjectContext.execute(stopTimeRequest) as! NSAsynchronousFetchResult<GtfsStopTime>
            let stopTripIDs = (asynchronousStopTimeRequest.finalResult ?? []).map { item -> String in
                return item.tripID!
            }

            let tripRequest: NSFetchRequest<GtfsTrip> = GtfsTrip.fetchRequest()
            tripRequest.predicate = NSPredicate(format: "tripID IN %@", stopTripIDs)

            let asynchronousTripRequest = try managedObjectContext.execute(tripRequest) as! NSAsynchronousFetchResult<GtfsTrip>
            let routeIDs = (asynchronousTripRequest.finalResult ?? []).map { item -> String in
                return item.routeID!
            }

            let routeFetchRequest: NSFetchRequest<GtfsRoute> = GtfsRoute.fetchRequest()
            routeFetchRequest.predicate = NSPredicate(format: "routeID IN %@", routeIDs)
            let routes = try managedObjectContext.fetch(routeFetchRequest)
            stop.addToTransports(NSSet(array: routes))
        } catch {
            print(error)
        }
    }
}
