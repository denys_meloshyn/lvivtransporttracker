//
// Created by Denys Meloshyn on 2019-01-26.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import MapKit

protocol StopMapViewProtocol: class {
    func updateAllAnnotations()
    func add(annotations: [MKAnnotation])
    func remove(annotations: [MKAnnotation])
    func addStop(annotation: StopAnnotation)
}

class StopMapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!

    private var presenter: StopMapPresenterProtocol!
    private let stopAnnotationIdentifier = "StopAnnotationView"
    private let clusterAnnotationIdentifier = "ClusterAnnotationView"
    private let transportAnnotationIdentifier = "TransportAnnotationView"

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.register(StopAnnotationView.self, forAnnotationViewWithReuseIdentifier: stopAnnotationIdentifier)
        mapView.register(ClusterAnnotationView.self, forAnnotationViewWithReuseIdentifier: clusterAnnotationIdentifier)
        mapView.register(TransportAnnotationView.self, forAnnotationViewWithReuseIdentifier: transportAnnotationIdentifier)

        let router = StopMapRouter(viewController: self)
        let interactor = StopMapInteractor(managedObjectContext: CoreDataStack.shared.persistentContainer.viewContext,
                                           logger: DependencyContainer.instance.logger,
                                           analytics: DependencyContainer.instance.analytics)
        let presenter = StopMapPresenter(view: self, interactor: interactor, router: router)
        self.presenter = presenter
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        mapView.selectedAnnotations.forEach { annotation in
            mapView.deselectAnnotation(annotation, animated: false)
        }
    }
}

extension StopMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKClusterAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: clusterAnnotationIdentifier) as? ClusterAnnotationView
            return annotationView
        } else if annotation is StopAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: stopAnnotationIdentifier) as? StopAnnotationView
            annotationView?.clusteringIdentifier = "ClusterStop"
            return annotationView
        } else if annotation is TransportAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: transportAnnotationIdentifier) as? TransportAnnotationView
            annotationView?.updateAnnotation()
            return annotationView
        }

        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        presenter.stopPressed(annotation: view.annotation!)
        mapView.deselectAnnotation(view.annotation, animated: false)
    }
}

extension StopMapViewController: StopMapViewProtocol {
    func addStop(annotation: StopAnnotation) {
        mapView.addAnnotation(annotation)
        mapView.showAnnotations(mapView.annotations, animated: false)
    }

    func updateAllAnnotations() {
        mapView.annotations.forEach { annotation in
            let view = mapView.view(for: annotation)

            if let transportView = view as? TransportAnnotationView {
                transportView.updateAnnotation()
            }
        }
    }

    func remove(annotations: [MKAnnotation]) {
        mapView.removeAnnotations(annotations)
    }

    func add(annotations: [MKAnnotation]) {
        mapView.addAnnotations(annotations)
    }
}
