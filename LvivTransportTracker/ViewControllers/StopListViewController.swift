//
//  StopListViewController.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 24/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit
import RxSwift
import AlignedCollectionViewFlowLayout

protocol BaseViewProtocol: class {
    func showPage(title: String?)
}

protocol StopListViewProtocol: BaseViewProtocol {
    func refreshItems()
    func emptyMessage(isHidden: Bool)
    func refreshItems(at indexPath: IndexPath)
}

class StopListViewController: UIViewController, BaseViewProtocol {
    @IBOutlet private var collectionView: UICollectionView!
    @IBOutlet private var emptyView: EmptyContainerView!

    private let disposeBag = DisposeBag()
    private let numberOfDummyItems = 3
    private var sectionViewSize = [Int: CGSize]()
    private var sectionViews = [UICollectionReusableView: Int]()

    var presenter: StopListPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        emptyView.titleLabel.text = R.string.localizable.youDontHaveAnyContentPleaseGoToSettingsAndPressUpdate()
        configureSearchController()
        tabBarItem = UITabBarItem(title: LocalisationManager.stops(),
                                  image: R.image.tabbar_stop_icon(),
                                  selectedImage: R.image.tabbar_stop_icon())

        configureCollectionView()
        
        ConstantsSwift.isUpdated.subscribe { [weak self] event in
            self?.configureStack()
            self?.navigationController?.popToRootViewController(animated: false)
        }.disposed(by: disposeBag)
    }
    
    func configureStack() {
        let router = StopListRouter(viewController: self)
        let interactor = StopListInteractor(managedObjectContext: CoreDataStack.shared.persistentContainer.viewContext,
                                            logger: DependencyContainer.instance.logger,
                                            analytics: DependencyContainer.instance.analytics)
        interactor.config()
        let listPresenter = StopListPresenter(view: self, interactor: interactor, router: router)
        presenter = listPresenter
        interactor.delegate = listPresenter
    }

    @IBAction func locationButtonPressed() {
        presenter.locationPressed()
    }

    private func configureSearchController() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = search
    }

    private func configureCollectionView() {
        collectionView.register(UINib(resource: R.nib.stopListSectionView),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: R.reuseIdentifier.stopListSectionView.identifier)

        let leftCollectionFlowLayout = collectionView.collectionViewLayout as! AlignedCollectionViewFlowLayout
        leftCollectionFlowLayout.horizontalAlignment = .left
        leftCollectionFlowLayout.sectionHeadersPinToVisibleBounds = true

        var reservedSpace = leftCollectionFlowLayout.sectionInset.left
        reservedSpace += leftCollectionFlowLayout.sectionInset.right
        var availableWidth = collectionView.frame.width
        availableWidth -= reservedSpace
        availableWidth -= CGFloat(numberOfDummyItems - 1) * leftCollectionFlowLayout.minimumInteritemSpacing
        let size = floor(availableWidth / CGFloat(numberOfDummyItems))
        leftCollectionFlowLayout.itemSize = CGSize(width: size, height: size)
    }
}

extension StopListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        presenter.searchChanged(searchController.searchBar.text ?? "")
    }
}

extension StopListViewController: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.interactor.numberOfSections()
    }

    public func collectionView(_ collectionView: UICollectionView,
                               viewForSupplementaryElementOfKind kind: String,
                               at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: R.reuseIdentifier.stopListSectionView,
                                                                   for: indexPath)!
        view.title.text = presenter.title(for: indexPath.section)
        view.delegate = self

        sectionViews[view] = indexPath.section

        return view
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rows = presenter.interactor.numberOfItems(for: section)
        if rows == 0 {
            return numberOfDummyItems
        }

        return rows
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.stopListCollectionViewCell,
                                           for: indexPath)!
        let model = presenter.model(for: indexPath)
        cell.configure(with: model)

        return cell
    }
}

extension StopListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        didEndDisplayingSupplementaryView view: UICollectionReusableView,
                        forElementOfKind elementKind: String,
                        at indexPath: IndexPath) {
        sectionViews.removeValue(forKey: view)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.selectStop(at: indexPath)
    }
}

extension StopListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        guard let size = sectionViewSize[section] else {
            let height: CGFloat = 50
            let view = R.nib.stopListSectionView.firstView(owner: nil)!
            view.frame = CGRect(x: 0, y: 0, width: collectionView.frame.width, height: height)
            view.title.text = presenter.title(for: section)
            view.title.layoutIfNeeded()

            let newSize = CGSize(width: collectionView.frame.width, height: max(height, view.title.frame.height))
            sectionViewSize[section] = newSize

            return newSize
        }

        return size
    }
}

extension StopListViewController: StopListViewProtocol {
    func showPage(title: String?) {
        navigationItem.title = title
    }

    func emptyMessage(isHidden: Bool) {
        emptyView.isHidden = isHidden
    }

    func refreshItems(at indexPath: IndexPath) {
        if collectionView.indexPathsForVisibleSupplementaryElements(ofKind: UICollectionView.elementKindSectionHeader).contains(indexPath) {
            collectionView.reloadData()
        }
    }

    func refreshItems() {
        collectionView.reloadData()
    }
}

extension StopListViewController: StopListSectionViewDelegate {
    func actionButtonPressed(sender: StopListSectionView) {
        guard let index = sectionViews[sender] else {
            return
        }

        presenter.sectionPressed(index)
    }
}
