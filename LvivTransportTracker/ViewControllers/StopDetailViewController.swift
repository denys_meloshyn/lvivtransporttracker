//
//  StopDetailViewController.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 19.08.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

import HTTPNetworking

class StopDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tableView: UITableView!

    var currentStopID: NSManagedObjectID!

    var timer: Timer!
    private var stop: GtfsStop!
    let analytics = DependencyContainer.instance.analytics
    private var fetchController: NSFetchedResultsController<StopTransportTime>!
    private let managedObjectContext = TransportManager.sharedInstance().managedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()

        stop = managedObjectContext.object(with: currentStopID) as? GtfsStop
        createStopTransportTimesIfNeeded(stop: stop)

        let stopTransportTimesFetchRequest: NSFetchRequest<StopTransportTime> = StopTransportTime.fetchRequest()
        stopTransportTimesFetchRequest.predicate = NSPredicate(format: "stop.stopID == %@", stop.stopID!)
        stopTransportTimesFetchRequest.sortDescriptors = [NSSortDescriptor(key: "stop.name", ascending: true)]
        fetchController = NSFetchedResultsController(fetchRequest: stopTransportTimesFetchRequest,
                                                     managedObjectContext: managedObjectContext,
                                                     sectionNameKeyPath: nil,
                                                     cacheName: nil)
        fetchController.delegate = self

        do {
            try fetchController.performFetch()
        } catch {
            print("\(error)")
        }

        navigationItem.title = stop.name
        configureRightBarItems()

        configureTable()

        timer = Timer(timeInterval: 5.0, repeats: true) { _ in
            self.tableView.reloadData()
        }

        RunLoop.current.add(timer, forMode: .default)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }

    private func configureTable() {
        tableView.estimatedRowHeight = 160.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(R.nib.transportTableViewCell)
    }

    private func configureRightBarItems() {
        let image: UIImage?
        if (FavouriteRepository.shared.isFavourite(type: .stop, itemID: stop.stopID ?? "")) {
            image = R.image.favorite_selected()
        } else {
            image = R.image.favorite_not_selected()
        }

        let favoriteButton = UIBarButtonItem(image: image,
                                             style: .plain,
                                             target: self,
                                             action: #selector(StopDetailViewController.addToFavorite))

        let mapButton = UIBarButtonItem(image: R.image.show_route_icon(),
                                        style: .plain,
                                        target: self,
                                        action: #selector(StopDetailViewController.openMap))

        navigationItem.rightBarButtonItems = [favoriteButton, mapButton]
    }

    private func createStopTransportTimesIfNeeded(stop: GtfsStop) {
        for transport in stop.sortedTransport() {
            _ = TransportManagerSwift.instance.createOrFetchStopTransportTimes(for: currentStopID,
                                                                               transportID: transport.objectID,
                                                                               managedObjectContext: managedObjectContext)
        }
    }


    @objc func addToFavorite() {
        FavouriteRepository.shared.changeFavouriteStatus(type: .stop, itemID: stop.stopID ?? "")
        let isFavourite = FavouriteRepository.shared.isFavourite(type: .stop, itemID: stop.stopID ?? "")
        analytics.log(.stopDetailFavourite, parameters: ["value": isFavourite])

        AnalyticsManager.sharedInstance().addRemoveTransport(fromFavourite: kScreenRouteDetail,
                                                             name: stop?.name,
                                                             isFavourite: isFavourite)
        configureRightBarItems()
    }

    @objc func openMap() {
        analytics.log(.stopDetailOpenMap, parameters: ["name": stop.name ?? ""])
        let stopDetailMapViewController = R.storyboard.main.stopDetailMapViewController()!
        stopDetailMapViewController.stopID = currentStopID

        navigationController?.pushViewController(stopDetailMapViewController, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = fetchController.sections?.first

        return section?.numberOfObjects ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.transportTableViewCell, for: indexPath)!
        let model = fetchController.object(at: indexPath)

        let timeTable = model.nextTime(for: Date())
        var nextTimeTable: TimeTable?
        if let checkedTimeTable = timeTable {
            nextTimeTable = model.nextTime(after: checkedTimeTable)
        }
        cell.configure(model: model.transport!, arrivingTime: timeTable, nextTimeTable: nextTimeTable)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

        let model = fetchController.object(at: indexPath)
        analytics.log(.stopDetailOpenTransport, parameters: ["stop": stop.name ?? "",
                                                             "transport": model.transport?.shortName ?? ""])

        let viewController = R.storyboard.main.transportDetailViewController()!
        viewController.stopID = model.stop!.objectID
        viewController.transportID = model.transport!.objectID

        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension StopDetailViewController: NSFetchedResultsControllerDelegate {
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                           didChange anObject: Any,
                           at indexPath: IndexPath?,
                           for type: NSFetchedResultsChangeType,
                           newIndexPath: IndexPath?) {
        tableView.reloadData()
    }
}
