//
//  TransportDetailViewController.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 08/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit
import Rswift
import RxSwift
import RxCocoa

protocol TransportDetailViewProtocol: BaseViewProtocol {
    func refreshStops()
    func refreshTimetableMinutesAndShowFirst()
    func changePickerVisibility(isHidden: Bool)
    func showTimeTableActionButton(title: String)
}

class TransportDetailViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var pickerView: UIPickerView!

    var stopID: NSManagedObjectID!
    var transportID: NSManagedObjectID!

    private let disposeBag = DisposeBag()
    private var presenter: TransportDetailPresenter!
    private var interactor: TransportDetailInteractor!
    private let managedObjectContext = TransportManager.sharedInstance().managedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(R.nib.routeStopTableViewCell)
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableView.automaticDimension

        let router = TransportDetailRouter(viewController: self)
        interactor = TransportDetailInteractor(stopID: stopID,
                                               transportID: transportID,
                                               managedObjectContext: managedObjectContext,
                                               logger: DependencyContainer.instance.logger,
                                               analytics: DependencyContainer.instance.analytics)
        presenter = TransportDetailPresenter(view:self, interactor: interactor, router: router)

        pickerView.selectRow(presenter.selectedTimeTableIndexPath.hourIndex, inComponent: 0, animated: false)
        pickerView.selectRow(presenter.selectedTimeTableIndexPath.minuteIndex, inComponent: 1, animated: false)
    }

    @IBAction func pickerOkAction() {
        presenter.selectTimetableAction()
    }

    @IBAction func pickerCancelAction() {
        presenter.cancelSelectingTimetableAction()
    }
}

extension TransportDetailViewController: TransportDetailViewProtocol {
    func showPage(title: String?) {
        navigationItem.title = title
    }

    func refreshStops() {
        tableView.reloadData()
    }

    func refreshTimetableMinutesAndShowFirst() {
        pickerView.reloadComponent(1)
        pickerView.selectRow(0, inComponent: 1, animated: true)
    }

    func changePickerVisibility(isHidden: Bool) {
        stackView.isHidden = isHidden
    }

    func showTimeTableActionButton(title: String) {
        let timeTableBarButtonItem = BarButtonItem(with: title, style: .plain) { [weak self] item in
            self?.presenter.timeTablePressed()
        }
        
        let mapTableBarButtonItem = UIBarButtonItem(image: R.image.show_route_icon()!, style: .plain, target: nil, action: nil)
        
        mapTableBarButtonItem.rx.tap.subscribe { event in
            self.presenter.mapPressed()
        }.disposed(by: disposeBag)
        
        navigationItem.rightBarButtonItems = [timeTableBarButtonItem, mapTableBarButtonItem]
        navigationController?.navigationBar.tintColor = Constants.defaultApperanceColor()
    }
}

extension TransportDetailViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return presenter.numberOfHours()
        }

        return presenter.numberOfMinutes()
    }
}

extension TransportDetailViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.timeTableTitle(for: row, component: component)
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        presenter.newTimeTableSelected(for: row, component: component)
    }
}

extension TransportDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = presenter.cellViewModel(for: indexPath)
        if model.isFetching {
            return 89
        }

        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.stopSelected(at: indexPath)
    }
}

extension TransportDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor.numberOfStops()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.routeStopTableViewCell, for: indexPath)!
        let model = presenter.cellViewModel(for: indexPath)
        cell.configure(with: model)

        return cell
    }
}
