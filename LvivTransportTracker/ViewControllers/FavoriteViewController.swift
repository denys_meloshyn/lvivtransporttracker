//
// Created by Denys Meloshyn on 2019-01-20.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import RxSwift

protocol FavoriteViewProtocol: BaseViewProtocol {
    func refresh()
    func showEmptyMessage()
    func showFavoriteList()
}

class FavoriteViewController: UIViewController {
    @IBOutlet var emptyListView: UIView!
    @IBOutlet var tableView: UITableView!

    private let disposeBag = DisposeBag()
    private var presenter: FavoritePresenterProtocol!

    override func awakeFromNib() {
        super.awakeFromNib()

        tabBarItem = UITabBarItem(title: R.string.localizable.favourite(),
                                  image: R.image.tabbar_favorite_not_selected_icon()!,
                                  selectedImage: R.image.tabbar_favorite_not_selected_icon()!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(R.nib.favouriteStopTableViewCell)
        tableView.register(R.nib.transportShortTableViewCell)
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableView.automaticDimension

        ConstantsSwift.isUpdated.subscribe { [weak self] event in
            self?.configureStack()
            self?.navigationController?.popToRootViewController(animated: false)
        }.disposed(by: disposeBag)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        presenter.viewWillAppear(animated)
    }

    func configureStack() {
        let router = FavoriteRouter(viewController: self)
        let interactor = FavoriteInteractor(managedObjectContext: CoreDataStack.shared.persistentContainer.viewContext,
                                            logger: DependencyContainer.instance.logger,
                                            analytics: DependencyContainer.instance.analytics)
        presenter = FavoritePresenter(view: self, interactor: interactor, router: router)
    }
}

extension FavoriteViewController: FavoriteViewProtocol {
    func showPage(title: String?) {
        navigationItem.title = title
    }

    func refresh() {
        tableView.reloadData()
    }

    func showEmptyMessage() {
        tableView.isHidden = true
        emptyListView.isHidden = false
    }

    func showFavoriteList() {
        tableView.isHidden = false
        emptyListView.isHidden = true
    }
}

extension FavoriteViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.interactor.numberOfSections()
    }

    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter.title(for: section)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.interactor.numberOfItems(at: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        let model = presenter.viewModel(for: indexPath)

        switch model {
        case .stop (let name):
            let stopCell = tableView.dequeueReusableCell(withIdentifier: R.nib.favouriteStopTableViewCell, for: indexPath)!
            stopCell.textLabel?.text = name
            cell = stopCell

        case .transport (let name, let color):
            let transportCell = tableView.dequeueReusableCell(withIdentifier: R.nib.transportShortTableViewCell, for: indexPath)!
            transportCell.routeNumberLabel.text = name
            transportCell.contentView.backgroundColor = color
            cell = transportCell
        }

        return cell
    }
}

extension FavoriteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        presenter.didSelectItem(at: indexPath)
    }
}
