//
//  TransportMapViewController.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 27/11/2019.
//  Copyright © 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import RxSwift
import HTTPNetworking

class TransportMapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!
    
    private let disposeBag = DisposeBag()
    private var transports = [GtfsRoute]()
    private var allAnnotations = [TransportAnnotation]()
    private let stopAnnotationIdentifier = "StopAnnotationView"
    private let transportAnnotationIdentifier = "TransportAnnotationView"
    
    var tripID = ""
    var transportID = ""
    var stops = [GtfsStop]()
    var shapes = [CLLocationCoordinate2D]()
    var transportLocation = BehaviorSubject<TransitRealtime_FeedMessage?>(value: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let fetchRequest: NSFetchRequest<GtfsRoute> = GtfsRoute.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "routeID == %@", transportID)
            transports = try CoreDataStack.shared.persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            DependencyContainer.instance.logger.log(.error, "\(error)")
        }

        mapView.register(StopAnnotationView.self, forAnnotationViewWithReuseIdentifier: stopAnnotationIdentifier)
        mapView.register(TransportAnnotationView.self, forAnnotationViewWithReuseIdentifier: transportAnnotationIdentifier)
        
        let polyline = MKPolyline(coordinates: shapes, count: shapes.count)
        mapView.addOverlay(polyline)
        
        stops.forEach { stop in
            let coordinate = CLLocationCoordinate2DMake(stop.latitude?.doubleValue ?? 0,
                                                        stop.longitude?.doubleValue ?? 0)
            let annotation = StopAnnotation(coordinate: coordinate, title: stop.name ?? "", stopID: stop.objectID)
            self.mapView.addAnnotation(annotation)
            self.mapView.showAnnotations(mapView.annotations, animated: false)
        }
        
        transportLocation.subscribe { [weak self] event in
            switch event {
            case .next(let model):
                guard let data = model, let strongSelf = self else {
                    return
                }
                let (add, remove, all) = strongSelf.configureTransportAnnotations(for: data, with: strongSelf.allAnnotations)
                strongSelf.allAnnotations = all
                strongSelf.add(annotations: add)
                strongSelf.remove(annotations: remove)
                strongSelf.updateAllAnnotations()
            default:
                break
            }
        }.disposed(by: disposeBag)
        
        loadContent()
    }
    
    func configureTransportAnnotations(for data: TransitRealtime_FeedMessage,
                                       with previousAnnotations: [TransportAnnotation]) -> (new: [TransportAnnotation], remove: [TransportAnnotation], all: [TransportAnnotation]) {
        var newAnnotations = [TransportAnnotation]()
        var allAnnotations = [TransportAnnotation]()
        let annotationsToRemove = NSMutableArray(array: previousAnnotations)
        
        let feedItems = data.entity.filter { entity -> Bool in
            let filter = transports.filter { route -> Bool in
                return route.routeID == entity.vehicle.trip.routeID
            }

            return !filter.isEmpty
        }

        for item in feedItems {
            let annotations = previousAnnotations.filter { annotation -> Bool in
                return annotation.currentModel.routeID == item.vehicle.vehicle.id
            }

            let transportAnnotation: TransportAnnotation
            if let annotation = annotations.first {
                annotationsToRemove.remove(annotation)
                allAnnotations.append(annotation)
                transportAnnotation = annotation
            } else {
                let filter = transports.filter { (route: GtfsRoute) -> Bool in
                    return route.routeID == item.vehicle.trip.routeID
                }

                transportAnnotation = TransportAnnotation()
                let position = TransportPosition()
                position.licensePlate = item.vehicle.vehicle.licensePlate
                position.transportName = filter.first?.shortName
                position.routeObjectID = filter.first?.objectID
                position.routeColor = Utilities.color(for: filter.first?.routeType ?? "")
                position.routeID = item.vehicle.vehicle.id

                transportAnnotation.currentModel = position
                newAnnotations.append(transportAnnotation)
                allAnnotations.append(transportAnnotation)
            }

            transportAnnotation.currentModel.angle = CGFloat(item.vehicle.position.bearing)
            transportAnnotation.coordinate = CLLocationCoordinate2D(latitude: Double(item.vehicle.position.latitude),
                                                                    longitude: Double(item.vehicle.position.longitude))
        }

        return (newAnnotations, annotationsToRemove as! [TransportAnnotation], allAnnotations)
    }
    
    func loadContent() {
        let completable = Single<TransitRealtime_FeedMessage>.create { event in
            let url = URL(string: "http://track.ua-gis.com/gtfs/lviv/vehicle_position")!
            let request = URLRequest(url: url)

            let task = HTTPNetwork.instance.load(request) { data, response, error in
                guard let data = data else {
                    event(.error(NSError(domain: "empty data", code: -1)))
                    return
                }

                do {
                    let feed = try TransitRealtime_FeedMessage(serializedData: data)
                    event(.success(feed))
                } catch {
                    event(.error(error))
                }
            }

            return Disposables.create {
                task.cancel()
            }
        }.delay(RxTimeInterval.seconds(UserSettings.sharedInstance().updateInterval), scheduler: MainScheduler.instance)

        completable.subscribe { [weak self] event in
            switch event {
            case .success(let model):
                self?.transportLocation.onNext(model)
            case .error:
                break
            }
            
            self?.loadContent()
        }.disposed(by: disposeBag)
    }

    func updateAllAnnotations() {
        mapView.annotations.forEach { annotation in
            let view = mapView.view(for: annotation)

            if let transportView = view as? TransportAnnotationView {
                transportView.updateAnnotation()
            }
        }
    }

    func add(annotations: [MKAnnotation]) {
        mapView.addAnnotations(annotations)
    }
}

extension TransportMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is TransportAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: transportAnnotationIdentifier) as? TransportAnnotationView
            annotationView?.updateAnnotation()
            annotationView?.rightCalloutAccessoryView = nil
            return annotationView
        } else if annotation is StopAnnotation {
            let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: stopAnnotationIdentifier) as? StopAnnotationView
            annotationView?.clusteringIdentifier = nil
            annotationView?.rightCalloutAccessoryView = nil
            return annotationView
        }

        return nil
    }
    
    func remove(annotations: [MKAnnotation]) {
        mapView.removeAnnotations(annotations)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        mapView.deselectAnnotation(view.annotation, animated: false)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = Utilities.color(for: transports.first?.routeType ?? "").withAlphaComponent(0.8)
        polylineRenderer.lineWidth = 5
        
        return polylineRenderer
    }
}

