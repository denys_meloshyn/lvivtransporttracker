//
// Created by Denys Meloshyn on 2019-01-06.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import RxSwift

protocol TransportListViewProtocol: BaseViewProtocol {
    func refreshItems()
    func emptyMessage(isHidden: Bool)
    func showTabBarItem(item: UITabBarItem)
}

class TransportListViewController: UIViewController {
    @IBOutlet var emptyView: EmptyContainerView!
    @IBOutlet var collectionView: UICollectionView!

    private let disposeBag = DisposeBag()
    private var widths = [[CGFloat]]()
    private var presenter: TransportListPresenterProtocol!

    override func awakeFromNib() {
        super.awakeFromNib()

        showTabBarItem(item: UITabBarItem(title: LocalisationManager.transport(),
                                          image: R.image.tabbar_transport_icon(),
                                          selectedImage: R.image.tabbar_transport_icon()))
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        ConstantsSwift.isUpdated.subscribe { [weak self] event in
            self?.configureStack()
            self?.navigationController?.popToRootViewController(animated: false)
        }.disposed(by: disposeBag)
    }

    func configureStack() {
        let interactor = TransportListInteractor(managedObjectContext: TransportManager.sharedInstance().managedObjectContext,
                                                 logger: DependencyContainer.instance.logger,
                                                 analytics: DependencyContainer.instance.analytics)
        let router = TransportListRouter(viewController: self)
        presenter = TransportListPresenter(view: self, interactor: interactor, router: router)

        emptyView.titleLabel.text = R.string.localizable.youDontHaveAnyContentPleaseGoToSettingsAndPressUpdate()
        collectionView.register(R.nib.transportShortCollectionViewCell)
        collectionView.register(R.nib.transportCollectionReusableView,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader)
        widths = calculateCellWidth()
    }

    private func calculateCellWidth() -> [[CGFloat]] {
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let maxWidth = view.frame.width - collectionViewLayout.sectionInset.left - collectionViewLayout.sectionInset.right

        let result = (0..<presenter.interactor.numberOfTransportTypes()).map { i -> [CGFloat] in
            return (0..<presenter.interactor.numberOfTransports(for: i)).map { j -> CGFloat in
                let indexPath = IndexPath(row: j, section: i)
                let cell = R.nib.transportShortCollectionViewCell.firstView(owner: nil)!
                cell.frame.size = CGSize(width: collectionView.frame.width, height: 44)
                cell.configure(with: presenter.modelForCell(at: indexPath))

                return min(maxWidth, cell.width())
            }
        }

        return result
    }
}

extension TransportListViewController: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.interactor.numberOfTransportTypes()
    }

    public func collectionView(_ collectionView: UICollectionView,
                               viewForSupplementaryElementOfKind kind: String,
                               at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: R.reuseIdentifier.transportCollectionReusableView,
                                                                   for: indexPath)!
        view.titleLabel.text = presenter.titleForSection(index: indexPath.section)

        return view
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.interactor.numberOfTransports(for: section)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.transportShortCollectionViewCell,
                                                      for: indexPath)!
        let model = presenter.modelForCell(at: indexPath)
        cell.configure(with: model)

        return cell
    }
}

extension TransportListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.transportSelected(at: indexPath)
    }
}

extension TransportListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 28)
    }

    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: widths[indexPath.section][indexPath.row], height: 40)
    }
}

extension TransportListViewController: TransportListViewProtocol {
    func showTabBarItem(item: UITabBarItem) {
        tabBarItem = item
    }

    func showPage(title: String?) {
        navigationItem.title = title
    }

    func emptyMessage(isHidden: Bool) {
        emptyView.isHidden = isHidden
    }

    func refreshItems() {
        widths = calculateCellWidth()
        collectionView.reloadData()
    }
}
