//
// Created by Denys Meloshyn on 2019-01-27.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol StopDetailMapViewProtocol: class {
    func updateAllAnnotations()
    func add(annotation: MKAnnotation)
    func add(annotations: [MKAnnotation])
    func move(to annotation: MKAnnotation)
    func remove(annotations: [MKAnnotation])
}

class StopDetailMapViewController: UIViewController {
    @IBOutlet var mapView: MKMapView!

    var stopID: NSManagedObjectID!

    private var presenter: StopDetailMapPresenterProtocol!
    private let stopAnnotationIdentifier = "StopAnnotationView"
    private let transportAnnotationIdentifier = "TransportAnnotationView"

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.register(StopAnnotationView.self, forAnnotationViewWithReuseIdentifier: stopAnnotationIdentifier)
        mapView.register(TransportAnnotationView.self, forAnnotationViewWithReuseIdentifier: transportAnnotationIdentifier)

        let interactor = StopDetailMapInteractor(managedObjectContext: TransportManager.sharedInstance().managedObjectContext,
                                                 stopID: stopID,
                                                 logger: DependencyContainer.instance.logger)
        let presenter = StopDetailMapPresenter(view: self, interactor: interactor)
        self.presenter = presenter
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        mapView.selectedAnnotations.forEach { annotation in
            mapView.deselectAnnotation(annotation, animated: false)
        }
    }
}

extension StopDetailMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView?

        if let _ = annotation as? StopAnnotation {
            let stopAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: stopAnnotationIdentifier) as? StopAnnotationView
            annotationView = stopAnnotationView
        } else {
            let transportAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: transportAnnotationIdentifier) as? TransportAnnotationView
            transportAnnotationView?.updateAnnotation()
            annotationView = transportAnnotationView
        }

        return annotationView
    }

    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        mapView.deselectAnnotation(view.annotation, animated: false)
    }
}

extension StopDetailMapViewController: StopDetailMapViewProtocol {
    func add(annotation: MKAnnotation) {
        mapView.addAnnotation(annotation)
    }

    func updateAllAnnotations() {
        mapView.annotations.forEach { annotation in
            let view = mapView.view(for: annotation)

            if let transportView = view as? TransportAnnotationView {
                transportView.updateAnnotation()
            }

            view?.rightCalloutAccessoryView = nil
        }
    }

    func remove(annotations: [MKAnnotation]) {
        mapView.removeAnnotations(annotations)
    }

    func add(annotations: [MKAnnotation]) {
        mapView.addAnnotations(annotations)
    }

    func move(to annotation: MKAnnotation) {
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}
