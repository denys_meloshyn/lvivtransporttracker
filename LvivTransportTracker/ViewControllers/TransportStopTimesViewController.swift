//
// Created by Denys Meloshyn on 2019-01-12.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

protocol TransportStopTimesView: BaseViewProtocol {
    func updateFavourite(image: UIImage)
}

class TransportStopTimesViewController: UIViewController {
    @IBOutlet var collectionView: UICollectionView!

    var transportID: NSManagedObjectID!

    private var presenter: TransportStopTimesPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.register(R.nib.transportStopTimesCollectionViewCell)

        let interactor = TransportStopTimesInteractor(managedObjectContext: TransportManager.sharedInstance().managedObjectContext,
                                                      transportID: transportID,
                                                      logger: DependencyContainer.instance.logger, analytics: DependencyContainer.instance.analytics)
        let router = TransportStopTimesRouter(viewController: self)
        presenter = TransportStopTimesPresenter(view: self, interactor: interactor, router: router)
    }
}

extension TransportStopTimesViewController: TransportStopTimesView {
    func showPage(title: String?) {
        navigationItem.title = title
    }

    @objc func updateFavouriteAction() {
        presenter.favouritePressed()
    }

    func updateFavourite(image: UIImage) {
        let favoriteButton = UIBarButtonItem(image: image,
                                             style: .plain,
                                             target: self,
                                             action: #selector(TransportStopTimesViewController.updateFavouriteAction))
        navigationItem.rightBarButtonItems = [favoriteButton]
    }
}

extension TransportStopTimesViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.interactor.numberOfStops()
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.transportStopTimesCollectionViewCell,
                                                      for: indexPath)!
        let model = presenter.modelForCell(at: indexPath)
        cell.configure(with: model)
        cell.delegate = self

        return cell
    }
}

extension TransportStopTimesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension TransportStopTimesViewController: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 146)
    }
}

extension TransportStopTimesViewController: TransportStopTimesCollectionViewCellDelegate {
    func detailPressed(sender: TransportStopTimesCollectionViewCell) {
        let indexPath = collectionView.indexPath(for: sender)!
        presenter.stopDetailPressed(at: indexPath)
    }
}
