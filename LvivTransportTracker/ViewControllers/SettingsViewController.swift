//
// Created by Denys Meloshyn on 2019-01-19.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import UIKit
import MessageUI

protocol SettingsViewProtocol: BaseViewProtocol {
    func refreshView(with model: SettingsViewModel)
    func showTabBarItem(title: String, image: UIImage)
    func sendEmail(with recipient: String, subject: String)
    func showUpdateMessage(_ title: String, message: String, okMessage: String, cancelMessage: String, handler: @escaping ((UIAlertAction) -> Void))
    func showErrorMessageDuringLoading(title: String, message: String, tryAgainMessage: String, cancelMessage: String, tryAgainHandler: @escaping ((UIAlertAction) -> Void))
}

struct SettingsViewModel {
    let email: String
    let update: String
    let updateFrequency: Double
    let lastUpdate: String
    let loadingProgress: Double?
    let loadingProgressText: String?
}

class SettingsViewController: UIViewController {
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var timeStepper: UIStepper!
    @IBOutlet var separatorLines: [UIView]!
    @IBOutlet var lastUpdateLabel: UILabel!
    @IBOutlet var sendEmailButton: UIButton!
    @IBOutlet var emailSupportLabel: UILabel!
    @IBOutlet var updateTimeIntervalLabel: UILabel!

    private var progressView: ProgressAnimationView?
    private var presenter: SettingsPresenterProtocol!
    private var mailComposeViewController: MFMailComposeViewController?

    override func awakeFromNib() {
        super.awakeFromNib()

        showTabBarItem(title: LocalisationManager.settings(), image: R.image.tabbar_settings_icon()!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if MFMailComposeViewController.canSendMail() {
            mailComposeViewController = MFMailComposeViewController()
        }

        let favoriteInteractor = FavoriteInteractor(managedObjectContext: TransportManager.sharedInstance().managedObjectContext,
                                                    logger: DependencyContainer.instance.logger,
                                                    analytics: DependencyContainer.instance.analytics)
        let interactor = SettingsInteraction(managedObjectContext: CoreDataStack.shared.persistentContainer.viewContext,
                                             favoriteInteractor: favoriteInteractor,
                                             logger: DependencyContainer.instance.logger,
                                             analytics: DependencyContainer.instance.analytics)
        let presenter = SettingsPresenter(view: self, interactor: interactor)
        interactor.delegate = presenter
        
        self.presenter = presenter

        timeStepper.stepValue = Double(defaultTimeIntervalUpdate)
        timeStepper.minimumValue = Double(defaultTimeIntervalUpdate)

        refreshView(with: presenter.viewModel())
    }

    @IBAction func updateButtonAction() {
        presenter.updatePressed()
    }

    @IBAction func timeStepperValueChangedAction(sender: UIStepper) {
        presenter.timeStepperChanged(with: sender.value)
    }

    @IBAction func sendEmailButtonAction() {
        presenter.sendEmailPressed()
    }

    @IBAction func followOnFBAction() {
        presenter.followOnFBPressed()
    }
}

extension SettingsViewController: SettingsViewProtocol {
    func showTabBarItem(title: String, image: UIImage) {
        tabBarItem = UITabBarItem(title: title, image: image, selectedImage: image)
    }

    func showPage(title: String?) {
        navigationItem.title = title
    }

    func showUpdateMessage(_ title: String, message: String, okMessage: String, cancelMessage: String, handler: @escaping ((UIAlertAction) -> Void)) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: cancelMessage, style: .cancel)
        let okAction = UIAlertAction(title: okMessage, style: .default, handler: handler)

        alertController.addAction(cancelAction)
        alertController.addAction(okAction)

        present(alertController, animated: true)
    }

    func refreshView(with viewModel: SettingsViewModel) {
        timeStepper.value = viewModel.updateFrequency

        lastUpdateLabel.text = viewModel.lastUpdate
        updateTimeIntervalLabel.text = viewModel.update

        let attributes: [NSAttributedString.Key: Any] = [.underlineStyle: NSAttributedString.Key.underlineStyle,
                                                         .foregroundColor: Constants.defaultApperanceColor()]
        let attributedText = NSAttributedString(string: viewModel.email, attributes: attributes)
        emailSupportLabel.attributedText = attributedText

        if let loadingProgressText = viewModel.loadingProgressText,
           let loadingProgress = viewModel.loadingProgress {
            if progressView == nil {
                progressView = ProgressAnimationView.show(in: tabBarController?.view)
                progressView?.alpha = 0

                UIView.animate(withDuration: 0.5) {
                    self.progressView?.alpha = 1
                }
            }

            progressView?.updateProgress(CGFloat(loadingProgress), withText: loadingProgressText)
        } else {
            guard progressView != nil else {
                return
            }

            UIView.animate(withDuration: 0.5,
                           animations: { self.progressView?.alpha = 0  },
                           completion: { _ in
                               self.progressView = nil
                               ProgressAnimationView.remove(from: self.tabBarController?.view)
                           })
        }
    }

    func sendEmail(with recipient: String, subject: String) {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }

        let mailViewController = MFMailComposeViewController()
        mailViewController.mailComposeDelegate = self
        mailViewController.setToRecipients([recipient])
        mailViewController.setSubject(subject)
        mailViewController.modalTransitionStyle = .coverVertical
        mailViewController.navigationBar.tintColor = Constants.defaultApperanceColor()

        mailComposeViewController = mailViewController
        present(mailViewController, animated: true)
    }

    func showErrorMessageDuringLoading(title: String, message: String, tryAgainMessage: String, cancelMessage: String, tryAgainHandler: @escaping ((UIAlertAction) -> Void)) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: cancelMessage, style: .cancel)
        let tryAgainAction = UIAlertAction(title: tryAgainMessage, style: .default, handler: tryAgainHandler)

        alertController.addAction(cancelAction)
        alertController.addAction(tryAgainAction)

        present(alertController, animated: true)
    }
}

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        mailComposeViewController?.dismiss(animated: true)
    }
}
