//
//  StoptEntity.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 26/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoptEntity : NSObject

@property (nonatomic) BOOL isFavorite;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *stopID;
@property (nonatomic, strong) NSString *type;

//- (void)configureWithCoreDataModel:(Stop *)model;

@end
