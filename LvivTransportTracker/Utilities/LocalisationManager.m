//
//  LocalisationManager.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 02.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "LocalisationManager.h"

@implementation LocalisationManager

+ (NSString *)tram
{
    return NSLocalizedString(@"tram", @"Tram");
}

+ (NSString *)bus
{
    return NSLocalizedString(@"bus", @"Bus");
}

+ (NSString *)trolleybus
{
    return NSLocalizedString(@"trolleybus", @"Trolleybus");
}

+ (NSString *)stops
{
    return NSLocalizedString(@"Stops", @"Stops");
}

+ (NSString *)favorite
{
    return NSLocalizedString(@"Favourite", @"Favorite");
}

+ (NSString *)transport
{
    return NSLocalizedString(@"Transport", @"transport");
}

+ (NSString *)genericErrorTitle
{
    return NSLocalizedString(@"Error", @"Generic error title");
}

+ (NSString *)errorLoadingDescription
{
    return NSLocalizedString(@"Error during loading transport information", @"Error description when we load all stops, transport, routes and etc");
}

+ (NSString *)tryAgain
{
    return NSLocalizedString(@"Try again", @"Try again");
}

+ (NSString *)settings
{
    return NSLocalizedString(@"Settings", @"Settings");
}

+ (NSString *)warning
{
    return NSLocalizedString(@"Warning", @"Warning");
}

+ (NSString *)updateDescription
{
    return NSLocalizedString(@"Do you want to update all transport and route info?", @"Description message when we want to update all data");
}

+ (NSString *)genericOk
{
    return NSLocalizedString(@"OK", @"Generic OK");
}

+ (NSString *)genericCancel
{
    return NSLocalizedString(@"Cancel", @"Generic Cancel");
}

+ (NSString *)emailSubject
{
    return NSLocalizedString(@"Feedback or questions about iOS application", @"Email subject");
}

+ (NSString *)addToFavourites
{
    return NSLocalizedString(@"Add to favourites", @"Add to favourites");
}

+ (NSString *)removeFromFavourites
{
    return NSLocalizedString(@"Remove from favourites", @"Remove from favourites");
}

+ (NSString *)absentArrivalDataForTransport
{
    return NSLocalizedString(@"-//-", @"Absent arrival data for transport");
}

+ (NSString *)listSeparator
{
    return NSLocalizedString(@", ", @"List separator");
}

@end
