//
//  TransportManagerAsyncTask.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 09/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

struct TransportManagerAsyncTask {
    let contextToSave: NSManagedObjectContext
    let block: ()->()
}
