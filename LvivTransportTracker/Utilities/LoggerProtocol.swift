//
// Created by Denys Meloshyn on 2018-12-16.
// Copyright (c) 2018 Denys Meloshyn. All rights reserved.
//

import Foundation
import XCGLogger

enum LogType {
    case debug
    case warning
    case error
}

protocol LoggerProtocol {
    func logMessage(_ type: LogType, _ message: String, fileName: StaticString, functionName: StaticString, lineNumber: Int)
}

extension LoggerProtocol {
    func log(_ type: LogType,
             _ message: String,
             fileName: StaticString = #file,
             functionName: StaticString = #function,
             lineNumber: Int = #line) {
        logMessage(type, message, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
    }
}

extension XCGLogger: LoggerProtocol {
    func logMessage(_ type: LogType,
                    _ message: String,
                    fileName: StaticString = #file,
                    functionName: StaticString = #function,
                    lineNumber: Int = #line) {
        switch type {
        case .error:
            error(message, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: [:])
        case .debug:
            debug(message, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: [:])
        case .warning:
            warning(message, functionName: functionName, fileName: fileName, lineNumber: lineNumber, userInfo: [:])
        }
    }
}
