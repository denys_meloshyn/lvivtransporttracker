//
// Created by Denys Meloshyn on 21/11/2019.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol ConfigureEntityProtocol {
    func configure(with dict: [String: String])
}
