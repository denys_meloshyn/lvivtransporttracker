//
// Created by Denys Meloshyn on 2019-01-31.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import XCGLogger
import BugfenderSDK

class RemoteLogsDestination: AppleSystemLogDestination {
    private var logDetails = LogDetails(level: XCGLogger.Level.info, date: Date(), message: "", functionName: "", fileName: "", lineNumber: 0)

    override func output(logDetails: LogDetails, message: String) {
        self.logDetails = logDetails

        super.output(logDetails: logDetails, message: message)
    }

    override func write(message: String) {
        BFLog(message)
    }
}
