//
//  UserSettings.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 10/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSettings : NSObject

+ (UserSettings *)sharedInstance;

@property (nonatomic) NSInteger updateInterval;
@property (nonatomic, strong) NSDate *lastUpdateDate;

@end
