//
//  AnalyticsManager.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 17.04.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "AnalyticsManager.h"

#import <Firebase/Firebase.h>

@interface AnalyticsManager ()
@end

@implementation AnalyticsManager

+ (AnalyticsManager *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static AnalyticsManager *shareInstance = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        shareInstance = [[AnalyticsManager alloc] init];
    });
    
    return shareInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
    }
    
    return self;
}

- (void)configure
{
    // Configure tracker from GoogleService-Info.plist.
    [FIRApp configure];
}

- (void)trackError:(NSString *)screen title:(NSString *)title error:(NSError *)error
{
    NSLog(@"%@ error:%@", title, error);
}

- (void)openStopDetailScreen:(NSString *)screenName model:(NSString *)name
{
    NSString *description = @"Open stop detail screen";

    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"stop": name}];
}
//
//- (void)openTransportDetailScreen:(NSString *)screenName model:(Transport *)model
//{
//    NSString *description = @"Open transport detail screen";
//
//    [Answers logContentViewWithName:screenName
//                        contentType:description
//                          contentId:model.name
//                   customAttributes:nil];
//
//    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"transport": model.name}];
//}
//
//- (void)addRemoveStopFromFavourite:(NSString *)screenName model:(Transport *)model
//{
//    NSString *description;
//
//    if (model.isFavorite) {
//        description = @"Remove stop from favourite";
//    }
//    else {
//        description = @"Add stop to favourite";
//    }
//
//    [Answers logContentViewWithName:screenName
//                        contentType:description
//                          contentId:model.name
//                   customAttributes:nil];
//
//    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"transport": model.name}];
//}

- (void)addRemoveTransportFromFavourite:(NSString *)screenName name:(NSString *)name isFavourite:(BOOL)isFavourite
{
    NSString *description;
    if (isFavourite) {
        description = @"Remove transport from favourite";
    }
    else {
        description = @"Add transport to favourite";
    }

    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"transport": name != nil ? name : @""}];
}

- (void)searchStop:(NSString *)search
{
}

- (void)swicthViewModeToMap:(BOOL)isMap
{
    NSString *title = @"Stops";
    NSString *description = @"Switch view mode";
    NSString *value;
    
    if (isMap) {
        value = @"Map";
    }
    else {
        value = @"List";
    }
    
    [FIRAnalytics logEventWithName:title parameters:@{@"action": description, @"switch": value}];
}

//- (void)openMap:(NSString *)screenName model:(Transport *)model
//{
//    NSString *description = @"Open map";
//
//    [Answers logContentViewWithName:screenName
//                        contentType:description
//                          contentId:model.name
//                   customAttributes:nil];
//
//    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"transport": model.name}];
//}

- (void)updateContent
{
    NSString *title = @"Settings";
    NSString *description = @"Update";
    
    [FIRAnalytics logEventWithName:title parameters:@{@"action": description}];
}

- (void)updateTimeInterval:(NSInteger)value
{
    NSString *title = @"Settings";
    NSString *description = @"Update time interval";
    
    [FIRAnalytics logEventWithName:title parameters:@{@"action": description, @"time": [NSString stringWithFormat:@"%li", (long)value]}];
}

- (void)sendEmail
{
    NSString *title = @"Settings";
    NSString *description = @"Send email";
    
    [FIRAnalytics logEventWithName:title parameters:@{@"action": description}];
}

//- (void)openRouteScreen:(NSString *)screenName model:(Transport *)model
//{
//    NSString *description = @"Open route screen";
//    
//    [Answers logContentViewWithName:screenName
//                        contentType:description
//                          contentId:model.name
//                   customAttributes:nil];
//    
//    [FIRAnalytics logEventWithName:screenName parameters:@{@"action": description, @"transport": model.name}];
//}

- (void)trackScreenVisible:(NSString *)screenName
{ 
    [FIRAnalytics logEventWithName:screenName parameters:nil];
}

@end
