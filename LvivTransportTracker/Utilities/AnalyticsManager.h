//
//  AnalyticsManager.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 17.04.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "StoptEntity.h"

@interface AnalyticsManager : NSObject

+ (AnalyticsManager *)sharedInstance;

- (void)configure;
- (void)sendEmail;
- (void)updateContent;
- (void)searchStop:(NSString *)seach;
- (void)swicthViewModeToMap:(BOOL)isMap;
- (void)updateTimeInterval:(NSInteger)value;
- (void)trackScreenVisible:(NSString *)screenName;
//- (void)openMap:(NSString *)screenName model:(Transport *)model;
//- (void)openRouteScreen:(NSString *)screenName model:(Transport *)model;
- (void)openStopDetailScreen:(NSString *)screenName model:(NSString *)name;
//- (void)addRemoveStopFromFavourite:(NSString *)screenName model:(Stop *)model;
//- (void)openTransportDetailScreen:(NSString *)screenName model:(Transport *)model;
- (void)trackError:(NSString *)screen title:(NSString *)title error:(NSError *)error;
- (void)addRemoveTransportFromFavourite:(NSString *)screenName name:(NSString *)name isFavourite:(BOOL)isFavourite;

@end
