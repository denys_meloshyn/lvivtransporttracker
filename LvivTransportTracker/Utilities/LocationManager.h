//
//  LocationManager.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <MapKit/MapKit.h>

@interface LocationManager : NSObject

@property (nonatomic, strong, readonly) CLLocationManager *locationManager;
@property (nonatomic, weak) NSObject <CLLocationManagerDelegate> *delegate;

+ (LocationManager *)sharedInstance;

- (BOOL)isAccessGranted;
- (void)stopUpdatingLocation;
- (void)startUpdatingLocation;

@end
