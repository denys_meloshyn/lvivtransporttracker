//
// Created by Denys Meloshyn on 2019-01-26.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

class StopAnnotation: NSObject, MKAnnotation {
    let title: String?
    let stopID: NSManagedObjectID
    let coordinate: CLLocationCoordinate2D

    init(coordinate: CLLocationCoordinate2D, title: String?, stopID: NSManagedObjectID) {
        self.title = title
        self.stopID = stopID
        self.coordinate = coordinate
    }
}
