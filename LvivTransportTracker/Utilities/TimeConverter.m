//
//  TimeConverter.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 07.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "TimeConverter.h"

@implementation TimeConverter

+ (NSString *)convert:(CGFloat)seconds
{
    NSDateComponentsFormatter* formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleShort;
    formatter.allowedUnits = NSCalendarUnitMinute | kCFCalendarUnitSecond;
    formatter.maximumUnitCount = 1;
    
    // Use the configured formatter to generate the string.
    NSString *outputString = [formatter stringFromTimeInterval:fabs(seconds)];
    
    return outputString;
}

+ (NSString *)minuteSecondsConvert:(CGFloat)seconds
{
    NSDateComponentsFormatter* formatter = [[NSDateComponentsFormatter alloc] init];
    formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleAbbreviated;
    formatter.allowedUnits = NSCalendarUnitMinute | kCFCalendarUnitSecond;
    
    // Use the configured formatter to generate the string.
    NSString *outputString = [formatter stringFromTimeInterval:fabs(seconds)];
    
    return outputString;
}

@end
