//
//  NetworkActivityIndicatorManager.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 23/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkActivityIndicatorManager : NSObject

+ (NetworkActivityIndicatorManager *)sharedInstance;

- (void)stopLoading;
- (void)startLoading;

@end
