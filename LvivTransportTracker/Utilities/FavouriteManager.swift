//
// Created by Denys Meloshyn on 2019-01-27.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol FavouriteManagerProtocol {
    func add(favouriteID: String, storage: StorageKey)
    func favourite(for key: StorageKey) -> Set<String>
}

enum StorageKey: String {
    case stop = "favourite_stop"
    case transport = "favourite_transport"
}

class FavouriteManager: FavouriteManagerProtocol {
    static let instance = FavouriteManager()

    private init() {
    }

    func favourite(for key: StorageKey) -> Set<String> {
        return UserDefaults.standard.value(forKey: key.rawValue) as? Set<String> ?? []
    }

    func save(favourite: Set<String>, for key: StorageKey) {
        UserDefaults.standard.set(favourite, forKey: key.rawValue)
    }

    func add(favouriteID: String, storage: StorageKey) {
        var favouritesID = favourite(for: storage)
        favouritesID.insert(favouriteID)

        save(favourite: favouritesID, for: storage)
    }
}
