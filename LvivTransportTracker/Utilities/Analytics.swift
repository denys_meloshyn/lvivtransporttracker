//
// Created by Denys Meloshyn on 2019-01-28.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseAnalytics

enum AnalyticEvent: String {
    case settingsUpdateError
    case settingsUpdateFinish
    case settingsUpdateShowWarning
    case settingsUpdate
    case settingsStepperChanged
    case settingsSendEmail
    case settingsFollowFB

    case stopListOpenMap
    case stopListOpenStop
    case stopListOpenTransport
    case stopListSearch

    case stopDetailOpenMap
    case stopDetailFavourite
    case stopDetailOpenTransport

    case stopMapStopSelected

    case transportDetailShowTimetable
    case transportDetailOpenStop

    case transportListOpenTransport

    case favouriteOpenTransport
    case favouriteOpenStop

    case transportStopTimeOpenStop
    case transportStopTimeFavourite
}

protocol AnalyticsProtocol {
    func log(_ event: AnalyticEvent, parameters: [String: Any]?)
}

extension AnalyticsProtocol {
    func log(_ event: AnalyticEvent) {
        log(event, parameters: nil)
    }
}

protocol AnalyticsTrackerProtocol {
    func track(_ event: AnalyticEvent, parameters: [String: Any]?)
}

extension AnalyticsTrackerProtocol {
    func track(_ event: AnalyticEvent) {
        track(event, parameters: nil)
    }
}

class MockAnalytics: AnalyticsProtocol {
    func log(_ event: AnalyticEvent, parameters: [String: Any]?) {
    }
}

class GoogleAnalytics: AnalyticsProtocol {
    static let instance = GoogleAnalytics()

    private init() {
        FirebaseApp.configure()
    }

    func log(_ event: AnalyticEvent, parameters: [String: Any]?) {
        Analytics.logEvent("ltt_\(event.rawValue)", parameters: parameters)
    }
}
