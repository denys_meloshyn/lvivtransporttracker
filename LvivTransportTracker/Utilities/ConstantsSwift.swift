//
// Created by Denys Meloshyn on 2018-12-26.
// Copyright (c) 2018 Denys Meloshyn. All rights reserved.
//

import Foundation
import RxSwift

class ConstantsSwift {
    static let isUpdated = BehaviorSubject<Bool>(value: false)
    
    static var timeTableDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .none

        return formatter
    }
}
