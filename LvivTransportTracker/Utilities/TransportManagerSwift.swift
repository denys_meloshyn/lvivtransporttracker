//
//  TransportManagerSwift.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 09/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

class TransportManagerSwift {
    static let instance = TransportManagerSwift(logger: DependencyContainer.instance.logger)

    private let logger: LoggerProtocol
    private var isFetchInProgress = false

    private var queue = [NSManagedObjectID: TransportManagerAsyncTask]() {
        didSet {
            guard isFetchInProgress == false else {
                return
            }

            guard let modelID = queue.keys.first else {
                return
            }

            guard let task = queue[modelID] else {
                self.queue.removeValue(forKey: modelID)
                return
            }

            isFetchInProgress = true
            DispatchQueue.global(qos: .background).async {
                task.block()

                DispatchQueue.main.async {
                    do {
                        try task.contextToSave.save()
                    } catch {
                        self.logger.log(.error, "\(error)")
                    }

                    self.isFetchInProgress = false
                    self.queue.removeValue(forKey: modelID)
                }
            }
        }
    }

    init(logger: LoggerProtocol) {
        self.logger = logger
    }

    func add(_ task: TransportManagerAsyncTask, with objectID: NSManagedObjectID) {
        guard queue[objectID] == nil else {
            return
        }
        
        queue[objectID] = task
    }

    func remove(objectID: NSManagedObjectID) {
        queue.removeValue(forKey: objectID)
    }

    func stopTime(for transportID: String, stopID: String, managedObjectContext: NSManagedObjectContext) throws -> [GtfsStopTime] {
        let tripFetchRequest: NSFetchRequest<GtfsTrip> = GtfsTrip.fetchRequest()
        tripFetchRequest.predicate = NSPredicate(format: "routeID == %@", transportID)
        let trips = try managedObjectContext.fetch(tripFetchRequest)
        let tripIDs = trips.map { trip -> String in
            return trip.tripID!
        }

        let stopTimesFetchRequest: NSFetchRequest<GtfsStopTime> = GtfsStopTime.fetchRequest()
        stopTimesFetchRequest.predicate = NSPredicate(format: "stopID == %@ AND tripID IN %@", stopID, tripIDs)
        let times = try managedObjectContext.fetch(stopTimesFetchRequest)

        return times
    }

    func createOrFetchStopTransportTimes(for stopID: NSManagedObjectID,
                                         transportID: NSManagedObjectID,
                                         managedObjectContext: NSManagedObjectContext) -> StopTransportTime {
        let stop = managedObjectContext.object(with: stopID) as! GtfsStop
        let transport = managedObjectContext.object(with: transportID) as! GtfsRoute
        let stopTransportTimesFetchRequest: NSFetchRequest<StopTransportTime> = StopTransportTime.fetchRequest()
        stopTransportTimesFetchRequest.predicate = NSPredicate(format: "stop.stopID == %@ AND transport.routeID == %@", stop.stopID!, transport.routeID!)
        stopTransportTimesFetchRequest.fetchBatchSize = 1

        do {
            let items = try managedObjectContext.fetch(stopTransportTimesFetchRequest)

            guard let item = items.first else {
                let times = try stopTime(for: transport.routeID!,
                                          stopID: stop.stopID!,
                                          managedObjectContext: managedObjectContext)

                let stopTransportTimes = StopTransportTime(context: managedObjectContext)
                stopTransportTimes.stop = stop
                stopTransportTimes.transport = transport
                stopTransportTimes.times = Set(times) as NSSet

                return stopTransportTimes
            }

            return item
        } catch {
            logger.log(.error, "\(error)")
            fatalError()
        }
    }

    func timeTable(from times:[GtfsStopTime]) -> [[TimeTable]] {
        var hourMinuteTrip = [String: [String: String]]()

        for stopTime in times {
            guard let hoursMinutesSeconds = stopTime.arrivalTime?.components(separatedBy: ":"),
                  hoursMinutesSeconds.count == 3 else {
                continue
            }

            let hour = hoursMinutesSeconds[0]
            let minute = hoursMinutesSeconds[1]

            var minuteTrip = hourMinuteTrip[hour] ?? [String: String]()

            if let tripID = minuteTrip[minute] {
                logger.log(.debug, "Stop: \(stopTime.stopID ?? "") time: \(hour):\(minute) already exist for trip ID: \(tripID), new value: \(stopTime.tripID ?? "")")
            }

            minuteTrip[minute] = stopTime.tripID ?? ""

            hourMinuteTrip[hour] = minuteTrip
        }

        var result = [[TimeTable]]()
        for hour in hourMinuteTrip.keys.sorted(by: <) {
            var timeTableMinutes = [TimeTable]()
            let minutes = hourMinuteTrip[hour]!
            for minute in minutes.keys.sorted(by: <) {
                let trip = minutes[minute]!
                let timeTable = TimeTable(hour: Int(hour)!, minute: Int(minute)!, second: 0, tripID: trip)
                timeTableMinutes.append(timeTable)
            }
            result.append(timeTableMinutes)
        }

        return result
    }
}
