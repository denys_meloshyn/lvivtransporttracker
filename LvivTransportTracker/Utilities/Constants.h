//
//  Constants.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 23/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UIColor_Hex/UIColor+Hex.h>

typedef void (^ProgressBlock)(NSNumber *total, NSNumber *finished);

typedef enum {
    ViewStatusWillAppear = 0,
    ViewStatusWillDisappear,
} ViewStatus;

static NSString *const kElectroType = @"LET";
static NSString *const kMicroBusesType = @"LAD";

static NSString *const kRouteTypeTrolleybus = @"kRouteTypeTrolleybus";
static NSString *const kRouteTypeTram = @"kRouteTypeTram";
static NSString *const kRouteTypeBus = @"kRouteTypeBus";

static NSString *const kRouteSeparator = @"»";
static NSInteger defaultTimeIntervalUpdate = 5;
static NSString *const kSupportEmail = @"LvivTransportTracker@gmail.com";

static NSString *const kScreenMap = @"Map";
static NSString *const kScreenNearestStop = @"Stops";
static NSString *const kScreenSettings = @"Settings";
static NSString *const kScreenFavourite = @"Favourite";
static NSString *const kScreenStopDetail = @"StopDetail";
static NSString *const kScreenRouteDetail = @"RouteDetail";
static NSString *const kScreenTransportList = @"TransportList";

static NSString *const kStopTableHeaderView = @"StopTableHeaderView";
static NSString *const kStopTableViewCellIdentifier = @"StopTableViewCell";
static NSString *const kTransportRouteTableViewCell = @"TransportRouteTableViewCell";
static NSString *const kTransportShortTableViewCellIdentifier = @"TransportShortTableViewCell";

@interface Constants : NSObject

+ (UIColor *)letColor;
+ (UIColor *)ladColor;
+ (UIColor *)tramColor;
+ (UIColor *)defaultApperanceColor;

@end
