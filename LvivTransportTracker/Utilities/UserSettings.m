//
//  UserSettings.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 10/03/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "UserSettings.h"

#import "Constants.h"

@interface UserSettings () <NSCoding>

@end

@implementation UserSettings

+ (UserSettings *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static UserSettings *shareInstance = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserSettings"];
        if (data == nil) {
            shareInstance = [[UserSettings alloc] init];
            
            shareInstance.updateInterval = defaultTimeIntervalUpdate;
            
            NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
            dateComponents.calendar = [NSCalendar currentCalendar];
            
            dateComponents.year = 2018;
            dateComponents.month = 3;
            dateComponents.day = 22;
            
            shareInstance.lastUpdateDate = [dateComponents date];
        }
        else {
            shareInstance = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            [shareInstance addObservers];
        }
    });
    
    return shareInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        [self addObservers];
    }
    
    return self;
}

#pragma mark - KVO methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    [self save];
}

#pragma mark - NSCoding methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self != nil) {
        self.lastUpdateDate = [aDecoder decodeObjectForKey:@"lastUpdateDate"];
        self.updateInterval = [[aDecoder decodeObjectForKey:@"updateInterval"] integerValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.lastUpdateDate forKey:@"lastUpdateDate"];
    [aCoder encodeObject:@(self.updateInterval) forKey:@"updateInterval"];
}

#pragma mark - Private methods

- (void)addObservers
{
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(lastUpdateDate)) options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(updateInterval)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)save
{
    NSData *object = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:@"UserSettings"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
