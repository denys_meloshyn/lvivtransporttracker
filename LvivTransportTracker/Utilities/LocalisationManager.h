//
//  LocalisationManager.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 02.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalisationManager : NSObject

+ (NSString *)bus;
+ (NSString *)tram;
+ (NSString *)stops;
+ (NSString *)warning;
+ (NSString *)tryAgain;
+ (NSString *)settings;
+ (NSString *)favorite;
+ (NSString *)genericOk;
+ (NSString *)transport;
+ (NSString *)trolleybus;
+ (NSString *)emailSubject;
+ (NSString *)listSeparator;
+ (NSString *)genericCancel;
+ (NSString *)addToFavourites;
+ (NSString *)updateDescription;
+ (NSString *)genericErrorTitle;
+ (NSString *)removeFromFavourites;
+ (NSString *)errorLoadingDescription;
+ (NSString *)absentArrivalDataForTransport;

@end
