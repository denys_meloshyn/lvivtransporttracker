//
//  TimeConverter.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 07.03.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeConverter : NSObject

+ (NSString *)convert:(CGFloat)seconds;
+ (NSString *)minuteSecondsConvert:(CGFloat)seconds;

@end
