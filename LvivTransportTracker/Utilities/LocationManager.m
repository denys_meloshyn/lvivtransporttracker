//
//  LocationManager.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 24/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "LocationManager.h"

@interface LocationManager () <CLLocationManagerDelegate>

@end

@implementation LocationManager

+ (LocationManager *)sharedInstance
{
    static dispatch_once_t onceToken = 0;
    static LocationManager *shareInstance = nil;
    
    // Init singleton
    dispatch_once(&onceToken, ^{
        shareInstance = [[LocationManager alloc] init];
    });
    
    return shareInstance;
}

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    
    return self;
}

#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
    }
    
    if (self.delegate != nil) {
        [self.delegate locationManager:manager didChangeAuthorizationStatus:status];
    }
}

#pragma mark - Public methods

- (BOOL)isAccessGranted
{
    return ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse);
}

- (void)startUpdatingLocation
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
}

@end
