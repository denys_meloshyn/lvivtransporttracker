//
//  Constants.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 23/02/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "Constants.h"

@implementation Constants

+ (UIColor *)letColor
{
    UIColor *color = [UIColor colorWithCSS:@"1E8AE1"];
    
    return color;
}

+ (UIColor *)ladColor
{
    UIColor *color = [UIColor colorWithCSS:@"F1C500"];
    
    return color;
}

+ (UIColor *)tramColor
{
    UIColor *color = [UIColor colorWithCSS:@"FF221F"];
    
    return color;
}

+ (UIColor *)defaultApperanceColor
{
    return [UIColor colorWithCSS:@"00AEEB"];
}

@end
