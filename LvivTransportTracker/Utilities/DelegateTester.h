//
//  DelegateTester.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 02/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DelegateTester : NSObject

@property (nonatomic, weak) id delegate;
@property (nonatomic, strong) Class delegateClass;
@property (nonatomic, readonly) SEL lastCalledMethod;
@property (nonatomic, strong) NSMutableArray <NSString *> *calledMethods;

- (void)reset;

@end
