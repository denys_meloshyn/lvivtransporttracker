//
//  DelegateTester.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 02/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "DelegateTester.h"

@implementation DelegateTester

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        self.calledMethods = [NSMutableArray array];
    }
    
    return self;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    return YES;
}

- (id)forwardingTargetForSelector:(SEL)aSelector
{
    [self.calledMethods addObject:NSStringFromSelector(aSelector)];
    
    NSObject *objectDelegate = [[self.delegateClass alloc] init];
    return objectDelegate;
}

- (SEL)lastCalledMethod
{
    NSString *selector = [self.calledMethods lastObject];
    
    return NSSelectorFromString(selector);
}

- (void)reset
{
    [self.calledMethods removeAllObjects];
}

@end
