//
// Created by Denys Meloshyn on 2018-12-26.
// Copyright (c) 2018 Denys Meloshyn. All rights reserved.
//

import Foundation

class Utilities {
    class func nilOrPrimaryKey(from values: [String?]) -> String? {
        let items = values.compactMap {
            $0
        }
        guard items.count == values.count else {
            return nil
        }

        return primaryKey(from: items)
    }

    class func primaryKey(from values: [String]) -> String {
        return values.joined(separator: ",")
    }

    class func splitTime(_ time: String) -> (Int, Int, Int)? {
        let hoursMinutesSeconds = time.components(separatedBy: ":")
        guard hoursMinutesSeconds.count == 3,
              let hour = Int(hoursMinutesSeconds[0]),
              let minute = Int(hoursMinutesSeconds[1]),
              let second = Int(hoursMinutesSeconds[0]) else {
            return nil
        }

        return (hour, minute, second)
    }

    class func color(for transportType: String) -> UIColor {
        let color: UIColor

        switch transportType {
        case kRouteTypeBus:
            color = Constants.ladColor()
        case kRouteTypeTrolleybus:
            color = Constants.letColor()
        default:
            color = Constants.tramColor()
        }

        return color
    }
}
