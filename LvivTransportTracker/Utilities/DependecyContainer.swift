//
// Created by Denys Meloshyn on 2018-12-16.
// Copyright (c) 2018 Denys Meloshyn. All rights reserved.
//

import Foundation
import Fabric
import XCGLogger
import Crashlytics
import BugfenderSDK

class DependencyContainer {
    static let instance = DependencyContainer()

    let logger: LoggerProtocol
    let analytics: AnalyticsProtocol

    private init() {
        let xcgLogger = XCGLogger.default

        if Environment.current != .unitTest {
            analytics = GoogleAnalytics.instance
            Fabric.with([Crashlytics.self])
            
            Bugfender.activateLogger("x041vOzFfgsTGl7PGfHlzlof9lPXxBjb")
            Bugfender.setPrintToConsole(false)
            Bugfender.enableUIEventLogging()  // optional, log user interactions automatically
            
            let remoteLogsDestination = RemoteLogsDestination(identifier: "remote.logger")
            remoteLogsDestination.outputLevel = .debug
            remoteLogsDestination.showLogIdentifier = false
            remoteLogsDestination.showFunctionName = true
            remoteLogsDestination.showThreadName = false
            remoteLogsDestination.showLevel = true
            remoteLogsDestination.showFileName = true
            remoteLogsDestination.showLineNumber = true
            remoteLogsDestination.showDate = true
            xcgLogger.add(destination: remoteLogsDestination)
        } else {
            analytics = MockAnalytics()
        }
        
        logger = xcgLogger
    }
}
