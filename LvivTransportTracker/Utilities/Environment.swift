//
// Created by Denys Meloshyn on 2019-02-03.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

enum Environment {
    case debugProduction
    case production
    case unitTest

    static var current: Environment {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            return .unitTest
        }

        #if DEBUG_PRODUCTION
        return .debugProduction
        #elseif PRODUCTION
        return .production
        #endif
    }
}
