//
// Created by Denys Meloshyn on 21/11/2019.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import CoreData

protocol ComputedPrimaryKeyProtocol: class {
    var computedPrimaryKeySwift: String? { get }
    static func primaryKeys() -> [String]
    func primaryKey(dict: [String: String]) -> String?
    static func primaryKey(dict: [String: String]) -> String?
}

extension ComputedPrimaryKeyProtocol {
    func primaryKey(dict: [String: String]) -> String? {
        return Self.primaryKey(dict: dict)
    }

    static func primaryKey(dict: [String: String]) -> String? {
        let values = primaryKeys().map { key -> String? in
            dict[key]
        }

        return Utilities.nilOrPrimaryKey(from: values)
    }
}
