//
//  GtfsFareRules+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsFareRules {
    func configure(with dict: [String: String]) {
        containsID = dict["contains_id"]
        destinationID = dict["destination_id"]
        originID = dict["origin_id"]
    }
}
