//
//  GtfsAgency+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsAgency {
    func configure(with dict: [String: String]) {
        agencyID = dict["agency_id"]
        fareUrl = dict["agency_fare_url"]
        lang = dict["agency_lang"]
        name = dict["agency_name"]
        phone = dict["agency_phone"]
        timezone = dict["agency_timezone"]
        url = dict["agency_url"]

        updateDate = Date()
    }
}
