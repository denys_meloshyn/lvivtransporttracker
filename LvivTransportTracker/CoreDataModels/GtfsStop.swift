//
//  GtfsStop+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsStop {
    func configure(with dict: [String: String]) {
        code = dict["stop_code"]
        latitude = NSNumber(value: Double(dict["stop_lat"] ?? "") ?? 0)
        longitude = NSNumber(value: Double(dict["stop_lon"] ?? "") ?? 0)
        name = dict["stop_name"]
        stopID = dict["stop_id"]
        stopDescription = dict["stop_desc"]
    }

    func sortedTransport() -> [GtfsRoute] {
        let transports = self.transports as? Set<GtfsRoute> ?? []
        let sorted = transports.sorted { (left, right) in
            return left.shortName ?? "" < right.shortName ?? ""
        }

        return sorted
    }
}
