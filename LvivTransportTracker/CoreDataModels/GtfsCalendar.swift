//
//  GtfsCalendar+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsCalendar {
    private func mapToBool(value: String?) -> NSNumber? {
        value.flatMap { item -> Bool? in
            Bool(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }
    }
    
    func configure(with dict: [String: String]) {
        serviceID = dict["service_id"]
        monday = mapToBool(value: dict["monday"])
        tuesday = mapToBool(value: dict["tuesday"])
        wednesday = mapToBool(value: dict["wednesday"])
        thursday = mapToBool(value: dict["thursday"])
        friday = mapToBool(value: dict["friday"])
        saturday = mapToBool(value: dict["saturday"])
        sunday = mapToBool(value: dict["sunday"])

        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyyMMdd"

        if let startDate = dateFormatter.date(from: dict["start_date"] ?? "") {
            self.startDate = startDate
        }
        
        if let endDate = dateFormatter.date(from: dict["end_date"] ?? "") {
            self.endDate = endDate
        }
    }
}
