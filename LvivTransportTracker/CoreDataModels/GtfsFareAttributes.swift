//
//  GtfsFareAttributes+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsFareAttributes {
    func configure(with dict: [String: String]) {
        fareID = dict["fare_id"]
        currency = dict["currency_type"]
        paymentMethod = dict["payment_method"]
        transfers = dict["transfers"]
        transferDuration = dict["transfer_duration"]
        
        price = dict["price"].flatMap { item -> Double? in
            Double(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }
    }
}
