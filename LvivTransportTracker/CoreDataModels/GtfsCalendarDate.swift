//
//  GtfsCalendarDate+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsCalendarDate {
    func configure(with dict: [String: String]) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyyMMdd"

        exceptionType = dict["exception_type"]
        if let date = dateFormatter.date(from: dict["date"] ?? "") {
            self.date = date
        }
    }
}
