//
//  GtfsRoute+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsRoute {
    func configure(with dict: [String: String]) {
        routeID = dict["route_id"]
        shortName = dict["route_short_name"]
        longName = dict["route_long_name"]
        type = dict["route_type"]
        desc = dict["route_desc"]
        url = dict["route_url"]
        color = dict["route_color"]
        textColor = dict["route_text_color"]

        routeType = configureRouteType()
    }

    func configureRouteType() -> String {
        if shortName?.hasPrefix("Тр") == true {
            return kRouteTypeTrolleybus
        } else if shortName?.hasPrefix("Т") == true {
            return kRouteTypeTram
        }

        return kRouteTypeBus
    }
}
