//
//  GtfsStopTime+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsStopTime {
    enum CodingKeys: String, CodingKey {
        case stopID = "stop_id"
        case tripID = "trip_id"
    }

    func configure(with dict: [String: String]) {
        arrivalTime = dict["arrival_time"]
        departureTime = dict["departure_time"]
        headsign = dict["stop_headsign"]
        pickupType = dict["pickup_type"]
        dropOffType = dict["drop_off_type"]
        stopID = dict["stop_id"]
        tripID = dict["trip_id"]

        shapeDistTraveled = dict["shape_dist_traveled"].flatMap { item -> Int? in
            Int(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }

        timepoint = dict["timepoint"].flatMap { item -> Int? in
            Int(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }

        sequence = dict["stop_sequence"].flatMap { item -> Int? in
            Int(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }

        updateDate = Date()
        computedPrimaryKey = primaryKey(dict: dict)
    }
}

extension GtfsStopTime: ConfigureEntityProtocol {
}

extension GtfsStopTime: ComputedPrimaryKeyProtocol {
    var computedPrimaryKeySwift: String? {
        computedPrimaryKey
    }
    
    class func primaryKeys() -> [String] {
        return [CodingKeys.tripID.rawValue, CodingKeys.stopID.rawValue]
    }
}
