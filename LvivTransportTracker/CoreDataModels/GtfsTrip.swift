//
//  GtfsTrip+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 22/11/2018.
//
//

import Foundation
import CoreData

extension GtfsTrip {
    func configure(with dict: [String: String]) {
        tripID = dict["trip_id"]
        routeID = dict["route_id"]
        serviceID = dict["service_id"]
        shapeID = dict["shape_id"]
        headsign = dict["trip_headsign"]
        shortName = dict["trip_short_name"]
        blockID = dict["block_id"]
        
        direction = dict["direction_id"].flatMap { item -> Int? in
            Int(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }
        
        bikesAllowed = dict["bikes_allowed"].flatMap { item -> Bool? in
            Bool(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }
        
        wheelchairAccessible = dict["wheelchair_accessible"].flatMap { item -> Bool? in
            Bool(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }
    }
}
