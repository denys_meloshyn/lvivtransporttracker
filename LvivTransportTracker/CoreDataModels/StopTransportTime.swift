//
//  StopTransportTime+CoreDataClass.swift
//  
//
//  Created by Denys Meloshyn on 02/12/2018.
//
//

import Foundation
import CoreData
import XCGLogger

extension StopTransportTime {
    func firstTimeTable(in timeTable: [[TimeTable]], after date: Date) -> TimeTable? {
        let set: Set<Calendar.Component> = Set([.hour, .minute])
        let dateComponents = Calendar.current.dateComponents(set, from: date)

        for timeTables in timeTable {
            guard let dateHour = dateComponents.hour else {
                continue
            }

            // Check only time tables for this or next hours
            guard let timeTable = timeTables.first, timeTable.hour >= dateHour else {
                continue
            }

            for time in timeTables {
                guard let dateMinute = dateComponents.minute else {
                    continue
                }

                if time.hour == dateHour {
                    if time.minute > dateMinute {
                        return time
                    }
                } else if time.hour > dateHour {
                    if time.minute < dateMinute {
                        return time
                    }
                }
            }
        }

        return nil
    }

    func timeTable() -> [[TimeTable]] {
        return TransportManagerSwift.instance.timeTable(from: times?.allObjects as? [GtfsStopTime] ?? [])
    }

    func nextTime(for date: Date) -> TimeTable? {
        let set = Set([Calendar.Component.hour, Calendar.Component.minute])
        let dateComponents = Calendar.current.dateComponents(set, from: date)
        var returnNext = false

        for hourSection in timeTable() {
            if returnNext {
                return hourSection.first
            }

            guard let item = hourSection.first, item.hour == dateComponents.hour else {
                continue
            }

            for timeTable in hourSection {
                if timeTable.minute > dateComponents.minute! {
                    return timeTable
                }
            }

            returnNext = true
        }

        return nil
    }

    func nextTime(after timeTable: TimeTable) -> TimeTable? {
        var returnNext = false

        for hourSection in self.timeTable() {
            if returnNext {
                return hourSection.first
            }

            for timeTableSection in hourSection {
                if returnNext {
                    return timeTableSection
                }

                if timeTableSection == timeTable {
                    returnNext = true
                }
            }
        }

        return nil
    }
}
