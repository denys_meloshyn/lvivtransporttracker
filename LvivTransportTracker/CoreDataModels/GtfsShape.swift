//
// Created by Denys Meloshyn on 27/11/2019.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import CoreData

extension GtfsShape {
    func configure(with dict: [String: String]) {
        distTraveled = dict["shape_dist_traveled"].flatMap { item -> Double? in
            Double(item)
        }.map { item -> NSNumber in
            NSNumber(value: item)
        }

        shapeID = dict["shape_id"]
        latitude = NSNumber(value: Double(dict["shape_pt_lat"] ?? "") ?? 0)
        longitude = NSNumber(value: Double(dict["shape_pt_lon"] ?? "") ?? 0)
        sequence = NSNumber(value: Int(dict["shape_pt_sequence"] ?? "") ?? 0)
    }
    
    func locationCoordinate() -> CLLocationCoordinate2D? {
        guard let latitude = latitude?.doubleValue, let longitude = longitude?.doubleValue else {
            return nil
        }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
