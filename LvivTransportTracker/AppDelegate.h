//
//  AppDelegate.h
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 22.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

