//
//  TransportDetailInteractor.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 08/12/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

struct TimeTableIndexPath {
    let hourIndex: Int
    let minuteIndex: Int
}

protocol TransportDetailInteractorDelegate: class {
    func contentChanged()
}

class TransportDetailInteractor: NSObject, AnalyticsTrackerProtocol {
    let logger: LoggerProtocol
    let timeTables: [[TimeTable]]
    var currentTimeTable: TimeTable!
    var tripTimeTable: [GtfsStopTime]
    let stopTransportTime: StopTransportTime
    weak var delegate: TransportDetailInteractorDelegate?

    private let analytics: AnalyticsProtocol
    private let managedObjectContext: NSManagedObjectContext
    private var fetchController: NSFetchedResultsController<GtfsStopTime>!

    init(stopID: NSManagedObjectID,
         transportID: NSManagedObjectID,
         managedObjectContext: NSManagedObjectContext,
         logger: LoggerProtocol,
         analytics: AnalyticsProtocol) {
        self.analytics = analytics
        self.managedObjectContext = managedObjectContext
        stopTransportTime = TransportManagerSwift.instance.createOrFetchStopTransportTimes(for: stopID,
                                                                                           transportID: transportID,
                                                                                           managedObjectContext: managedObjectContext)
        timeTables = stopTransportTime.timeTable()
        tripTimeTable = []
        self.logger = logger

        super.init()

        let currentTimeTable = firstTimetableForCurrentDateOrLast()
        updateCurrentTimetable(with: currentTimeTable)

        let fetchRequest: NSFetchRequest<GtfsStopTime> = GtfsStopTime.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "tripID == %@", currentTimeTable.tripID)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "sequence", ascending: true)]

        fetchController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                     managedObjectContext: managedObjectContext,
                                                     sectionNameKeyPath: nil,
                                                     cacheName: nil)
        fetchController.delegate = self
        do {
            try fetchController.performFetch()
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    private func firstTimetableForCurrentDateOrLast() -> TimeTable {
        guard let timeTable = stopTransportTime.firstTimeTable(in: timeTables, after: Date()) else {
            let sections = timeTables.last!
            return sections.last!
        }

        return timeTable
    }

    func index(for timeTable: TimeTable) -> TimeTableIndexPath {
        var indexPath: TimeTableIndexPath?

        for (hourIndex, hourSection) in timeTables.enumerated() {
            guard let item = hourSection.first, item.hour == timeTable.hour else {
                continue
            }

            for (minuteIndex, hourTimeTable) in hourSection.enumerated() {
                if timeTable.minute == hourTimeTable.minute {
                    indexPath = TimeTableIndexPath(hourIndex: hourIndex, minuteIndex: minuteIndex)
                }
            }
        }

        return indexPath!
    }

    func numberOfStops() -> Int {
        let section = fetchController.sections?.first
        return section?.numberOfObjects ?? 0
    }

    func stop(at indexPath: IndexPath) -> GtfsStop {
        let model = fetchController.object(at: indexPath)

        if let stop = model.stop {
            return stop
        }

        let fetchRequest: NSFetchRequest<GtfsStop> = GtfsStop.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "stopID == %@", model.stopID ?? "")
        fetchRequest.fetchBatchSize = 1

        var items = [GtfsStop]()
        do {
            items = try managedObjectContext.fetch(fetchRequest)
        } catch {
            logger.log(.error, "\(error)")
        }

        let stop = items.first!
        model.stop = stop

        return stop
    }
    
    func allStops() -> [GtfsStop] {
        return (0..<numberOfStops()).map { index -> GtfsStop in
            self.stop(at: IndexPath(row: index, section: 0))
        }
    }

    func fetchTransport(for stop: GtfsStop) {
        let task = TransportManagerAsyncTask(contextToSave: managedObjectContext) {
            let childContext = self.managedObjectContext.childrenManagedObjectContext()

            let stop = childContext.object(with: stop.objectID) as! GtfsStop
            guard stop.transports?.count ?? 0 == 0 else {
                return
            }

            TransportManager.sharedInstance().fetchAllTransports(for: stop.objectID,
                                                                 managedObjectContext: childContext)
            do {
                try childContext.save()
            } catch {
                self.logger.log(.error, "\(error)")
            }
            
            DispatchQueue.main.async {
                self.delegate?.contentChanged()
            }
        }
        TransportManagerSwift.instance.add(task, with: stop.objectID)
    }

    func allTransport(for stop: GtfsStop, except name: String) -> [GtfsRoute] {
        let transports = (stop.transports?.allObjects as? [GtfsRoute]) ?? []
        let otherTransport = transports.filter { (route: GtfsRoute) -> Bool in
            return route.shortName != name
        }.sorted { (left, right) -> Bool in
            return (left.shortName ?? "") < (right.shortName ?? "")
        }

        return otherTransport
    }

    func fetchStops(for tripID: String) {
        let fetchRequest = fetchController.fetchRequest
        fetchRequest.predicate = NSPredicate(format: "tripID == %@", tripID)

        do {
            try fetchController.performFetch()
            delegate?.contentChanged()
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    func updateCurrentTimetable(with newValue: TimeTable) {
        currentTimeTable = newValue
        tripTimeTable = fetchTimeForEachStop(with: currentTimeTable.tripID)
    }

    func fetchTimeForEachStop(with tripID: String) -> [GtfsStopTime] {
        let fetchRequest: NSFetchRequest<GtfsStopTime> = GtfsStopTime.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "arrivalTime", ascending: true)
        let predicate = NSPredicate(format: "tripID == %@", tripID)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = [sortDescriptor]

        var items = [GtfsStopTime]()
        do {
            items = try managedObjectContext.fetch(fetchRequest)
        } catch {
            logger.log(.error, "\(error)")
        }

        return items
    }

    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
    
    func shape(for tripID: String) -> [GtfsShape]? {
        let fetchRequest: NSFetchRequest<GtfsTrip> = GtfsTrip.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "tripID == %@", tripID)

        do {
            guard let trip = try managedObjectContext.fetch(fetchRequest).first, let shapeID = trip.shapeID else {
                return nil
            }
            
            let shapeFetchRequest: NSFetchRequest<GtfsShape> = GtfsShape.fetchRequest()
            shapeFetchRequest.predicate = NSPredicate(format: "shapeID == %@", shapeID)
            shapeFetchRequest.sortDescriptors = [NSSortDescriptor(key: "sequence", ascending: true)]
            
            return try managedObjectContext.fetch(shapeFetchRequest)
        } catch {
            logger.log(.error, "\(error)")
        }
        
        return nil
    }
}

extension TransportDetailInteractor: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
}
