//
// Created by Denys Meloshyn on 2019-01-27.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import HTTPNetworking

protocol StopDetailMapInteractorProtocol {
    var stop: GtfsStop { get }
    var delegate: StopDetailMapInteractorDelegate? { set get }

    func transports() -> [GtfsRoute]
}

protocol StopDetailMapInteractorDelegate: class {
    func contentChanged(data: TransitRealtime_FeedMessage)
}

class StopDetailMapInteractor: StopDetailMapInteractorProtocol {
    private var timer: Timer?
    private let logger: LoggerProtocol
    private var currentSession: URLSessionDataTask?
    private let managedObjectContext: NSManagedObjectContext

    let stop: GtfsStop
    weak var delegate: StopDetailMapInteractorDelegate?

    init(managedObjectContext: NSManagedObjectContext, stopID: NSManagedObjectID, logger: LoggerProtocol) {
        self.logger = logger
        self.managedObjectContext = managedObjectContext
        stop = managedObjectContext.object(with: stopID) as! GtfsStop

        loadContent()
    }

    func loadContent() {
        currentSession?.cancel()

        let url = URL(string: "http://track.ua-gis.com/gtfs/lviv/vehicle_position")!
        let request = URLRequest(url: url)

        currentSession = HTTPNetwork.instance.load(request) { data, response, error in
            self.runTimer()

            guard let data = data else {
                return
            }

            do {
                let feed = try TransitRealtime_FeedMessage(serializedData: data)
                self.delegate?.contentChanged(data: feed)
            } catch {
                print(error)
            }
        }
    }

    func runTimer() {
        let interval = UserSettings.sharedInstance().updateInterval
        let timer = Timer(timeInterval: TimeInterval(interval), repeats: false) { [weak self]  timer in
            self?.loadContent()
        }
        RunLoop.current.add(timer, forMode: .common)
        self.timer = timer
    }

    func transports() -> [GtfsRoute] {
        return stop.transports?.allObjects as? [GtfsRoute] ?? []
    }
}
