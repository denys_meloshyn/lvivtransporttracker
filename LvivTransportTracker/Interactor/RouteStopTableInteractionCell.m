//
//  RouteStopTableInteractionCell.m
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 25/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "RouteStopTableInteractionCell.h"

#import "StoptEntity.h"
#import "TransportManager.h"

@interface RouteStopTableInteractionCell ()

//@property (nonatomic, strong) Stop *stop;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation RouteStopTableInteractionCell

- (instancetype)init
{
    self = [super init];
    
    if (self != nil) {
        self.managedObjectContext = [TransportManager sharedInstance].managedObjectContext;
    }
    
    return self;
}

//- (StoptEntity *)entity
//{
//    StoptEntity *model = [[StoptEntity alloc] init];
//    [model configureWithCoreDataModel:self.stop];
//    
//    return model;
//}
//
//- (void)configureStopEntity:(NSManagedObjectID *)objectID
//{
//    self.stop = [self.managedObjectContext objectWithID:objectID];
//}

@end
