//
// Created by Denys Meloshyn on 2019-01-26.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa
import HTTPNetworking

protocol StopMapInteractorProtocol: AnalyticsTrackerProtocol {
    var transports: [GtfsRoute] { get }
    var transportLocation: BehaviorSubject<TransitRealtime_FeedMessage?> { get }
    
    func numberOfStops() -> Int
    func stop(for row: Int) -> GtfsStop
    func fetchTransports(for stop: GtfsStop)
    func stop(for annotation: StopAnnotation) -> GtfsStop
}

class StopMapInteractor: StopMapInteractorProtocol {
    private let logger: LoggerProtocol
    private let disposeBag = DisposeBag()
    private let analytics: AnalyticsProtocol
    private let managedObjectContext: NSManagedObjectContext
    private let stopFetchedResultController: NSFetchedResultsController<GtfsStop>
    
    var transports = [GtfsRoute]()
    var transportLocation = BehaviorSubject<TransitRealtime_FeedMessage?>(value: nil)

    init(managedObjectContext: NSManagedObjectContext, logger: LoggerProtocol, analytics: AnalyticsProtocol) {
        self.logger = logger
        self.analytics = analytics
        self.managedObjectContext = managedObjectContext

        let stopSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let stopFetchRequest: NSFetchRequest<GtfsStop> = GtfsStop.fetchRequest()
        stopFetchRequest.sortDescriptors = [stopSortDescriptor]

        stopFetchedResultController = NSFetchedResultsController(fetchRequest: stopFetchRequest,
                                                                 managedObjectContext: managedObjectContext,
                                                                 sectionNameKeyPath: nil,
                                                                 cacheName: nil)
        do {
            try stopFetchedResultController.performFetch()
            transports = try managedObjectContext.fetch(GtfsRoute.fetchRequest())
        } catch {
            logger.log(.error, "\(error)")
        }
        
        loadContent()
    }

    func numberOfStops() -> Int {
        guard let section = stopFetchedResultController.sections?.first else {
            return 0
        }

        return section.numberOfObjects
    }

    func loadContent() {
        let completable = Single<TransitRealtime_FeedMessage>.create { event in
            let url = URL(string: "http://track.ua-gis.com/gtfs/lviv/vehicle_position")!
            let request = URLRequest(url: url)

            let task = HTTPNetwork.instance.load(request) { data, response, error in
                guard let data = data else {
                    event(.error(NSError(domain: "empty data", code: -1)))
                    return
                }

                do {
                    let feed = try TransitRealtime_FeedMessage(serializedData: data)
                    event(.success(feed))
                } catch {
                    event(.error(error))
                }
            }

            return Disposables.create {
                task.cancel()
            }
        }.delay(RxTimeInterval.seconds(UserSettings.sharedInstance().updateInterval), scheduler: MainScheduler.instance)

        completable.subscribe { [weak self] event in
            switch event {
            case .success(let model):
                self?.transportLocation.onNext(model)
            case .error:
                break
            }
            
            self?.loadContent()
        }.disposed(by: disposeBag)
    }

    func stop(for row: Int) -> GtfsStop {
        let indexPath = IndexPath(row: row, section: 0)
        return stopFetchedResultController.object(at: indexPath)
    }

    func stop(for annotation: StopAnnotation) -> GtfsStop {
        return managedObjectContext.object(with: annotation.stopID) as! GtfsStop
    }

    func fetchTransports(for stop: GtfsStop) {
        TransportManagerSwift.instance.remove(objectID: stop.objectID)
        TransportManager.sharedInstance().fetchAllTransports(for: stop.objectID, managedObjectContext: managedObjectContext)
    }

    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}
