//
//  StopListInteractor.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 25/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import CoreData
import DeviceKit

protocol StopListInteractorProtocol: AnalyticsTrackerProtocol {
    func numberOfSections() -> Int
    func filterStops(with name: String)
    func stop(at section: Int) -> GtfsStop
    func numberOfItems(for section: Int) -> Int
    func fetchTransports(for indexPath: IndexPath)
    func transport(at indexPath: IndexPath) -> GtfsRoute?
}

protocol StopListInteractorDelegate: class {
    func contentChanged()
    func itemChanged(at indexPath: IndexPath)
}

class StopListInteractor: NSObject, StopListInteractorProtocol {
    weak var delegate: StopListInteractorDelegate?

    private let logger: LoggerProtocol
    private let analytics: AnalyticsProtocol
    private let reusablePredicate: NSPredicate
    private let managedObjectContext: NSManagedObjectContext
    private let fetchedResultController: NSFetchedResultsController<GtfsStop>

    init(managedObjectContext: NSManagedObjectContext, logger: LoggerProtocol, analytics: AnalyticsProtocol) {
        self.logger = logger
        self.analytics = analytics
        self.managedObjectContext = managedObjectContext
        reusablePredicate = NSPredicate(format: "name CONTAINS[cd] $name")

        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let fetchRequest: NSFetchRequest<GtfsStop> = GtfsStop.fetchRequest()
        fetchRequest.sortDescriptors = [sortDescriptor]

        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                             managedObjectContext: managedObjectContext,
                                                             sectionNameKeyPath: nil,
                                                             cacheName: nil)
        do {
            try fetchedResultController.performFetch()
        } catch {
            logger.log(.error, "\(error)")
        }

        logger.log(.debug, "iOS: \(UIDevice.current.systemVersion), device: \(Device())")

        super.init()
    }

    func config() {
        fetchedResultController.delegate = self
    }

    func stop(at section: Int) -> GtfsStop {
        let indexPath = IndexPath(row: section, section: 0)
        return fetchedResultController.object(at: indexPath)
    }

    func transport(at indexPath: IndexPath) -> GtfsRoute? {
        let model = stop(at: indexPath.section)
        let transports = model.sortedTransport()

        guard indexPath.row <= (transports.count - 1) else {
            return nil
        }

        return transports[indexPath.row]
    }

    func numberOfSections() -> Int {
        let sectionInfo = fetchedResultController.sections?.first
        return sectionInfo?.numberOfObjects ?? 0
    }

    func numberOfItems(for section: Int) -> Int {
        let model = stop(at: section)
        return model.transports?.count ?? 0
    }

    func fetchTransports(for indexPath: IndexPath) {
        let model = stop(at: indexPath.section)

        let task = TransportManagerAsyncTask(contextToSave: managedObjectContext) {
            let childContext = self.managedObjectContext.childrenManagedObjectContext()
            TransportManager.sharedInstance().fetchAllTransports(for: model.objectID, managedObjectContext: childContext)

            do {
                try childContext.save()
            } catch {
                self.logger.log(.error, "\(error)")
            }
        }
        TransportManagerSwift.instance.add(task, with: model.objectID)
    }

    func filterStops(with name: String) {
        if name.isEmpty {
            fetchedResultController.fetchRequest.predicate = nil
        } else {
            let fetchRequest = fetchedResultController.fetchRequest
            fetchRequest.predicate = reusablePredicate.withSubstitutionVariables(["name": name])
        }

        do {
            try fetchedResultController.performFetch()
            delegate?.contentChanged()
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}

extension StopListInteractor: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        guard let indexPath = newIndexPath else {
            return
        }

        delegate?.itemChanged(at: indexPath)
    }
}
