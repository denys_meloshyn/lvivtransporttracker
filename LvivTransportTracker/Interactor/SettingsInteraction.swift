//
//  SettingsInteraction.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 29.07.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import CoreData

protocol SettingsInteractionDelegate: class {
    func loadingFinished()
    func errorDuringLoading(error: Error)
    func loadingProgressChanged(total: Int, loaded: Int)
}

protocol SettingsInteractionProtocol: AnalyticsTrackerProtocol {
    func loadUpdate()
    func lastUpdate() -> Date
    var updateIntervalValue: Int { set get }
}

class SettingsInteraction: SettingsInteractionProtocol {
    private let gtfs: GTFS
    private let logger: LoggerProtocol
    private let analytics: AnalyticsProtocol
    private let managedObjectContext: NSManagedObjectContext
    private let favoriteInteractor: FavoriteInteractorProtocol

    var updateIntervalValue: Int {
        set {
            UserSettings.sharedInstance().updateInterval = newValue
        }
        get {
            return UserSettings.sharedInstance().updateInterval
        }
    }

    weak var delegate: SettingsInteractionDelegate?

    init(managedObjectContext: NSManagedObjectContext,
         favoriteInteractor: FavoriteInteractorProtocol,
         logger: LoggerProtocol,
         analytics: AnalyticsProtocol) {
        self.managedObjectContext = managedObjectContext
        self.favoriteInteractor = favoriteInteractor
        self.logger = logger
        self.analytics = analytics
        gtfs = GTFS(staticURL: URL(string: "http://track.ua-gis.com/gtfs/lviv/static.zip")!,
                    realTimeURL: URL(string: "http://track.ua-gis.com/gtfs/lviv/vehicle_position")!,
                    managedObjectContext: managedObjectContext)
        gtfs.delegate = self
    }

    func loadUpdate() {
        logger.log(.debug, "Start update")
        gtfs.loadStatic { [weak self] data, response, error in
            guard let strongSelf = self else {
                return
            }

            if let error = error {
                strongSelf.delegate?.errorDuringLoading(error: error)
                return
            }

            ConstantsSwift.isUpdated.onNext(true)
            TransportManager.sharedInstance()?.save(strongSelf.managedObjectContext)
            UserSettings.sharedInstance().lastUpdateDate = Date()
            strongSelf.delegate?.loadingFinished()
        }
    }

    func lastUpdate() -> Date {
        return UserSettings.sharedInstance().lastUpdateDate
    }

    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}

extension SettingsInteraction: GTFSDelegate {
    func file(_ file: GTFSFile, item: [String: String], managedObjectContext: NSManagedObjectContext) {
        switch file {
        case .fareAttributes:
            let fareAttributes = GtfsFareAttributes(context: managedObjectContext)
            fareAttributes.configure(with: item)

        case .fareRules:
            let fareRule = GtfsFareRules(context: managedObjectContext)
            fareRule.configure(with: item)

        case .agency:
            let model = GtfsAgency(context: managedObjectContext)
            model.configure(with: item)

        case .calendar:
            let model = GtfsCalendar(context: managedObjectContext)
            model.configure(with: item)

        case .calendarDates:
            let calendarDate = GtfsCalendarDate(context: managedObjectContext)
            calendarDate.configure(with: item)

        case .stops:
            let model = GtfsStop(context: managedObjectContext)
            model.configure(with: item)

        case .stopTimes:
            let stopTime = GtfsStopTime(context: managedObjectContext)
            stopTime.configure(with: item)

        case .routes:
            let model = GtfsRoute(context: managedObjectContext)
            model.configure(with: item)

        case .trips:
            let model = GtfsTrip(context: managedObjectContext)
            model.configure(with: item)

        case .shapes:
            let model = GtfsShape(context: managedObjectContext)
            model.configure(with: item)

        default:
            break
        }
    }

    func finishReadingFile(name: String, index: Int, from: Int) {
        DispatchQueue.main.async {
            self.delegate?.loadingProgressChanged(total: from, loaded: index)
        }
    }
}
