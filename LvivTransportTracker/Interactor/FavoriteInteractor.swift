//
// Created by Denys Meloshyn on 2019-01-20.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import CoreData

protocol FavoriteInteractorProtocol: AnalyticsTrackerProtocol {
    var delegate: FavoriteInteractorDelegate? { set get }
    var stops: [GtfsStop] { get }
    var transports: [GtfsRoute] { get }

    func reFetch()

    func numberOfSections() -> Int

    func stop(at row: Int) -> GtfsStop

    func transport(at row: Int) -> GtfsRoute

    func numberOfItems(at section: Int) -> Int
}

protocol FavoriteInteractorDelegate: class {
    func contentChanged()
}

class FavoriteInteractor: FavoriteInteractorProtocol {
    private let logger: LoggerProtocol
    private let analytics: AnalyticsProtocol
    private let managedObjectContext: NSManagedObjectContext
    var stops = [GtfsStop]()
    var transports = [GtfsRoute]()

    weak var delegate: FavoriteInteractorDelegate?

    init(managedObjectContext: NSManagedObjectContext, logger: LoggerProtocol, analytics: AnalyticsProtocol) {
        self.logger = logger
        self.analytics = analytics
        self.managedObjectContext = managedObjectContext
        self.reFetch()
    }

    func numberOfSections() -> Int {
        let sections = [stops.count, transports.count]
        return sections.compactMap {
            $0
        }.filter { (item: Int) -> Bool in
            item > 0
        }.count
    }

    func numberOfItems(at section: Int) -> Int {
        let result: Int
        if numberOfSections() > 1 {
            if section == 0 {
                result = stops.count
            } else {
                result = transports.count
            }
        } else {
            if stops.count > 0 {
                result = stops.count
            } else {
                result = transports.count
            }
        }

        return result
    }

    func stop(at row: Int) -> GtfsStop {
        stops[row]
    }

    func transport(at row: Int) -> GtfsRoute {
        transports[row]
    }

    func reFetch() {
        do {
            let stopSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
            let stopFetchRequest: NSFetchRequest<GtfsStop> = GtfsStop.fetchRequest()
            stopFetchRequest.sortDescriptors = [stopSortDescriptor]
            stopFetchRequest.predicate = NSPredicate(format: "stopID IN %@", FavouriteRepository.shared.stops())
            stops = try managedObjectContext.fetch(stopFetchRequest)

            let transportSortDescriptorName = NSSortDescriptor(key: "shortName", ascending: true)
            let transportSortDescriptorType = NSSortDescriptor(key: "routeType", ascending: true)
            let transportFetchRequest: NSFetchRequest<GtfsRoute> = GtfsRoute.fetchRequest()
            transportFetchRequest.sortDescriptors = [transportSortDescriptorType, transportSortDescriptorName]
            transportFetchRequest.predicate = NSPredicate(format: "routeID IN %@", FavouriteRepository.shared.transports())

            transports = try managedObjectContext.fetch(transportFetchRequest)
        } catch {
            logger.log(.error, "\(error)")
        }
    }
}

extension FavoriteInteractor: AnalyticsTrackerProtocol {
    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}
