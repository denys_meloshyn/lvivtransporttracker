//
// Created by Denys Meloshyn on 2019-01-06.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportListInteractorProtocol: AnalyticsTrackerProtocol {
    var delegate: TransportListInteractorDelegate? { set get }

    func numberOfTransportTypes() -> Int
    func sectionName(for index: Int) -> String
    func transport(at indexPath: IndexPath) -> GtfsRoute
    func numberOfTransports(for typeSection: Int) -> Int
}

protocol TransportListInteractorDelegate: class {
    func contentChanged()
}

class TransportListInteractor: BaseInteractor, TransportListInteractorProtocol {
    private let logger: LoggerProtocol
    private let managedObjectContext: NSManagedObjectContext
    private let fetchedResultController: NSFetchedResultsController<GtfsRoute>

    weak var delegate: TransportListInteractorDelegate?

    init(managedObjectContext: NSManagedObjectContext, logger: LoggerProtocol, analytics: AnalyticsProtocol) {
        self.logger = logger
        self.managedObjectContext = managedObjectContext

        let fetchRequest: NSFetchRequest<GtfsRoute> = GtfsRoute.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "shortName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]

        fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                             managedObjectContext: managedObjectContext,
                                                             sectionNameKeyPath: "routeType",
                                                             cacheName: nil)
        super.init(analytics: analytics)

        fetchedResultController.delegate = self
        do {
            try fetchedResultController.performFetch()
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    func numberOfTransportTypes() -> Int {
        return fetchedResultController.sections?.count ?? 0
    }

    func numberOfTransports(for typeSection: Int) -> Int {
        guard let section = fetchedResultController.sections?[typeSection] else {
            return 0
        }

        return section.numberOfObjects
    }

    func sectionName(for index: Int) -> String {
        return fetchedResultController.sections?[index].name ?? ""
    }

    func transport(at indexPath: IndexPath) -> GtfsRoute {
        return fetchedResultController.object(at: indexPath)
    }
}

extension TransportListInteractor: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.contentChanged()
    }
}
