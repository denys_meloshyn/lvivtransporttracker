//
//  RouteStopTableInteractionCell.h
//  LvivTransportTracker
//
//  Created by Denys.Meloshyn on 25/07/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>

@interface RouteStopTableInteractionCell : NSObject

@property (nonatomic, strong) NSManagedObject *route;
@property (nonatomic, strong) NSManagedObject *stopEntity;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
