//
// Created by Denys Meloshyn on 2019-01-10.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportStopTimesInteractorProtocol: AnalyticsTrackerProtocol {
    var transportDetail: TransportDetail { get }

    func numberOfStops() -> Int
    func changeFavouriteState()
    func fetchTransports(for stop: GtfsStop)
    func stop(at indexPath: IndexPath) -> GtfsStop
    func stopName(at indexPath: IndexPath) -> String
    func timetable(at indexPath: IndexPath) -> [[TimeTable]]
}

class TransportStopTimesInteractor: TransportStopTimesInteractorProtocol {
    let transportDetail: TransportDetail

    private let logger: LoggerProtocol
    private let analytics: AnalyticsProtocol
    private var timetables = [IndexPath: [[TimeTable]]]()
    private let managedObjectContext: NSManagedObjectContext
    private let fetchResultsController: NSFetchedResultsController<TransportDetailStopsTimes>

    init(managedObjectContext: NSManagedObjectContext,
         transportID: NSManagedObjectID,
         logger: LoggerProtocol,
         analytics: AnalyticsProtocol) {
        self.logger = logger
        self.analytics = analytics
        self.managedObjectContext = managedObjectContext

        let transportDetailStopsTimesFetchRequest: NSFetchRequest<TransportDetailStopsTimes> = TransportDetailStopsTimes.fetchRequest()
        transportDetailStopsTimesFetchRequest.sortDescriptors = [NSSortDescriptor(key: "sequence", ascending: true)]
        fetchResultsController = NSFetchedResultsController(fetchRequest: transportDetailStopsTimesFetchRequest,
                                                            managedObjectContext: self.managedObjectContext,
                                                            sectionNameKeyPath: nil,
                                                            cacheName: nil)

        let transport = self.managedObjectContext.object(with: transportID) as! GtfsRoute

        let fetchRequest: NSFetchRequest<TransportDetail> = TransportDetail.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "transport.routeID == %@", transport.routeID ?? "")
        fetchRequest.fetchBatchSize = 1

        var items = [TransportDetail]()
        do {
            items = try self.managedObjectContext.fetch(fetchRequest)
        } catch {
            logger.log(.error, "\(error)")
        }

        if let model = items.first {
            transportDetail = model
        } else {
            transportDetail = TransportDetail(context: self.managedObjectContext)
            transportDetail.transport = transport

            let tripFetchRequest: NSFetchRequest<GtfsTrip> = GtfsTrip.fetchRequest()
            tripFetchRequest.predicate = NSPredicate(format: "routeID == %@", transport.routeID ?? "")
            do {
                let tripsID: [String] = try self.managedObjectContext.fetch(tripFetchRequest).compactMap { trip in
                    return trip.tripID
                }

                let stopTimesFetchRequest: NSFetchRequest<GtfsStopTime> = GtfsStopTime.fetchRequest()
                stopTimesFetchRequest.sortDescriptors = [NSSortDescriptor(key: "sequence", ascending: false)]
                stopTimesFetchRequest.predicate = NSPredicate(format: "tripID IN %@", tripsID)

                let stopTimes = try self.managedObjectContext.fetch(stopTimesFetchRequest)
                var insertedStopsID = Set<String>()
                var uniqueStopTimes = [GtfsStopTime]()

                for stopTime in stopTimes {
                    guard let stopID = stopTime.stopID else {
                        continue
                    }

                    guard insertedStopsID.contains(stopID) == false else {
                        continue
                    }

                    insertedStopsID.insert(stopID)
                    uniqueStopTimes.append(stopTime)
                }
                
                let result = uniqueStopTimes.map { stopTime -> TransportDetailStopsTimes in
                    let model = TransportDetailStopsTimes(context: self.managedObjectContext)
                    model.stopID = stopTime.stopID
                    model.sequence = stopTime.sequence
                    model.transportDetail = transportDetail

                    return model
                }

                transportDetail.stopTimes = NSSet(array: result)
            } catch {
                logger.log(.error, "\(error)")
            }
        }

        do {
            try fetchResultsController.performFetch()
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    func timetable(at indexPath: IndexPath) -> [[TimeTable]] {
        guard let items = timetables[indexPath] else {
            let transportDetailStopsTimes = self.transportDetailStopsTimes(at: indexPath)
            if transportDetailStopsTimes.stop == nil {
                fetchStop(for: indexPath)
            }

            var times = transportDetailStopsTimes.times?.allObjects as? [GtfsStopTime] ?? []
            if times.isEmpty {
                times = fetchStopTimes(for: indexPath)
            }

            let timetable = TransportManagerSwift.instance.timeTable(from: times)
            timetables[indexPath] = timetable

            return timetable
        }

        return items
    }

    func numberOfStops() -> Int {
        guard let section = fetchResultsController.sections?.first else {
            return 0
        }

        return section.numberOfObjects
    }

    func stopName(at indexPath: IndexPath) -> String {
        return transportDetailStopsTimes(at: indexPath).stop?.name ?? ""
    }

    func stop(at indexPath: IndexPath) -> GtfsStop {
        return transportDetailStopsTimes(at: indexPath).stop!
    }

    func changeFavouriteState() {
        guard let modelID = transportDetail.transport?.routeID else {
            return
        }
        
        FavouriteRepository.shared.changeFavouriteStatus(type: .transport, itemID: modelID)
    }

    private func transportDetailStopsTimes(at indexPath: IndexPath) -> TransportDetailStopsTimes {
        return fetchResultsController.object(at: indexPath)
    }

    private func fetchStop(for indexPath: IndexPath) {
        let model = transportDetailStopsTimes(at: indexPath)
        let fetchRequest: NSFetchRequest<GtfsStop> = GtfsStop.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "stopID == %@", model.stopID ?? "")
        fetchRequest.fetchBatchSize = 1

        do {
            let items = try managedObjectContext.fetch(fetchRequest)
            model.stop = items.first
        } catch {
            logger.log(.error, "\(error)")
        }
    }

    private func fetchStopTimes(for indexPath: IndexPath) -> [GtfsStopTime] {
        let model = transportDetailStopsTimes(at: indexPath)

        do {
            let items = try TransportManagerSwift.instance.stopTime(for: model.transportDetail?.transport?.routeID ?? "",
                                                                    stopID: model.stop?.stopID ?? "",
                                                                    managedObjectContext: managedObjectContext)
            model.addToTimes(NSSet(array: items))
            return items
        } catch  {
            logger.log(.error, "\(error)")
            fatalError()
        }
    }

    func fetchTransports(for stop: GtfsStop) {
        TransportManagerSwift.instance.remove(objectID: stop.objectID)
        TransportManager.sharedInstance().fetchAllTransports(for: stop.objectID, managedObjectContext: managedObjectContext)
    }

    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}
