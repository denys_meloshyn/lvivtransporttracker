//
// Created by Denys Meloshyn on 2019-01-29.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

class BaseInteractor: NSObject {
    private let analytics: AnalyticsProtocol

    init(analytics: AnalyticsProtocol) {
        self.analytics = analytics
    }
}

extension BaseInteractor: AnalyticsTrackerProtocol {
    func track(_ event: AnalyticEvent, parameters: [String: Any]?) {
        analytics.log(event, parameters: parameters)
    }
}
