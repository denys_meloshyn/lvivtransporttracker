//
//  NSManagedObjectContext+Child.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 05.08.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    func childrenManagedObjectContext() -> NSManagedObjectContext {
        let childrenManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        childrenManagedObjectContext.parent = self

        return childrenManagedObjectContext
    }
}
