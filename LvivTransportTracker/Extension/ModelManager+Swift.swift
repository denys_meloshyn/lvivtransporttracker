//
//  ModelManager+Swift.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 01.09.2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import HTTPNetworking

class ModelManager {
    static let shared = ModelManager()
    
    func loadRoutePositions(completion: ((TransitRealtime_FeedMessage?, HTTPURLResponse?, Error?) -> Void)?) -> URLSessionDataTask {
        let url = URL(string: "http://track.ua-gis.com/gtfs/lviv/vehicle_position")!
        let request = URLRequest(url: url)

        let session = HTTPNetwork.instance.load(request) { data, response, error in
            if let error = error {
                completion?(nil, response, error)
                return
            }

            guard let data = data else {
                completion?(nil, response, nil)
                return
            }

            do {
                let feed = try TransitRealtime_FeedMessage(serializedData: data)
                completion?(feed, response, nil)
            } catch {
                completion?(nil, response, error)
            }
        }

        return session
    }
}
