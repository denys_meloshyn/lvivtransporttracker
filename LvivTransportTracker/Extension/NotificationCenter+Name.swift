//
// Created by Denys Meloshyn on 2019-01-25.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let appStartUpdate = Notification.Name("appStartUpdate")
    static let appFinishUpdate = Notification.Name("appFinishUpdate")
}
