//
//  AppDelegate.m
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 22.02.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

#import "AppDelegate.h"

#import "Constants.h"
#import "AnalyticsManager.h"
#import "TransportManager.h"
#import "LvivTransportTracker-Swift.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@property (nonatomic, strong) GTFSManagerObjC *gtfsManagerObjC;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self configureAppearance];
    
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//    [[TransportManager sharedInstance] saveContext:[TransportManager sharedInstance].managedObjectContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // Saves changes in the application's managed object context before the application terminates.
//    [[TransportManager sharedInstance] saveContext:[TransportManager sharedInstance].managedObjectContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)configureAppearance
{
    [UITabBar appearance].tintColor = [Constants defaultApperanceColor];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [Constants defaultApperanceColor]} forState:UIControlStateSelected];
    
    [UINavigationBar appearance].tintColor = [Constants defaultApperanceColor];
    [UINavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName : [Constants defaultApperanceColor]};
    [UINavigationBar appearance].largeTitleTextAttributes = @{NSForegroundColorAttributeName: [Constants defaultApperanceColor]};
    
    [UISearchBar appearance].tintColor = [Constants defaultApperanceColor];
    [UISearchBar appearance].barTintColor = [UIColor colorWithCSS:@"F9F9F9"];
    
    [UIStepper appearance].tintColor = [Constants defaultApperanceColor];
}

@end
