//
// Created by Denys Meloshyn on 25/11/2019.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation
import CoreData

class FavouriteRepository {
    static let shared = FavouriteRepository()

    enum FavouriteType: String {
        case transport
        case stop
    }

    func stops() -> [String] {
        let container = CoreDataStack.shared.favouritePersistentContainer
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        request.predicate = NSPredicate(format: "type = %@", FavouriteType.stop.rawValue)

        do {
            let favourite = try container.viewContext.fetch(request)
            return favourite.compactMap { favourite -> String? in
                favourite.itemID
            }
        } catch {
            DependencyContainer.instance.logger.log(.error, "\(error)")
            return []
        }
    }

    func transports() -> [String] {
        let container = CoreDataStack.shared.favouritePersistentContainer
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        request.predicate = NSPredicate(format: "type = %@", FavouriteType.transport.rawValue)

        do {
            let favourite = try container.viewContext.fetch(request)
            return favourite.compactMap { favourite -> String? in
                favourite.itemID
            }
        } catch {
            DependencyContainer.instance.logger.log(.error, "\(error)")
            return []
        }
    }

    func isFavourite(type: FavouriteType, itemID: String) -> Bool {
        let container = CoreDataStack.shared.favouritePersistentContainer
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        request.predicate = NSPredicate(format: "type = %@ AND itemID = %@", type.rawValue, itemID)
        request.fetchBatchSize = 1

        do {
            let items = try container.viewContext.fetch(request)
            return !items.isEmpty
        } catch {
            return false
        }
    }

    func changeFavouriteStatus(type: FavouriteType, itemID: String) {
        let container = CoreDataStack.shared.favouritePersistentContainer
        let request: NSFetchRequest<Favourite> = Favourite.fetchRequest()
        request.predicate = NSPredicate(format: "type = %@ AND itemID = %@", type.rawValue, itemID)
        request.fetchBatchSize = 1

        do {
            let items = try container.viewContext.fetch(request)
            if let model = items.first {
                container.viewContext.delete(model)
            } else {
                let model = Favourite(context: container.viewContext)
                model.type = type.rawValue
                model.itemID = itemID
            }

            try container.viewContext.save()
        } catch {
            DependencyContainer.instance.logger.log(.error, "\(error)")
        }
    }
}
