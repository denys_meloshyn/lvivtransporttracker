//
// Created by Denys Meloshyn on 2019-01-25.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol BaseRouterProtocol {
    func popToRoot()
}

class BaseRouter {
    weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }

    func popToRoot() {
        guard let viewControllers = viewController?.navigationController?.viewControllers, viewControllers.count > 1 else {
            return
        }

        viewController?.navigationController?.viewControllers = [viewControllers[0]]
    }
}
