//
// Created by Denys Meloshyn on 2019-01-12.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportListRouterProtocol: BaseRouterProtocol {
    func openTransportDetailScreen(transportID: NSManagedObjectID)
}

class TransportListRouter: BaseRouter, TransportListRouterProtocol {
    func openTransportDetailScreen(transportID: NSManagedObjectID) {
        let transportDetailViewController = R.storyboard.main.transportStopTimesViewController()!
        transportDetailViewController.transportID = transportID

        viewController?.navigationController?.pushViewController(transportDetailViewController, animated: true)
    }
}
