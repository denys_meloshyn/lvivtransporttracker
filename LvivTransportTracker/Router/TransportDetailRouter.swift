//
// Created by Denys Meloshyn on 2019-01-27.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportDetailRouterProtocol {
    func openStopDetail(with stopID: NSManagedObjectID)
    func openMap(with transportID: NSManagedObjectID, shapes: [CLLocationCoordinate2D], routeID: String, tripID: String, stops: [GtfsStop])
}

class TransportDetailRouter: BaseRouter, TransportDetailRouterProtocol {
    func openMap(with transportID: NSManagedObjectID, shapes: [CLLocationCoordinate2D], routeID: String, tripID: String, stops: [GtfsStop]) {
        let transportMapViewController = R.storyboard.main.transportMapViewController()!
        transportMapViewController.stops = stops
        transportMapViewController.shapes = shapes
        transportMapViewController.tripID = tripID
        transportMapViewController.transportID = routeID

        viewController?.navigationController?.pushViewController(transportMapViewController, animated: true)
    }
    
    func openStopDetail(with stopID: NSManagedObjectID) {
        let stopDetailViewController = R.storyboard.main.stopDetailViewController()!
        stopDetailViewController.currentStopID = stopID

        viewController?.navigationController?.pushViewController(stopDetailViewController, animated: true)
    }
}
