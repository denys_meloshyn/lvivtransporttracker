//
// Created by Denys Meloshyn on 2019-01-23.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol FavoriteRouterProtocol: BaseRouterProtocol {
    func openStopScreen(stopID: NSManagedObjectID)
    func openTransportScreen(transportID: NSManagedObjectID)
}

class FavoriteRouter: BaseRouter, FavoriteRouterProtocol {
    func openStopScreen(stopID: NSManagedObjectID) {
        let stopDetailViewController = R.storyboard.main.stopDetailViewController()!
        stopDetailViewController.currentStopID = stopID

        viewController?.navigationController?.pushViewController(stopDetailViewController, animated: true)
    }

    func openTransportScreen(transportID: NSManagedObjectID) {
        let transportStopTimesViewController = R.storyboard.main.transportStopTimesViewController()!
        transportStopTimesViewController.transportID = transportID

        viewController?.navigationController?.pushViewController(transportStopTimesViewController, animated: true)
    }
}
