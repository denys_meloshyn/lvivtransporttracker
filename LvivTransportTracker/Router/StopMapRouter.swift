//
// Created by Denys Meloshyn on 2019-01-26.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol StopMapRouterProtocol {
    func openStopDetailScreen(stopID: NSManagedObjectID)
}

class StopMapRouter: BaseRouter, StopMapRouterProtocol {
    func openStopDetailScreen(stopID: NSManagedObjectID) {
        let stopDetailViewController = R.storyboard.main.stopDetailViewController()!
        stopDetailViewController.currentStopID = stopID

        viewController?.navigationController?.pushViewController(stopDetailViewController, animated: true)
    }
}
