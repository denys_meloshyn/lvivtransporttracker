//
//  StopListRouter.swift
//  LvivTransportTracker
//
//  Created by Denys Meloshyn on 28/11/2018.
//  Copyright © 2018 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol StopListRouterProtocol: BaseRouterProtocol {
    func openStopMapScreen()
    func openStopScreen(_ stopID: NSManagedObjectID)
    func openTransportDetailScreen(stopID: NSManagedObjectID, transportID: NSManagedObjectID)
}

class StopListRouter: BaseRouter, StopListRouterProtocol {
    func openStopMapScreen() {
        let stopMapViewController = R.storyboard.main.stopMapViewController()!
        viewController?.navigationController?.pushViewController(stopMapViewController, animated: true)
    }

    func openStopScreen(_ stopID: NSManagedObjectID) {
        let stopDetailViewController = R.storyboard.main.stopDetailViewController()!
        stopDetailViewController.currentStopID = stopID

        viewController?.navigationController?.pushViewController(stopDetailViewController, animated: true)
    }

    func openTransportDetailScreen(stopID: NSManagedObjectID, transportID: NSManagedObjectID) {
        let transportDetailViewController = R.storyboard.main.transportDetailViewController()!
        transportDetailViewController.stopID = stopID
        transportDetailViewController.transportID = transportID

        viewController?.navigationController?.pushViewController(transportDetailViewController, animated: true)
    }
}
