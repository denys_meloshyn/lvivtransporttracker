//
// Created by Denys Meloshyn on 2019-01-16.
// Copyright (c) 2019 Denys Meloshyn. All rights reserved.
//

import Foundation

protocol TransportStopTimesRouterProtocol {
    func openStopScreen(_ stopID: NSManagedObjectID)
}

class TransportStopTimesRouter: TransportStopTimesRouterProtocol {
    private weak var viewController: UIViewController?

    init(viewController: UIViewController) {
        self.viewController = viewController
    }

    func openStopScreen(_ stopID: NSManagedObjectID) {
        let stopDetailViewController = R.storyboard.main.stopDetailViewController()!
        stopDetailViewController.currentStopID = stopID

        viewController?.navigationController?.pushViewController(stopDetailViewController, animated: true)
    }
}
